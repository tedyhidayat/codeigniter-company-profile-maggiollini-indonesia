    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $title; ?>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <?= $this->session->flashdata('message'); ?>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success">
                    <div class="box-header">
                        <?php if($user['level'] === 'superadmin') : ?>
                        <a href="<?= site_url('admin/user/create/'); ?>" class="btn btn-success btn-sm btn-flat"><i
                                class="fa fa-plus"></i> Create New</a>
                        <?php endif; ?>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example2" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>NO.</th>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th>Username</th>
                                        <th>Level</th>
                                        <th>Is Active</th>
                                        <th>Status</th>
                                        <?php if($user['level'] === 'superadmin') : ?>
                                        <th>Action</th>
                                        <?php endif; ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!$users) : ?>
                                    <tr>
                                        <td class="text-center" colspan="6">Empty!</td>
                                    </tr>
                                    <?php else : ?>
                                    <?php $no = 1; foreach ($users as $row): ?>
                                    <tr>
                                        <td><?= $no++; ?>.</td>
                                        <td>
                                            <img width="70" class="img-thumbnail" src="<?= base_url('assets/images/user/' . $row['profile_picture']); ?>">
                                        </td>
                                        <td>
                                            <?= $row['full_name']; ?><br>
                                            <small class="text-muted">Created :
                                                <?= date("d F Y", strtotime($row['created_at'])); ?></small>
                                        </td>
                                        <td>
                                            <?= $row['username']; ?><br>
                                            <small class="text-muted"> Email : <?= $row['email']; ?></small>
                                        </td>
                                        <td>
                                            <?php show($row['level']); ?>
                                        </td>
                                        <td>
                                            <?php 
                                                if ($row['status'] == 1) {
                                                    echo "<span class='label bg-green'>
                                                        <i class='fa fa-check'></i> Active
                                                    </span>";
                                                } else {
                                                    echo "<span class='label bg-red'>
                                                        <i class='fa fa-time'></i> Blocked
                                                    </span>";
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php 
                                                if ($row['login'] == 1) {
                                                    echo "<span class='label bg-blue'>
                                                        <i class='fa fa-'></i> ONLINE
                                                    </span>";
                                                } else {
                                                    echo "<span class='label bg-gray'>
                                                        <i class='fa fa-time'></i> OFFLINE
                                                    </span>";
                                                }
                                            ?>
                                        </td>
                                        <?php if($user['level'] === 'superadmin') : ?>
                                            <td>
                                            <a href="<?= site_url('admin/user/edit/' . $row['id']); ?>"
                                                class="btn btn-info btn-sm btn-flat"><i class="fa fa-pencil"></i></a>
                                            <?php include('delete.php') ?>
                                            </td>
                                        <?php endif; ?>
                                    </tr>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>



    </section>