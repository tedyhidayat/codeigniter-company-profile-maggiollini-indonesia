<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('User_model', 'user');
		$this->load->model('User_model', 'user');
		$this->load->model('Auth_model', 'auth');
		$this->load->model('Posts_model', 'post');
		$this->load->model('Files_model', 'files');
		$this->load->model('Abouts_model', 'abouts');
		$this->load->model('Sample_model', 'sample');
		$this->load->model('Careers_model', 'careers');
		$this->load->model('Messages_model', 'messages');
		$this->load->model('Gallery_model', 'galleries');
		$this->load->model('Partners_model', 'partners');
		$this->load->model('Services_model', 'services');
		$this->load->model('Configurations_model', 'conf');
		security_access();
	}

    public function index(){
		$data = [
			'title' 			=> 'Dashboard',
			'allPosts' 			=> $this->post->getServalPosts(),
			'user' 				=> $this->user->getUserBySession(),
			'messages'			=> $this->messages->getAllMessages(),
			'count_posts' 		=> $this->db->count_all('tbl_posts'),
			'config' 			=> $this->configs->getConfiguration(),
			'count_sample' 		=> $this->db->count_all('tbl_sample'),
			'careers' 			=> $this->careers->getServalCareers(),
			'count_messages'	=> $this->db->count_all('tbl_message'),
			'count_gallery' 	=> $this->db->count_all('tbl_gallery'),
			'count_careers' 	=> $this->db->count_all('tbl_careers'),
			'count_partners' 	=> $this->db->count_all('tbl_partners'),
			'count_projects' 	=> $this->db->count_all('tbl_projects'),
			];
			$this->templating->load('layouts/admin/wrapper', 'admin/dashboard', $data);
    }

}

/* End of file Dashboard.php */

?>