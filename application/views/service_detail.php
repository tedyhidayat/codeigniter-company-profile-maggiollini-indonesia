    <!-- Breadcrumbs -->
    <section id="breadcrumbs">
        <div class="container">
            <div class=" row justify-content-between align-items-center">
                <div class="col-12 col-md col-lg">
                    <h2 class="text-uppercase"><?= $this->uri->segment(1); ?></h2>
                </div>
                <div class="col-12 col-md col-lg">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= site_url('/'); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?= site_url('/'. $this->uri->segment(1). '/'); ?>"><?= $this->uri->segment(1); ?></a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?= $this->uri->segment(2); ?></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Detail services -->
    <section id="detail-services" class="">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-4 px-4 p-lg-0 text-center justify-content-start detail-img">
                    <img class="img-responsive animated fadeIn" src="<?= base_url('assets/images/services/'.$service['image']); ?>" alt="<?= $service['title']; ?> - <?= $configs['siteName'] ?>" title="<?= $service['title']; ?>">
                    <cite><small class="text-muted"><?= $configs['siteName']; ?></small></cite>
                </div>
                <div class="col-12 col-md-12 col-lg-8 mt-0 px-4 pl-md-5 services-txt animated fadeIn">
                    <article>
                        <!-- place title article here -->
                        <header class="mb-3">
                            <h2 class="title-detail"><?= $service['title']; ?></h2>
                            <small class="text-muted mr-2"><i class="fas fa-tags"></i> <?= $service['keywords']; ?></small>
                            <small class="text-muted"><i class="fas fa-calendar"></i>  <?= date('d-F-Y', strtotime($service['created_at'])); ?></small>
                        </header>
                        <!-- place your article here -->
                        <?= $service['description']; ?>
                    </article>
                    <!-- share article -->
                    <div class="d-md-flex justify-content-between mt-4 button">
                        <div class="share">
                            <small> <b> Bagikan dengan:</b></small><br>

                            <a href="http://www.facebook.com/share.php?u=<?= site_url('/services/detail/'.$service['slug']); ?>" target="_blank" class="btn"><i class="fab fa-facebook"></i></a>

                            <a href="https://www.twitter.com/intent/tweet?text=<?= $service['title']; ?>&url=<?= site_url('projects/detail-project/'.$service['slug']); ?>&text=<?= $service['title']; ?>;hashtags=konraktorexhibition" target="_blank" class="btn"><i class="fab fa-twitter"></i></a>

                            <a href="whatsapp://send?text=<?= site_url('projects/detail-project/'.$service['slug']); ?>" class="btn"><i class="fab fa-whatsapp"></i></a>

                        </div>
                        <div class="button-back">
                            <a href="<?= site_url('/services/'); ?>" class="btn btn-general text-uppercase mt-4"><i class="fas fa-fw fa-arrow-left"></i> Kembali ke servis</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>