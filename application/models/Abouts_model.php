<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Abouts_model extends CI_Model {

	// function get all about from table
	public function getAllAbouts() {
		/** 
			*join 2 table 
			*-tbl_user : get full name
			*-tbl_abouts : get all data
			*-tbl_category_abouts : get nama category about
		*/
		$this->db->select('tbl_abouts.*, tbl_category_abouts.name, tbl_users.full_name' );
		$this->db->from('tbl_abouts');
		$this->db->join('tbl_category_abouts','tbl_category_abouts.id = tbl_abouts.id_category', 'LEFT');
		$this->db->join('tbl_users','tbl_users.id = tbl_abouts.id_user', 'LEFT');
		$this->db->order_by('tbl_abouts.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// function get about from table display in page about
	public function getallAboutActive() {
		/** 
			*join 2 table 
			*-tbl_user : get full name
			*-tbl_abouts : get all data
			*-tbl_category_abouts : get nama category about
		*/
		$this->db->select('tbl_abouts.*, tbl_category_abouts.name, tbl_users.full_name' );
		$this->db->from('tbl_abouts');
		$this->db->join('tbl_category_abouts','tbl_category_abouts.id = tbl_abouts.id_category', 'LEFT');
		$this->db->join('tbl_users','tbl_users.id = tbl_abouts.id_user', 'LEFT');
		$this->db->where('tbl_abouts.status','Publish');
		$this->db->order_by('tbl_abouts.id','ASC');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	// function get about by id 
	public function getAboutsById($id) {
		/**
		 * get data about with parameter $id
		 * parameter $id get from controller data array
		 */
		return $this->db->get_where('tbl_abouts', ['id' => $id])->row_array();
	}

	// function create about
	public function create($data) {
		// get image from input type file 
		$upload_image = $_FILES['image']['name'];
		// Check data upload image
		if ($upload_image) {
			// define uploads file
			$config['max_size']     	= 2048;
			$config['allowed_types'] 	= 'jpg|jpeg|png';
			$config['upload_path'] 		= './assets/images/abouts/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-about-';
			// initializing $config
			$this->load->library('upload', $config);
			// check if file available
			if ( ! $this->upload->do_upload('image')) {
				// if failed redirect to page and display errors
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . '</div>');
				redirect('admin/abouts/create/');
			} else {
				/* if success define configuration for made thumbnail file */
				$config['image_library'] 	= 'gd2';
				$config['create_thumb'] 	= TRUE;
				$config['maintain_ratio'] 	= TRUE;
				$config['width']         	= 500;
				$config['height']       	= 470;
				$config['thumb_marker']    	= '';
				$config['source_image']	 	= './assets/images/abouts/'.$this->upload->data('file_name');
				$config['new_image']	 	= './assets/images/abouts/thumbs/'.$this->upload->data('file_name');
				// initializing configuration for made thumbnail
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				// collecting data from input form
				$data = [
					'created_at'	=> date('Y-m-d H:i:s'),
					'id_user' 		=> $this->session->userdata('id'),
					'image' 		=> $this->upload->data('file_name'),
					'title' 		=> $this->input->post('title', true),
					'status' 		=> $this->input->post('status', true),
					'id_category'	=> $this->input->post('categId', true),
					'keywords' 		=> $this->input->post('keywords', true),
					'description' 	=> $this->input->post('description', true),
					'slug' 			=> url_title($this->input->post('title', true), 'dash', true),
				];
			}
		} else {
			/** 
			 * if nothing file to upload
			 * collect data from input form
			*/ 
			$data = [
				'created_at'	=> date('Y-m-d H:i:s'),
				'id_user' 		=> $this->session->userdata('id'),
				'title' 		=> $this->input->post('title', true),
				'status' 		=> $this->input->post('status', true),
				'id_category'	=> $this->input->post('categId', true),
				'keywords' 		=> $this->input->post('keywords', true),
				'description' 	=>$this->input->post('description', true),
				'slug' 			=> url_title($this->input->post('title', true), 'dash', true),
			];
		}

		// query insert data
		$query = $this->db->insert('tbl_abouts', $data);
		// check data insert
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> failed insert data!</div>');
			redirect('admin/abouts/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Success.</div>');
			redirect('admin/abouts/');
		}
	}

	// functiomn edtit about
	public function edit($data) {
		// get data from input form 
		$title 			= $this->input->post('title', true);
		$status 		= $this->input->post('status', true);
		$id_category	= $this->input->post('categId', true);
		$keywords 		= $this->input->post('keywords', true);
		$description 	= $this->input->post('description', true);
		$slug 			= url_title($this->input->post('title', true), 'dash', true);
		// data upload file
		$upload_image 	= $_FILES['image']['name'];
		// check if upload file availabe
		if ($upload_image) {
			// define uploads file
			$config['max_size']     	= 2048;
			$config['allowed_types']	= 'jpeg|jpg|png';
			$config['upload_path'] 		= './assets/images/abouts/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-about-';
			// initializing $config
			$this->load->library('upload', $config);
			// check upload file
			if ( ! $this->upload->do_upload('image')) {
				// if nothing file to upload
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . ' You only can upload '. $config['allowed_types'] . ' type.' . '</div>');
				redirect('admin/abouts/edit/'. $data['id']);
			} else {
				// if file upload available
				// check old file
				$old_image = $data['about']['image'];
				if ($old_image != 'default.png') {
					// if ild file not same with default.png, delete file
					unlink(FCPATH . 'assets/images/abouts/' . $old_image);
					unlink(FCPATH . 'assets/images/abouts/thumbs/' . $old_image);
				}
				// define to creating thmbnail
				$new_file = $this->upload->data('file_name');
				$config['image_library'] 	= 'gd2';
				$config['source_image']	 	= './assets/images/abouts/'.$new_file;
				$config['new_image']	 	= './assets/images/abouts/thumbs/'.$new_file;
				$config['create_thumb'] 	= TRUE;
				$config['maintain_ratio'] 	= TRUE;
				$config['width']         	= 500;
				$config['height']       	= 470;
				$config['thumb_marker']    	= '';
				// initializing creating thumbnail
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				// set new file to database
				$this->db->set('image', $new_file);
			}
		}
		// query update
		$this->db->set('slug', $slug);
		$this->db->set('title', $title);
		$this->db->set('status', $status);
		$this->db->set('keywords', $keywords);
		$this->db->set('id_category', $id_category);
		$this->db->set('description', $description);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $data['id']);
		$this->db->update('tbl_abouts');
		// check if there is data changes
		if($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed editing data!</div>');
			redirect('admin/abouts/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your data ' . $title . ' has been updated!</div>');
			redirect('admin/abouts/');
		}
	}

	// function delete data about
	public function delete($data) {
		// check old file
		$old_image= $data['about']['image'];
		if ($old_image != 'default.png') {
			// if file available, deelete old file
			unlink(FCPATH . 'assets/images/abouts/' . $old_image);
			unlink(FCPATH . 'assets/images/abouts/thumbs/' . $old_image);
		}
		// query delete
		$this->db->where('id', $data['id']);
		$this->db->delete('tbl_abouts');
		// check if there is data changes
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed delete data!</div>');
			redirect('admin/abouts/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> About has been delete!</div>');
			redirect('admin/abouts/');
		}
	}

}

/* End of file Abouts_model.php */
/* Location: ./application/models/Abouts_model.php */
