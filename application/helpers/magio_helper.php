<?php

// for handling xss
function showing($str) {
	echo htmlentities($str, ENT_QUOTES, 'UTF-8');
}

// login security
function security_access() { 
	$CI =& get_instance();
	// check if user admin have session
	if (!$CI->session->userdata('username')) redirect('auth');

	// menu access management between admin and superadmin
	// check if level is admin, cannot access defined menu (configurations and configuration_images)
	if ($CI->session->userdata('level') == 'admin') {
		// get uri segment controller
		if ($CI->uri->segment(2) == 'configurations' ) {
			redirect('admin/dashboard','refresh');
		} elseif ($CI->uri->segment(2) == 'configuration_images' ) {
			redirect('admin/dashboard','refresh');
		}
	}

	// check if there configurations
	$CI->load->model('Configurations_model'); // load model configurations
	$config = $CI->Configurations_model->getConfiguration();
	if (!$config) {
		if ($CI->uri->segment(2) == 'dashboard' ||
			$CI->uri->segment(2) == 'abouts' ||
			$CI->uri->segment(2) == 'careers' ||
			$CI->uri->segment(2) == 'categories' ||
			$CI->uri->segment(2) == 'category_abouts' ||
			$CI->uri->segment(2) == 'category_files' ||
			$CI->uri->segment(2) == 'category_gallery' ||
			$CI->uri->segment(2) == 'category_projects' ||
			$CI->uri->segment(2) == 'category_sample' ||
			$CI->uri->segment(2) == 'category_videos' ||
			$CI->uri->segment(2) == 'files' ||
			$CI->uri->segment(2) == 'gallery' ||
			$CI->uri->segment(2) == 'news' ||
			$CI->uri->segment(2) == 'partners' ||
			$CI->uri->segment(2) == 'projects' ||
			$CI->uri->segment(2) == 'sample' ||
			$CI->uri->segment(2) == 'services' ||
			$CI->uri->segment(2) == 'videos' ) {
			$CI->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> You must setting site configurations!</div>');
			redirect('admin/configurations','refresh');
		} 
	}

	// check if nothing users registred
	$CI->load->model('User_model'); // load model configurations
	$users = $CI->User_model->getAllUser();
	if (!$users) {
		redirect('auth/logout');
	}
}

function create_cookie() {
	$CI =& get_instance();

	$cookie = [
		'name'      => 'set_cookie',
		'value'     => 'set_cookie_all',
		'domain'    => 'www.maggiollini.com',
		'path'      => '/',
		'expire'    => time()+(60 * 60 * 24 * 7),
		'prefix'    => 'thdev_',
		'secure'	=> true,
	];

	$CI->input->set_cookie($cookie);
}

?>