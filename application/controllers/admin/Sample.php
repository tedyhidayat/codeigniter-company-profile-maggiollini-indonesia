<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sample extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		security_access();
		$this->load->model('User_model');
		$this->load->model('Sample_model');
		$this->load->model('Category_sample_model');
	} 

	// function show index sample page
    public function index() {
		// get data from models
		$data = [
			'title' 	=> 'Sample',
			'config' 	=> $this->configs->getConfiguration(),
			'allSample' => $this->Sample_model->getAllSample(),
			'user' 		=> $this->User_model->getUserBySession(),
		];
		// view index sample page
        $this->templating->load('layouts/admin/wrapper', 'admin/sample/index', $data);
	}
	
	// function create sample page
	public function create() {
		// get data from models
		$data = [
			'title' 	=> 'Add Sample',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'categories'=> $this->Category_sample_model->getAllCategorySample()
		];
		// validation rules
		$this->form_validation->set_rules('image', 'Image', 'trim');
		$this->form_validation->set_rules('keywords', 'keywords', 'trim');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('categId', 'category', 'required');
		$this->form_validation->set_rules('sampleName', 'Sample Name', 'trim|required');
		$this->form_validation->set_rules('description', 'Description', 'required|trim');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/sample/create', $data);
		} else {
			// if validation success call function create from sample_model
			$this->Sample_model->create($data);
		}
	}

	// function edit sample page
	public function edit($id) {
		// get data from models
		$data = [
			'id' 		=> $id,
			'title' 	=> 'Edit Finished Sample',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'sample' 	=> $this->Sample_model->getSampleById($id),
			'categories'=> $this->Category_sample_model->getAllCategorySample(),
		];
		// validation rules
		$this->form_validation->set_rules('image', 'Image', 'trim');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('keywords', 'keywords', 'trim');
		$this->form_validation->set_rules('categId', 'category', 'required');
		$this->form_validation->set_rules('sampleName', 'Sample Name', 'trim|required');
		$this->form_validation->set_rules('description', 'Description', 'required|trim');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/sample/update', $data);
		} else {
			// if validation success call function edit from sample_model
			$this->Sample_model->edit($data);
		}
	}

	// function delete sample
	public function delete($id) {
		// get data from models
		$data = ['id' => $id, 'sample' => $this->Sample_model->getSampleById($id)];
		// call function delete from sample_model
		$this->Sample_model->delete($data);
		
	}
	
}

/* End of file Sample.php */

?>