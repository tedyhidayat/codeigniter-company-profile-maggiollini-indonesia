Website Company Profile PT. Maggiollini Indonesia.
Dibuat untuk menampung kumpulan projek yang telah mereka kerjakan. Berupa sarana media promosi untuk produk mereka.

Fitur :
- Content Management System
- Interface
- CRUD
- Authentication

Teknologi didalamnya :
- Codeigniter 3
- Bootstrap 4
- HTML
- CSS
- Jquery
- Javascript
- dan beberapa library pendukung