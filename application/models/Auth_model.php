<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

	// function get all data user
	public function getAllUser() {
		return $this->db->get('tbl_user')->result_array();
	}

	// function registration 
	public function registration() {
		// count all user from table user
		$count = $this->db->count_all('tbl_users');
		// check if user < 1, registration runing
		if ($count < 1) {
			$data = [
				'login' 			=> 0,
				'status' 			=> 1,
				'level' 			=> 'superadmin',
				'profile_picture' 	=> 'default.png',
				'email' 			=> htmlspecialchars(addslashes($this->input->post('email', true))),
				'username' 			=> htmlspecialchars(addslashes($this->input->post('username', true))),
				'full_name' 		=> htmlspecialchars(addslashes($this->input->post('full_name', true))),
				'password' 			=> password_hash(htmlspecialchars($this->input->post('password1', true)), PASSWORD_DEFAULT),
			];
			$this->db->insert('tbl_users', $data);
			$this->session->unset_userdata('captchaImage');
		} else {
			// if user > 1, regstration blocked! and redirect to registration page.
			$this->session->set_flashdata('message', '<div class="alert alert-danger shadow alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Full!</div>');
			redirect('auth/registration');
		}
	}

	// function login
	public function _login() {
		// get data input from
		$username = htmlspecialchars(addslashes($this->input->post('username', true)));
		$password = htmlspecialchars(addslashes($this->input->post('password', true)));
		// get username from tbl users
		$user 	  = $this->db->get_where('tbl_users', ['username' => $username])->row_array();
		// check if username is the same username in tbl users 
		if ($user) {
			// check if password matching with password in the table user
			if (password_verify($password, $user['password'])) {
					// creat data for session
					$data = [
						'id' 				=> $user['id'],
						'email' 			=> $user['email'],
						'level' 			=> $user['level'],
						'username' 			=> $user['username'],
						'full_name' 		=> $user['full_name'],
						'profile_picture' 	=> $user['profile_picture'],
					];
					// set session user from input username
					$this->session->set_userdata($data);
					// check if user level is admin or superadmin 
					if($user['level'] == 'admin' || $user['level'] == 'superadmin') {
						// update login data for last login where username (and success login redirect to admin)
						$this->db->set('login', 1); // set login user (status: online)
						$this->db->set('login_at', date('Y-m-d H:i:s'));
						$this->db->where('username', $username);
						$this->db->update('tbl_users');
						$this->session->unset_userdata('admin_access');
						$this->session->unset_userdata('captchaImage');
						redirect('admin/dashboard','refresh');
					} else {
						// if failed redirect to login page
						redirect('auth');
					}
			} else {
				// check if wrong password
				$this->session->set_flashdata('message', '<div class="alert alert-danger shadowalert-dismissable"role="alert"><button type="button" class="close" data-dismiss="alert"aria-hidden="true">×</button>Wrong password!</div>');
				redirect('auth/');
			}
		} else {
			// if user not regsitered (nothing in the tbl users)
			$this->session->set_flashdata('message', '<div class="alert alert-danger shadow alert-dismissable"role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> This User is not registered!</div>');
			redirect('auth/');
		}
			
	}

	// function logout
	public function logoutAt() {
		// logout and update data last logout where session username
		$this->db->set('login', 0);
		$this->db->set('logout_at', date('Y-m-d H:i:s'));
		$this->db->where('username', $this->session->userdata('username'));
		$this->db->update('tbl_users');
	}

}

/* End of file Auth_model.php */
/* Location: ./application/models/Auth_model.php */