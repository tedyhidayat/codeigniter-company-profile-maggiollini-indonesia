<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category_files extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		security_access();
		$this->load->model('User_model');
		$this->load->model('Category_files_model');
	}

	// function show index categoriey files
    public function index() {
		// get data from models
		$data = [
			'title' 			=> 'Category files',
			'config' 			=> $this->configs->getConfiguration(),
			'user' 				=> $this->User_model->getUserBySession(),
			'category_files' 	=> $this->Category_files_model->getAllCategoryfiles(),
		];
		// view index category files
        $this->templating->load('layouts/admin/wrapper', 'admin/category_files/index', $data);
	}
	
	// function create category file page
	public function create() {
		// get data from models
		$data = [
			'title' 	=> 'Add Category Files',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
		];
		// validation rules
		$this->form_validation->set_rules('categName', 'category', 'trim|required|is_unique[tbl_category_files.name]', [
			'is_unique' => 'Categoty Name already exist!'
		]);
		$this->form_validation->set_rules('position', 'position', 'required|numeric|is_unique[tbl_category_files.position]', [
			'is_unique' => 'position Name already exist!'
		]);
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/category_files/create', $data);
		} else {
			// if validation success call model create from category_files_model
			$this->Category_files_model->create($data);
		}
	}

	// function edit category file page
	public function edit($id) {
		// get data from models
		$data = [
			'id' 		=> $id,
			'title' 	=> 'Edit Category',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'category' 	=> $this->Category_files_model->getCategoryFilesById($id),
		];
		// validation rules
		$this->form_validation->set_rules('categName', 'category name', 'trim|required');
		$this->form_validation->set_rules('position', 'position', 'required|trim|numeric');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/category_files/update', $data);
		} else {
			// if validation success call model create from category_files_model
			$this->Category_files_model->edit($data);
		}
	}

	// function delete category file page
	public function delete($id) {
		// get data from models
		$data = ['id' => $id, 'category' => $this->Category_files_model->getCategoryFilesById($id)];
		// call function delete from category_files_model
		$this->Category_files_model->delete($data);
	}
	
}

/* End of file Category_files.php */

?>
