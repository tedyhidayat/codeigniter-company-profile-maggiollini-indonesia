<?php if($configs) : ?>   
    <!-- Breadcrumbs -->
    <section id="breadcrumbs">
        <div class="container">
            <div class=" row justify-content-between align-items-center">
                <div class="col-12 col-md col-lg">
                    <h2 class="text-uppercase"><?= $this->uri->segment(1); ?></h2>
                </div>
                <div class="col-12 col-md col-lg">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= site_url('/'); ?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?= $this->uri->segment(1); ?></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- Careers -->
    <section id="careers" class="">
        <div class="container">
            <?php if($careers) : ?>  
                <div class="row header-text">
                    <div class="col-12 note">
                    <?php foreach($config_page as $row_page) : ?>
                        <?php if($row_page['page_position'] == 'Careers') : ?>
                        <div class="card">
                            <div class="card-body">
                                <header>
                                    <h5 class="card-title"><?= $row_page['title']; ?></h5>
                                    <p><?= $row_page['description']; ?></p>
                                </header>
                            </div>
                        </div>
                        <?php endif;?>
                    <?php endforeach;?>
                    </div>
                    <!-- share -->
                    <div class="col-12 share">
                        <div class="card">
                            <div class="card-body">
                                <span><b> Bagikan ke :</b></span>
                                <div class="mt-3">
                                    <a class="btn-share" href="http://www.facebook.com/sharer.php?u=<?= site_url('/careers/'); ?>" target="_blank"><i class="fab fa-facebook"></i></a>

                                    <a class="btn-share" target="_blank" href="whatsapp://send?text=<?= site_url('careers/'); ?>"><i class="fab fa-whatsapp"></i></a>

                                    <a class="btn-share" href="https://www.twitter.com/intent/tweet?text=Careers%20<?= $configs['siteName']; ?>&url=<?= site_url('/careers/'); ?>" target="_blank"><i class="fab fa-twitter"></i></a>
                                    
                                    <a class="btn-share" target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?= site_url('careers/'); ?>&source=<?= $configs['siteUrl']; ?>"><i class="fab fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3 items-career" data-aos="fade-up">   
                    <!-- item career -->
                    <?php foreach($careers as $row_career) : ?>
                    <div class="col-6 col-sm-6 col-lg-4 mb-4">
                        <div class="card" style="background-image: url('<?= base_url('assets/images/careers/'.$row_career['image']); ?>');">
                            <div class="card-body">
                                <h5 class="card-title"><?= $row_career['title']; ?> </h5>
                                <p class="card-text"><small>Position: <b> <?= $row_career['position']; ?></b></small></p>
                                <a href="<?= site_url('/careers/detail/'.$row_career['slug']); ?>" class="btn btn-detail-career text-uppercase">Tamplikan Detail</a>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            <?php else : ?>
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="card">
                            <div class="card-body">
                                <h4>Karir belum tersedia.</h4>
                                <p><?= $configs['siteUrl']; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </section>
<?php else : ?>
    <section>
        <div class="container" style="min-height: 300px; ">
            <div class="row align-items-center">
                <div class="col-12 text-center">
                    <h2 class="mt-5">EMPTY!</h2>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>