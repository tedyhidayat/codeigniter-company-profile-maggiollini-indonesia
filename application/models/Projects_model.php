<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projects_model extends CI_Model {

	// sitemap
	public function sitemap() {
		$this->db->select('tbl_projects.slug, tbl_projects.created_at, tbl_projects.project_name');
		$this->db->from('tbl_projects');
		$this->db->order_by('id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// function get all data projects
	public function getAllProjects() {
		/**
		 * tbl_project join tbl_category_projects and tbl_users
		 * tbl_users: get full name, tbl_category_projects: get project name
		 */
		$this->db->select('tbl_projects.*, tbl_users.full_name, tbl_category_projects.name');
		$this->db->from('tbl_projects');
		$this->db->join('tbl_users','tbl_users.id = tbl_projects.id_user', 'LEFT');
		$this->db->join('tbl_category_projects','tbl_category_projects.id = tbl_projects.id_category', 'LEFT');
		$this->db->order_by('tbl_projects.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	// function get data projects limiter
	public function public_home_projects() {
		/**
		 * tbl_project join tbl_category_projects and tbl_users
		 * tbl_users: get full name, tbl_category_projects: get project name
		 */
		$this->db->select('tbl_projects.*, tbl_users.full_name, tbl_category_projects.name');
		$this->db->from('tbl_projects');
		$this->db->join('tbl_users','tbl_users.id = tbl_projects.id_user', 'LEFT');
		$this->db->join('tbl_category_projects','tbl_category_projects.id = tbl_projects.id_category', 'LEFT');
		$this->db->where('tbl_projects.status','Publish');
		$this->db->limit(9);
		$this->db->order_by('tbl_projects.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// function get data projects pagination
	public function public_projects_active($limit, $start) {
		/**
		 * tbl_project join tbl_category_projects and tbl_users
		 * tbl_users: get full name, tbl_category_projects: get project name
		 */
		$this->db->select('tbl_projects.*, tbl_users.full_name, tbl_category_projects.name');
		$this->db->from('tbl_projects');
		$this->db->join('tbl_users','tbl_users.id = tbl_projects.id_user', 'LEFT');
		$this->db->join('tbl_category_projects','tbl_category_projects.id = tbl_projects.id_category', 'LEFT');
		$this->db->where('tbl_projects.status','Publish');
		$this->db->limit($limit, $start);
		$this->db->order_by('tbl_projects.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// function get data projects limiter
	public function total() {
		/**
		 * tbl_project join tbl_category_projects and tbl_users
		 * tbl_users: get full name, tbl_category_projects: get project name
		 */
		$this->db->select('tbl_projects.*, tbl_users.full_name, tbl_category_projects.name');
		$this->db->from('tbl_projects');
		$this->db->join('tbl_users','tbl_users.id = tbl_projects.id_user', 'LEFT');
		$this->db->join('tbl_category_projects','tbl_category_projects.id = tbl_projects.id_category', 'LEFT');
		$this->db->where('tbl_projects.status','Publish');
		$this->db->order_by('tbl_projects.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	// fuunction get data project by ID
	public function getProjectsBySlug($slug = NULL) {
		if (!empty($slug)) {
			$this->db->select('tbl_projects.*, tbl_users.full_name, tbl_category_projects.name');
			$this->db->from('tbl_projects');
			$this->db->join('tbl_users','tbl_users.id = tbl_projects.id_user', 'LEFT');
			$this->db->join('tbl_category_projects','tbl_category_projects.id = tbl_projects.id_category', 'LEFT');
			$this->db->where('tbl_projects.slug',$slug);
			$query = $this->db->get();
			return $query->row_array();
		} else {
			return false;
		}
	}

	// fuunction get data project by ID
	public function getProjectsById($id) {
		return $this->db->get_where('tbl_projects', ['id' => $id])->row_array();
	}

	// function create data project
	public function create($data) {
		// get data from input file
		$upload_image = $_FILES['image']['name'];
		// check uploaded file
		if ($upload_image) {
			// define config upload file
			$config['max_size']     	= 2048;
			$config['file_ext_tolower'] = TRUE;
			$config['allowed_types'] 	= 'jpg|jpeg|png';
			$config['upload_path'] 		= './assets/images/projects/finished_projects/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-project-';
			// initializing config upload file
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('image')) {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . '</div>');
				redirect('admin/projects/create/');
			} else {
				// if success
				// define creating thumbnail
				$file = $this->upload->data('file_name');
				$config['image_library'] 	= 'gd2';
				$config['create_thumb'] 	= TRUE;
				$config['maintain_ratio'] 	= TRUE;
				$config['width']         	= 500;
				$config['height']       	= 470;
				$config['thumb_marker']    	= '';
				$config['source_image']	 	= './assets/images/projects/finished_projects/'.$file;
				$config['new_image']	 	= './assets/images/projects/finished_projects/thumbs/'.$file;
				// initializing config creating thumbnail
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				// collecting data for insert table
				$insertProjects = [
					'image' 		=> $file,
					'created_at' 	=> date('Y-m-d H:i:s'),
					'id_user' 		=> $this->session->userdata('id'),
					'status' 		=> $this->input->post('status', true),
					'keywords' 		=> $this->input->post('keywords', true),
					'id_category' 	=> $this->input->post('categoryId', true),
					'project_name'  => $this->input->post('projectName', true),
					'description' 	=> $this->input->post('description', true),
					'slug' 			=> url_title($this->input->post('projectName', true), 'dash', true),
				];
			}
		} else {
			// if noting file uploaded
			// collecting data for insert table
			$insertProjects = [
				'created_at' 	=> date('Y-m-d H:i:s'),
				'id_user' 		=> $this->session->userdata('id'),
				'status' 		=> $this->input->post('status', true),
				'keywords' 		=> $this->input->post('keyswords', true),
				'id_category' 	=> $this->input->post('categoryId', true),
				'description' 	=> $this->input->post('description', true),
				'project_name' 	=> $this->input->post('projectName', true),
				'slug' 			=> url_title($this->input->post('ProjectsName', true), 'dash', true),
			];
		}
		// query insert
		$this->db->insert('tbl_projects', $insertProjects);
		// check affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed insert data!</div>');
			redirect('admin/projects/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Success insert data!</div>');
			redirect('admin/projects/');
		}
	}

	// function edit data project
	public function edit($data) {
		// get data from input form
		$id_user 		= $this->session->userdata('id');
		$status 		= $this->input->post('status', true);
		$keywords 		= $this->input->post('keywords', true);
		$id_category 	= $this->input->post('categoryId', true);
		$projectName 	= $this->input->post('projectName', true);
		$description 	= $this->input->post('description', true);
		$slug 			= url_title($this->input->post('projectName', true), 'dash', true);
		$upload_image 	= $_FILES['image']['name'];
		// check uploaded file
		if ($upload_image) {
			// define config upload file
			$config['file_ext_tolower'] = TRUE;
			$config['max_size']     	= 2048;
			$config['allowed_types'] 	= 'jpg|png';
			$config['upload_path'] 		= './assets/images/projects/finished_projects/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-project-';
			// initializing config uppload file
			$this->load->library('upload', $config);
			if ($this->upload->do_upload('image')) {
				// if success
				// unlink image
				$old_image = $data['project']['image'];
				if ($old_image != 'default.png') {
					unlink(FCPATH . 'assets/images/projects/finished_projects/' . $old_image);
					unlink(FCPATH . 'assets/images/projects/finished_projects/thumbs/' . $old_image);
				}
				// define config creating thumbnail
				$new_image = $this->upload->data('file_name');
				$config['image_library'] 	= 'gd2';
				$config['create_thumb'] 	= TRUE;
				$config['maintain_ratio'] 	= TRUE;
				$config['width']         	= 500;
				$config['height']       	= 470;
				$config['thumb_marker']    	= '';
				$config['source_image']	 	= './assets/images/projects/finished_projects/'.$new_image;
				$config['new_image']		= './assets/images/projects/finished_projects/thumbs/'.$new_image;
				// initializing config creat thumbnail
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				// set file to table
				$this->db->set('image', $new_image);
			} else {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . '</div>');
				redirect('admin/projects/edit/' . $data['id']);
			}
		}
		// query update
		$this->db->set('slug', $slug);
		$this->db->set('status', $status);
		$this->db->set('keywords', $keywords);
		$this->db->set('id_category', $id_category);
		$this->db->set('description', $description);
		$this->db->set('project_name', $projectName);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $data['id']);
		$this->db->update('tbl_projects');
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your Projects failed to update!</div>');
			redirect('admin/projects/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your Projects has been updated!</div>');
			redirect('admin/projects/');
		}
	}

	// function Delete Projects
	public function delete($data) {
		if ($data['id']) {
			$old_image = $data['project']['image'];
			if ($old_image) {
				unlink(FCPATH . 'assets/images/projects/finished_projects/' . $old_image);
				unlink(FCPATH . 'assets/images/projects/finished_projects/thumbs/' . $old_image);
			}
			$this->db->where('id', $data['id']);
			$this->db->delete('tbl_projects');
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Success delete '. $data['projects']['project_name'] .'</div>');
			redirect('admin/projects/');
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to delete data!</div>');
			redirect('admin/projects/');
		}
	}


}

/* End of file Projects_model.php */
/* Location: ./application/models/Projects_model.php */