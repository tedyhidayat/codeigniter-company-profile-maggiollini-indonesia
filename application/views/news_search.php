    <!-- Breadcrumbs -->
    <section id="breadcrumbs">
        <div class="container">
            <div class=" row justify-content-between align-items-center">
                <div class="col-12 col-md col-lg">
                    <h2 class="text-uppercase"><?= $this->uri->segment(1); ?></h2>
                </div>
                <div class="col-12 col-md col-lg">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= site_url('/'); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?= site_url('/'.$this->uri->segment(1)); ?>"><?= $this->uri->segment(1); ?></a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?= $this->uri->segment(2); ?></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    
    <!-- post -->
    <main>
        <!-- News -->
        <section id="news">
            <div class="container">
                <div class="row justify-content-between">
                    <!-- ===============list news======================== -->
                    <div class="col-md-8 news-main mb-3">
                      <div class="row">
                        <div class="col-12 mb-4">
                            <h3 style="font-size:22px; font-weight: 400;">Keyword: <b> <?= $keyword; ?></b></h3>
                        </div>
                      </div>
                      <div class="row justify-content-between">
                        <!-- news item -->
                        <?php if(count($news) > 0) : ?>
                          <?php foreach($news as $row_search) : ?>
                            <article class="entry-news col-6 col-md-6" data-aos="fade-up">
                                <div class="card">
                                    <div class="img-news">
                                        <img src="<?= base_url('assets/images/posts/'.$row_search['image']); ?>" class="img-fluid" alt="<?= $row_search['title']; ?>">
                                    </div>
                                    <div class="card-body entry-content">
                                      <h2 class="card-title"><?= $row_search['title']; ?></h2>
                                      <div class="meta">
                                          <span class="meta-item"><i class="fas fa-fw fa-folder"></i> <?= $row_search['name']; ?></span>
                                          <span class="meta-item"><i class="fas fa-fw fa-user"></i> <?= $row_search['full_name']; ?></span>
                                          <span class="meta-item"><i class="fas fa-fw fa-calendar"></i> <?= date('d F Y', strtotime($row_search['created_at'])); ?></span>
                                      </div>
                                      <p class="card-text news-content">
                                        <?= word_limiter(strip_tags($row_search['description']), 20); ?>
                                      </p>
                                      <a href="<?= site_url('news/post/'.$row_search['slug']); ?>" class="btn btn-general">Baca Selengkapnya</a>
                                    </div>
                                  </div>
                            </article>
                          <?php endforeach; ?>
                        <?php else: ?>
                            <div class="col-12 text-center bg-white">
                              <h3>Berita tidak ditemukan!</h3>
                            </div>
                        <?php endif; ?>
                      </div>
                    </div>
                    <!-- ===============sidebar of news==================== -->
                    <aside class="col-md-4 sidebar" data-aos="fade-up">
                        <!-- search news -->
                        <div class="form-search">
                            <h3>Pencarian</h3>
                            <?= form_open('news/search/'); ?>
                                <div class="input-group mb-2">
                                    <input name="keywords" type="text" class="form-control" placeholder="Cari berita..">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <button type="submit"><i class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                  </div>
                            <?= form_close(); ?>
                        </div>
                        <!-- recent post -->
                        <div class="recent-post">
                            <h3>Posting Terbaru</h3>
                            <?php foreach($recent as $row_recent) : ?>
                            <div class="media">
                                <div class="img-media">
                                    <img src="<?= base_url('assets/images/posts/'.$row_recent['image']); ?>" class="img-fluid" alt="...">
                                </div>
                                <div class="media-body">
                                  <h5 class="mt-0"><a href="<?= site_url('news/post/'.$row_recent['slug']); ?>"><?= $row_recent['title']; ?></a></h5>
                                  <span><?= date('d F Y', strtotime($row_recent['created_at'])); ?></span>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                        <!-- tags -->
                        <div class="tags">
                            <h3>Kategori</h3>
                            <ul class="nav flex-row tags-item">
                                <?php foreach($category as $row_category) : ?>
                                <li class="nav-item">
                                  <a class="nav-link" href="<?= site_url('news/category/'.$row_category['slug']); ?>"><?= $row_category['name']; ?></a>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </aside>
                </div>
            </div>
        </section>
    </main>