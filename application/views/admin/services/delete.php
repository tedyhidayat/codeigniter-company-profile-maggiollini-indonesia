<button type="button" class="btn btn-danger btn-sm btn-flat" data-toggle="modal" data-target="#delete<?= $row['id'] ?>">
    <i class="fa fa-trash"></i>
</button>

<div class="modal fade" id="delete<?= $row['id'] ?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Are you sure to delete data ?</h4>
            </div>
            <div class="modal-body">
                <p><b> Item : </b><?= $row['title']; ?></p>
            </div>
            <div class="modal-footer">
                <a href="<?= base_url('admin/services/delete/' . $row['id'])?>"
                    class="btn btn-danger pull-right">Delete</a>
                <a class="btn pull-right" data-dismiss="modal">Cancel</a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<!-- /.modal -->