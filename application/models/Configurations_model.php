<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configurations_model extends CI_Model {

	// function get all data configuration site
	public function getAllConfigurations() {
		return $this->db->get('tbl_configurations')->result_array();
	}
	
	// function get all data configuration site
	public function getConfiguration() {
		$query = $this->db->get('tbl_configurations');
		if($query->result() != NULL) {
			foreach($query->result() as $row) {
				$config = [
					'fax' 		=> $row->fax,
					'icon' 		=> $row->icon,
					'logo' 		=> $row->logo,
					'email' 	=> $row->email,
					'gmaps' 	=> $row->gmaps,
					'twitter' 	=> $row->twitter,
					'address' 	=> $row->address,
					'tagline' 	=> $row->tagline,
					'siteUrl' 	=> $row->site_url,
					'keywords' 	=> $row->keywords,
					'facebook' 	=> $row->facebook,
					'whatsapp'	=> $row->whatsapp,
					'telephone' => $row->telephone,
					'instagram' => $row->instagram,
					'siteName' 	=> $row->site_name,
					'metaText' 	=> $row->meta_text,
					'metaDesc' 	=> $row->meta_desc,
				];
			}
			return $config;
		} else {
			return $this->db->get('tbl_configurations')->result_array();
		}
	}

	// function data configuration site by ID
	public function getConfigurationsById($id) {
		return $this->db->get_where('tbl_configurations', ['id' => $id])->row_array();
	}

	// function creat data configuration site
	public function create($data) {	
		// get data from input form
		$data = [
			'logo' 			=> 'default.png',
			'icon' 			=> 'default.png',
			'gmaps' 		=> $_POST['gmaps'],
			'instagram' 	=> $this->input->post('ig', true),
			'facebook' 		=> $this->input->post('fb', true),
			'id_user' 		=> $this->session->userdata('id'),
			'twitter' 		=> $this->input->post('tw', true),
			'whatsapp' 		=> $this->input->post('wa', true),
			'fax' 			=> $this->input->post('fax', true),
			'email' 		=> $this->input->post('email', true),
			'site_url' 		=> $this->input->post('siteUrl', true),
			'tagline' 		=> $this->input->post('tagline', true),
			'address' 		=> $this->input->post('address', true),
			'meta_text' 	=> $this->input->post('metaText', true),
			'keywords' 		=> $this->input->post('keywords', true),
			'meta_desc' 	=> $this->input->post('metaDesc', true),
			'site_name' 	=> $this->input->post('siteName', true),
			'telephone' 	=> $this->input->post('telephone', true),
		];
		// query insert
		$this->db->insert('tbl_configurations', $data);
		// check if affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to insert data!</div>');
			redirect('admin/configurations/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Success!</div>');
			redirect('admin/configurations/');
		}
		
	}

	// function edit configuuration site
	public function edit($data) {
		// get data from form
		$gmaps 			= $_POST['gmaps'];
		$created_at 	= date('Y-m-d H:i:s');
		$whatsapp 		= $this->input->post('wa', true);
		$instagram 		= $this->input->post('ig', true); 
		$facebook 		= $this->input->post('fb', true);
		$twitter 		= $this->input->post('tw', true);
		$fax 			= $this->input->post('fax', true);
		$email 			= $this->input->post('email', true);
		$site_url 		= $this->input->post('siteUrl', true);
		$address 		= $this->input->post('address', true);
		$tagline 		= $this->input->post('tagline', true);
		$meta_text 		= $this->input->post('metaText', true);
		$site_name 		= $this->input->post('siteName', true);
		$keywords 		= $this->input->post('keywords', true);
		$meta_desc 		= $this->input->post('metaDesc', true);
		$telephone 		= $this->input->post('telephone', true);
		// query update
		$this->db->set('fax', $fax);
		$this->db->set('gmaps', $gmaps);
		$this->db->set('email', $email);
		$this->db->set('twitter', $twitter);
		$this->db->set('address', $address);
		$this->db->set('tagline', $tagline);
		$this->db->set('whatsapp', $whatsapp);
		$this->db->set('keywords', $keywords);
		$this->db->set('facebook', $facebook);
		$this->db->set('site_url', $site_url);
		$this->db->set('telephone', $telephone);
		$this->db->set('site_name', $site_name);
		$this->db->set('meta_text', $meta_text);
		$this->db->set('meta_desc', $meta_desc);
		$this->db->set('instagram', $instagram);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $data['configId']['id']);
		$this->db->update('tbl_configurations');
		// check if affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed edit data!</div>');
			redirect('admin/configurations/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your Configurations has been updated!</div>');
			redirect('admin/configurations/');
		}
	}

	// function edit site logo site
	public function editLogo($data) {
		// get data image from input file
		$upload_image = $_FILES['image']['name'];
		// check uploaded file
		if ($upload_image) {
			// if availabel file/logo
			// define configuration upload logo
			$config['max_size']     	= 700;
			$config['allowed_types'] 	= 'jpg|png';
			$config['upload_path'] 		= './assets/images/configurations/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-logo-';
			// initializing configuration uploaded logo
			$this->load->library('upload', $config);
			if ($this->upload->do_upload('image')) {
				// if success
				// unlink old logo
				$old_image = $data['config']['logo'];
				if ($old_image != 'default.png') {
					unlink(FCPATH . 'assets/images/configurations/' . $old_image);
				}
				// set logog to database
				$this->db->set('logo', $this->upload->data('file_name'));
			} else {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . '</div>');
				redirect('admin/configurations/edit/' . $data['id']);
			}
		}
		// query update
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $data['id']);
		$this->db->update('tbl_configurations');
		// check id data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed edit logo!</div>');
			redirect('admin/configurations/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your Logo has been updated!</div>');
			redirect('admin/configurations/');
		}
		
	}

	// function edit icon site
	public function editIcon($data) {
		// get data uploaded file from input file
		$upload_image = $_FILES['image']['name'];
		// if uploaded file
		if ($upload_image) {
			// define file uploaded config
			$config['max_size']     	= 700;
			$config['file_name']     	= 'logo';
			$config['allowed_types'] 	= 'ico|png';
			$config['upload_path'] 		= './assets/images/configurations/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-icon-';
			// initializing uploaded file config
			$this->load->library('upload', $config);
			if ($this->upload->do_upload('image')) {
				// if success
				// unlink old icon
				$old_image = $data['config']['icon'];
				if ($old_image != 'default.png') {
					unlink(FCPATH . 'assets/images/configurations/' . $old_image);
				}
				// set icon to database
				$this->db->set('icon', $this->upload->data('file_name'));
			} else {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . '</div>');
				redirect('admin/configurations/edit/' . $data['id']);
			}
		}
		// query update
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $data['id']);
		$this->db->update('tbl_configurations');
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failde to edit icon!</div>');
			redirect('admin/configurations/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your Icon has been updated!</div>');
			redirect('admin/configurations/');
		}
		
	}

	// Reset Configurations
	public function reset($data) {
		$logo = $data['configId']['logo'];
		$icon = $data['configId']['icon'];
		if ($logo != 'default.png' && $icon != 'default.png') {
			unlink(FCPATH . 'assets/images/configurations/' . $logo);
			unlink(FCPATH . 'assets/images/configurations/' . $icon);
		}
		$this->db->where('id', $data['id']);
		$this->db->delete('tbl_configurations');
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your configuration site has been reset!</div>');
		redirect('admin/configurations/');
	}


}

/* End of file Configurations_model.php */
/* Location: ./application/models/Configurations_model.php */

