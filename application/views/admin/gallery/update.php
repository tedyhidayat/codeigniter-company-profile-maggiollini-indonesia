    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit gallery
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <?= $this->session->flashdata('message'); ?>


        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" action="<?= site_url('admin/gallery/edit/' . $gallery['id']); ?>"
                        enctype="multipart/form-data" role="form">
                        <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" class="form-control" value="<?= $this->security->get_csrf_hash(); ?>" >
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12 col-md-8">
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input type="text" name="title" class="form-control" id="title" value="<?= $gallery['title']; ?>">
                                        <?= form_error('title', '<small class="text-danger">', '</small>'); ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <label for="status">Status</label>
                                        <select name="status" id="status" class="form-control">
                                            <?php if (!$gallery['status']) : ?>
                                                <option value="">Selecet Status</option>
                                                <option value="Publish">Publish</option>
                                                <option value="Pending">Pending</option>
                                            <?php else : ?>
                                                <?php if ($gallery['status'] == 'Publish') : ?>
                                                    <option value="Publish">Publish</option>
                                                    <option value="Pending">Pending</option>
                                                <?php elseif ($gallery['status'] == 'Pending') : ?>
                                                    <option value="Pending">Pending</option>
                                                    <option value="Publish">Publish</option>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-8">
                                    <div class="form-group">
                                        <label for="categId">Category</label>
                                        <select name="categId" id="categId" class="form-control">
                                            <?php foreach ($categories as $row) : ?>
                                            <option value=" <?= $row['id']; ?>"
                                                <?= $row['id'] == $gallery['id_category'] || $row['id'] == '' ? 'selected' : '' ?>>
                                                <?= $row['name']; ?>
                                            </option>
                                            <?php endforeach; ?>
                                        </select>
                                        <?= form_error('categId', '<small class="text-danger">', '</small>'); ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-3">
                                    <div class="form-group">
                                        <label for="gallery">Upload Image</label>
                                        <input type="file" name="image" class="form-control" id="gallery">
                                        <small class="text-info">* Max size 2 Mb</small><br>
                                         <small class="text-info">* Only for jpg | jpeg | png</small>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-1">
                                    <?php if($gallery['image']) : ?>
                                        <img width="100" class="img-thumbnail" src="<?= base_url('assets/images/gallery/thumbs/' . $gallery['image']) ?>">
                                    <?php else : ?>
                                        N/A
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="keywords">Keywords (SEO)</label>
                                <textarea class="form-control" name="keywords" id="keywords" rows="2" cols="10" ><?= $gallery['keywords']; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" name="description" id="editor1" rows="10" cols="80"
                                    style="visibility: hidden; display: none;"><?= $gallery['description']; ?></textarea>
                                <?= form_error('description', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success btn-flat pull-right">Edit</button>
                            <a href="<?= site_url('admin/gallery/'); ?>" class="btn pull-right">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>




    </section>