<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?= $title; ?></h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- flash data -->
    <?= $this->session->flashdata('message'); ?>
    <div class="row">
        <div class="col-md-4">
            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle"
                        src="<?= base_url('assets/images/user/' . $user['profile_picture']) ?>"
                        alt="User profile picture">
                    <h3 class="profile-username text-center"><?= $user['full_name']; ?></h3>
                    <p class="text-muted text-center"><?= $user['level']; ?></p>
                    <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Email</b> <a class="pull-right"><?= $user['email']; ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Username</b> <a class="pull-right"><?= $user['username']; ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Date Created</b> <a
                                    class="pull-right"><?= date('H:i:s | d/m/Y', strtotime($user['created_at'])); ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Date Updated</b>
                                <?php if($user['updated_at'] == NULL) : ?>
                                   <a href="#" class="pull-right">--</a> 
                                <?php else: ?>
                                    <a class="pull-right"><?= date('H:i:s | d/m/Y', strtotime($user['updated_at'])); ?></a>
                                <?php endif; ?>
                            </li>
                            <li class="list-group-item">
                                <b class="text-success">Last Login</b> <a
                                    class="pull-right"><?= date('H:i:s | d/m/Y', strtotime($user['login_at'])); ?></a>
                            </li>
                            <li class="list-group-item">
                                <b class="text-danger">Last Logout</b> 
                                <?php if($user['logout_at'] == NULL) : ?>
                                   <a href="#" class="pull-right">--</a> 
                                <?php else: ?>
                                    <a class="pull-right"><?= date('H:i:s | d/m/Y', strtotime($user['logout_at'])); ?></a>
                                <?php endif; ?>
                            </li>
                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-8">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#settings" data-toggle="tab">Settings</a></li>
                </ul>
                <div class="tab-content">
                    <!-- profile -->
                    <div class="active tab-pane" id="settings">
                        <form method="post" action="<?= base_url('admin/user/editProfile'); ?>" class="form-horizontal" enctype="multipart/form-data">
                        <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" class="form-control" value="<?= $this->security->get_csrf_hash(); ?>" >
                            <input type="hidden" name="id" value="<?= $user['id']; ?>" readonly>
                            <div class="form-group">
                                <label for="fullname" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="full_name" class="form-control" id="fullname"
                                        value="<?= $user['full_name']; ?>">
                                    <?= form_error('full_name', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="text" name="email" class="form-control" id="email"
                                        value="<?= $user['email']; ?>">
                                    <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="gender" class="col-sm-2 control-label">Gender</label>
                                <div class="col-sm-10">
                                    <select name="gender" class="form-control">
                                        <?php if (!$user['gender']) : ?>
                                        <option value="">Selecet Gender</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                        <?php else : ?>
                                        <?php if ($user['gender'] == 'Male') : ?>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                        <?php elseif ($user['gender'] == 'Female') : ?>
                                        <option value="Female">Female</option>
                                        <option value="Male">Male</option>
                                        <?php endif; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="image" class="col-sm-2 control-label">Image</label>
                                <div class="col-sm-10">
                                    <input type="file" name="image" class="form-control" id="image">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-flat btn-block btn-success">Edit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#settings" data-toggle="tab">Change Password</a></li>
                </ul>
                <div class="tab-content">
                    <!-- reset password -->
                    <div class="active tab-pane" id="reset">
                        <form method="post" action="<?= base_url('admin/user/changePassword/'); ?>"
                            class="form-horizontal">
                            <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" class="form-control" value="<?= $this->security->get_csrf_hash(); ?>" >
                            <input type="hidden" name="id" value="<?= $user['id']; ?>" readonly>
                            <div class="form-group">
                                <label for="oldpassword" class="col-sm-2 control-label">Current Password</label>
                                <div class="col-sm-10">
                                    <input type="password" name="oldPassword" class="form-control" id="oldpassword">
                                    <?= form_error('oldPassword', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="newPassword" class="col-sm-2 control-label">New Password</label>
                                <div class="col-sm-10">
                                    <input type="password" name="newPassword" class="form-control" id="newPassword">
                                    <?= form_error('newPassword', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="confPassword" class="col-sm-2 control-label">Confirm Password</label>
                                <div class="col-sm-10">
                                    <input type="password" name="confPassword" class="form-control"
                                        id="confPassword">
                                    <?= form_error('confPassword', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-danger btn-block btn-flat">Reset Password</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
