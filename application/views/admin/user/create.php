    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $title; ?>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <?= $this->session->flashdata('message'); ?>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <div class="col-xs-12 col-md-6 col-md-offset-3">
                            <form action="<?= site_url('admin/user/create/'); ?>" method="post">
                            <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" class="form-control" value="<?= $this->security->get_csrf_hash(); ?>" >
                                <div class="form-group has-feedback">
                                    <input type="text" name="full_name" class="form-control" placeholder="Full Name"
                                        value="<?= set_value('full_name'); ?>">
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    <?= form_error('full_name', '<small class="text-danger">', '</small>'); ?>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="text" name="email" class="form-control" placeholder="Email"
                                        value="<?= set_value('email'); ?>">
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="text" name="username" class="form-control" placeholder="Username"
                                        value="<?= set_value('username'); ?>">
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    <?= form_error('username', '<small class="text-danger">', '</small>'); ?>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="password" name="password1" class="form-control" placeholder="Password">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    <?= form_error('password1', '<small class="text-danger">', '</small>'); ?>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="password" name="password2" class="form-control" placeholder="Confirm Password">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    <?= form_error('password2', '<small class="text-danger">', '</small>'); ?>
                                </div>
                                <div class="row">
                                    <div class="col-xs-8">
                                        <a href="<?= site_url('admin/user/'); ?>">Cancel</a>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-xs-4">
                                        <button type="submit" class="btn btn-success btn-block btn-flat">Add User</button>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>