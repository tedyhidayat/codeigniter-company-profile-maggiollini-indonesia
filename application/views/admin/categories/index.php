    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $title; ?>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <?= $this->session->flashdata('message'); ?>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success">
                    <div class="box-header">
                        <a href="<?= site_url('admin/categories/create'); ?>" class="btn btn-success btn-sm btn-flat"><i
                                class="fa fa-plus"></i> Create New</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example2" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="20">NO.</th>
                                        <th>Title</th>
                                        <th>Position</th>
                                        <th width="120">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!$categories) : ?>
                                    <tr>
                                        <td class="text-center" colspan="6">Empty!</td>
                                    </tr>
                                    <?php else : ?>
                                    <?php $no = 1; foreach ($categories as $row): ?>
                                    <tr>
                                        <td><?= $no++; ?>.</td>
                                        <td><?= $row['name']; ?></td>
                                        <td><?= $row['position']; ?></td>
                                        <td>
                                            <a href="<?= site_url('admin/categories/edit/' . $row['id']); ?>"
                                                class="btn btn-info btn-sm btn-flat"><i class="fa fa-pencil"></i></a>
                                            <?php include('delete.php') ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>



    </section>
