    <!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?= $title; ?></h1>
</section>
<!-- Main content -->
<section class="content">
        <!-- flash data -->
        <?= $this->session->flashdata('message'); ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" action="<?= site_url('admin/projects/create'); ?>" enctype="multipart/form-data"
                        role="form">
                        <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" class="form-control" value="<?= $this->security->get_csrf_hash(); ?>" >
                        <div class="box-body">
                            <div class="row">
                                <div class="col xs-12 col-md-8">
                                    <div class="form-group">
                                        <label for="title">Project Name</label>
                                        <input type="text" name="projectName" class="form-control" id="title"
                                            placeholder="Project Name" value="<?= set_value('projectsName'); ?>">
                                        <?= form_error('projectName', '<small class="text-danger">', '</small>'); ?>
                                    </div>
                                </div>    
                                <div class="col xs-12 col-md-4">
                                    <div class="form-group">
                                        <label for="status">Status</label>
                                        <select name="status" id="status" class="form-control">
                                            <option value="Pending">Pending</option>
                                            <option value="Publish">Publish</option>
                                        </select>
                                    </div>
                                </div>    
                            </div>                            
                            <div class="row">
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <label for="category">Category</label>
                                        <select name="categoryId" id="category" class="form-control">
                                            <option>Select Category</option>
                                            <?php foreach ($categories as $row) : ?>
                                            <option value="<?= $row['id']; ?>"><?= $row['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <?= form_error('categoryId', '<small class="text-danger">', '</small>'); ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <div class="form-group">
                                        <label for="file">Upload Image</label>
                                        <input type="file" name="image" class="form-control" id="file">
                                        <small class="text-info">* Max size 2 Mb</small><br>
                                         <small class="text-info">* Only for jpg | jpeg | png</small>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="keywords">Keywords (SEO)</label>
                                <textarea class="form-control" name="keywords" id="keywords" rows="2" cols="10" ><?= set_value('keywords'); ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" name="description" id="editor1" rows="10" cols="80"
                                    style="visibility: hidden; display: none;"><?= set_value('description'); ?></textarea>
                                <?= form_error('description', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success btn-flat pull-right">Save</button>
                            <a href="<?= site_url('admin/projects/'); ?>" class="btn pull-right">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</section>
