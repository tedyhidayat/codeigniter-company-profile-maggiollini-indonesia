<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Configurations extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		security_access();
		$this->load->model('User_model', 'user');
	}

	// function show index general configuration 
    public function index() {
		// get data from models
		$data = [
			'title' 	=> 'Configurations',
			'user' 		=> $this->user->getUserBySession(),
			'config' 	=> $this->configs->getConfiguration(),
			'allConfig' => $this->configs->getAllConfigurations(),
		];
		// view index general configurations
        $this->templating->load('layouts/admin/wrapper', 'admin/config/index', $data);
	}
	
	// function create general configuration 
	public function create() {
		$data = [
			'title'  => 'Set Configurations',
			'user' 	 => $this->user->getUserBySession(),
			'config' => $this->configs->getConfiguration(),
		];
		// validation rules
		$this->form_validation->set_rules('tw', 'Twitter', 'trim');
		$this->form_validation->set_rules('fb', 'Facebook', 'trim');
		$this->form_validation->set_rules('wa', 'whatsapp', 'trim');
		$this->form_validation->set_rules('ig', 'Instagram', 'trim');
		$this->form_validation->set_rules('fax', 'Fax Number', 'required|trim');
		$this->form_validation->set_rules('tagline', 'Tagline', 'required|trim');
		$this->form_validation->set_rules('address', 'Address', 'required|trim');
		$this->form_validation->set_rules('siteUrl', 'Site URL', 'required|trim');
		$this->form_validation->set_rules('keywords', 'keywords', 'required|trim');
		$this->form_validation->set_rules('siteName', 'Site Name', 'trim|required');
		$this->form_validation->set_rules('metaText', 'Meta Text for SEO', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
		$this->form_validation->set_rules('metaText', 'Meta Text for SEO', 'required|trim');
		$this->form_validation->set_rules('telephone', 'Telephone Number', 'required|trim');
		$this->form_validation->set_rules('metaDesc', 'Meta Description for SEO', 'required|trim');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/config/create', $data);
		} else {
			// if validation success call model create from configuration_model
			$this->configs->create($data);
		}
	}

	// function edit general configuration 
	public function edit($id) {
		// get data from models
		$data = [
			'id' 		=> $id,
			'title' 	=> 'Edit Configurations',
			'user' 		=> $this->user->getUserBySession(),
			'config' 	=> $this->configs->getConfiguration(),
			'configId' 	=> $this->configs->getConfigurationsById($id),
		];		
		// validation rules
		$this->form_validation->set_rules('tw', 'Twitter', 'trim');
		$this->form_validation->set_rules('fb', 'Facebook', 'trim');
		$this->form_validation->set_rules('wa', 'whatsapp', 'trim');
		$this->form_validation->set_rules('ig', 'Instagram', 'trim');
		$this->form_validation->set_rules('fax', 'Fax Number', 'required|trim');
		$this->form_validation->set_rules('address', 'Address', 'required|trim');
		$this->form_validation->set_rules('tagline', 'Tagline', 'required|trim');
		$this->form_validation->set_rules('siteUrl', 'Site URL', 'required|trim');
		$this->form_validation->set_rules('keywords', 'keywords', 'required|trim');
		$this->form_validation->set_rules('siteName', 'Site Name', 'trim|required');
		$this->form_validation->set_rules('metaText', 'Meta Text for SEO', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
		$this->form_validation->set_rules('telephone', 'Telephone Number', 'required|trim');
		$this->form_validation->set_rules('metaText', 'Meta Text for SEO', 'required|trim');
		$this->form_validation->set_rules('metaDesc', 'Meta Description for SEO', 'required|trim');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/config/update', $data);
		} else {
			// if validation success call function edit from configuration_model
			$this->configs->edit($data);
		}
	}

	// function edit logo general configuration 
	public function editLogo($id) {
		// get data from models
		$data = [
			'id' 		=> $id,
			'title' 	=> 'Edit Configurations',
			'user' 		=> $this->user->getUserBySession(),
			'config' 	=> $this->configs->getConfiguration(),
			'configId'	=> $this->configs->getConfigurationsById($id),
		];
		// call function edit logo from configuration_model
		$this->configs->editLogo($data);
	}

	// function edit icon general configuration 
	public function editIcon($id) {
		// get data from models
		$data = [
			'id' 		=> $id,
			'title' 	=> 'Edit Configurations',
			'user' 		=> $this->user->getUserBySession(),
			'config' 	=> $this->configs->getConfiguration(),
			'configId' 	=> $this->configs->getConfigurationsById($id),
		];
		// call function edit icon from configuration_model
		$this->configs->editIcon($data);
	}

	public function reset($id) {
		$data = [
			'id' => $id,
			'configId' => $this->configs->getConfigurationsById($id)
		];
		$this->configs->reset($data);
	}

}

/* End of file Configurations.php */

?>