<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Careers extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		create_cookie();
		$this->load->model('Careers_model');
	}

	// function show  page
	public function index()	{
		// unset sessinon captcha if there
		$this->session->unset_userdata('admin_access');
		$this->session->unset_userdata('captchaImage');
		// identification
		$configurations = $this->configs->getConfiguration();

		// check config for title
		(!empty($configurations['siteName'])) ? $title1 = $this->uri->segment(1) : $title1 = "THDEV";
		(!empty($configurations['siteName'])) ? $title2 = $configurations['siteName'] : $title2 = "Web Developer";

		$data = [
			'configs' 		=> $this->configs->getConfiguration(),
			'careers' 		=> $this->Careers_model->getAllCareersActive(),
			'config_page' 	=> $this->config_page->getAllConfigPageActiveHeading(),
			'title' 		=> $title1.' | '.$title2,
        ];
        
        $this->templating->load('layouts/public/wrapper', 'careers', $data);
    }
    
    // function show detail page
	public function detail($slug)	{
		// identification
		$configurations = $this->configs->getConfiguration();
		$slug = $this->uri->segment(3);
		
		// check config for title
		(!empty($configurations['siteName'])) ? $title1 = $this->uri->segment(1) : $title1 = "THDEV";
		(!empty($configurations['siteName'])) ? $title2 = $configurations['siteName'] : $title2 = "Web Developer";
        
		$data = [
			'configs' 		=> $this->configs->getConfiguration(),
			'career' 		=> $this->Careers_model->getCareersBySlug($slug),
			'config_page' 	=> $this->config_page->getAllConfigPageActiveHeading(),
			'title' 		=> $title1.' | '.$title2,
        ];
        
        $this->templating->load('layouts/public/wrapper', 'detail_career', $data);
	}


}

/* End of file Careers.php */
/* Location: ./application/controllers/Index.php */