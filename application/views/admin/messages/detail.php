<button type="button" class="btn btn-info btn-sm btn-flat" data-toggle="modal"
    data-target="#detail<?= $row['id'] ?>">
    <i class="fa fa-eye"></i>
</button>

<div class="modal fade" id="detail<?= $row['id'] ?>">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">From : <?= $row['full_name']; ?>  (<?= $row['company_name']; ?>)</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-hover " style="padding:0;">
                                <tbody>
                                    <tr style="padding:0;">
                                        <th>Email</th>
                                        <th>Contact</th>
                                        <th>Inquiry</th>
                                        <th>Receive</th>
                                    </tr>
                                    <tr style="padding:0;">
                                        <td><small><?= strip_tags($row['email']) ?></small></td>
                                        <td><small><?= strip_tags($row['contact_number']) ?></small></td>
                                        <td><small><?= strip_tags($row['inquiry']) ?></small></td>
                                        <td><small><?= date("l, d F Y", strtotime($row['created_at'])); ?></small></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"><small><b> Address:</b> <?= strip_tags($row['company_address']) ?></small></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"><small><b> Subject:</b> <?= strip_tags($row['subject']) ?></small></td>
                                    </tr>
                                    <tr style="background-color: #fafafa;">
                                        <td colspan="4" style="padding-top: 10px;padding-bottom: 10px;">
                                            <h5><b>Message:</b></h5>
                                            <?= strip_tags($row['message']); ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn pull-right btn-danger btn-sm btn-flat" data-dismiss="modal">Close</a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->