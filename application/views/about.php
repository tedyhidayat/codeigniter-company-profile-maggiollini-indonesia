<?php if($configs) : ?>   
   <!-- Breadcrumbs -->
    <section id="breadcrumbs">
        <div class="container">
            <div class=" row justify-content-between align-items-center">
                <div class="col-12 col-md col-lg">
                    <h2 class="text-uppercase"><?= $this->uri->segment(1); ?></h2>
                </div>
                <div class="col-12 col-md col-lg">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= site_url('/'); ?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?= $this->uri->segment(1); ?></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    
    <!-- about -->
    <section id="about" class="">
        <?php if($banner) : ?>  
            <?php foreach($banner as $row_banner) : ?>
                <div class="img-head" style="background-image: url('<?= base_url('assets/images/gallery/'.$row_banner['image']) ?>');"></div>
            <?php endforeach; ?>
        <?php else: ?>  
            <div class="img-head" style="background-image: url('<?= base_url('assets/images/gallery/default.png') ?>');"></div>
        <?php endif; ?>  

        <div class="container">
            <!-- about company -->
            <?php if($about) : ?>  
            <article>
                <div class="row history" data-aos="fade-up">
                    <div class="col-12 intro-txt">
                        <?php foreach($about as $row_about) : ?>
                            <?php if($row_about['name'] == 'sejarah') : ?>
                                <!-- place header title here -->
                                <header>
                                    <h2 class="text-capitalize"><?= $row_about['title']; ?></h2>
                                </header>
                                <!-- palce article here -->
                                <P class="detail"><?= strip_tags($row_about['title']); ?></P>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        
                        <div class="row">
                            <?php foreach($about as $row_about) : ?>
                                <?php if($row_about['name'] == 'value') : ?>
                                    <div class="col-12 col-lg-6 mb-3">
                                        <div class="card mb-3 ">
                                            <div class="card-body d-flex align-items-start">
                                                <div class="icon-contact">
                                                    <span>
                                                        <i class="fas fa-universal-access"></i>
                                                    </span>
                                                </div>
                                                <div class="text-contact">
                                                    <h5 class="card-title text-uppercase"><?= $row_about['title']; ?></h5>
                                                    <p class="card-text">
                                                    <?= strip_tags($row_about['description']); ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                        
                    </div>
                </div>
            </article>
            <?php endif; ?>  

            <!-- team -->
            <?php if($team) : ?>  
            <article>
                <div class="row justify-content-center team">
                    <header class="col-12 mb-3">
                    <?php foreach($config_page as $row_page) : ?>
                        <?php if($row_page['page_position'] == 'Team') : ?>
                            <!-- title client -->
                            <h2 class="text-capitalize"><?= $row_page['title']; ?></h2>
                            <p><?= $row_page['description']; ?></p>
                        <?php endif; ?>
                    <?php endforeach; ?>  
                    </header>
                    <!-- team -->
                    <?php foreach($team as $row_team) : ?>
                        <div class="col-sm-6 col-lg-6 mb-4">
                            <div class="card" data-aos="fade-up" data-aos-delay="300" style="background-image: url('<?= base_url('assets/images/gallery/'.$row_team['image']) ?>');">
                                <!-- <div class="card-body">
                                    <a href="#" class="btn-socmed"><i class="fab fa-facebook"></i></a>
                                    <a href="#" class="btn-socmed"><i class="fab fa-whatsapp"></i></a>
                                    <a href="#" class="btn-socmed"><i class="fab fa-twitter"></i></a>
                                    <a href="#" class="btn-socmed"><i class="fab fa-linkedin"></i></a>
                                </div> -->
                                <div class="card-footer">
                                    <h6><?= $row_team['title']; ?></h6>
                                    <span><?= strip_tags($row_team['description']); ?></span>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </article>
            <?php endif; ?>  

            <!-- clients -->
            <?php if($partners) : ?>  
            <article>
                <div class="row clients justify-content-center" data-aos="fade-up">
                    <header class="col-12 mb-3">
                    <?php foreach($config_page as $row_page) : ?>
                        <?php if($row_page['page_position'] == 'Client') : ?>
                            <!-- title client -->
                            <h2 class="text-capitalize"><?= $row_page['title']; ?></h2>
                            <p><?= $row_page['description']; ?></p>
                        <?php endif; ?>
                    <?php endforeach; ?>    
                    </header>
                    <!-- item client -->
                    <?php foreach($partners as $row_partner) : ?>
                    <div class="col-6 col-lg-3 mb-4">
                        <div class="card">
                            <div class="card-body">
                                <img src="<?= base_url('assets/images/partners/'.$row_partner['image']); ?>" alt="<?= $row_partner['image']; ?>" title="<?= $row_partner['partner_name']; ?>"><br>
                                <span><?= $row_partner['partner_name']; ?></span>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>  
                </div>
            </article>
            <?php endif; ?>  
        </div>
    </section>
<?php else : ?>
    <section>
        <div class="container" style="min-height: 300px; ">
            <div class="row align-items-center">
                <div class="col-12 text-center">
                    <h2 class="mt-5">EMPTY!</h2>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
