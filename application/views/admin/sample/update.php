<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?= $title; ?></h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- flash data -->
    <?= $this->session->flashdata('message'); ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="<?= site_url('admin/sample/edit/' . $sample['id']); ?>"
                    enctype="multipart/form-data" role="form">
                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" class="form-control" value="<?= $this->security->get_csrf_hash(); ?>" >
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12 col-md-9">
                                <div class="form-group">
                                    <label for="sampleName">Sample Name</label>
                                    <input type="text" name="sampleName" class="form-control" id="sampleName"
                                        value="<?= $sample['sample_name']; ?>">
                                    <?= form_error('sampleName', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select name="status" id="status" class="form-control">
                                        <?php if (!$sample['status']) : ?>
                                            <option value="">Selecet Status</option>
                                            <option value="Publish">Publish</option>
                                            <option value="Pending">Pending</option>
                                        <?php else : ?>
                                            <?php if ($sample['status'] == 'Publish') : ?>
                                                <option value="Publish">Publish</option>
                                                <option value="Pending">Pending</option>
                                            <?php elseif ($sample['status'] == 'Pending') : ?>
                                                <option value="Pending">Pending</option>
                                                <option value="Publish">Publish</option>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label for="categId">Category</label>
                                    <select name="categId" id="categId" class="form-control">
                                        <?php foreach ($categories as $row) : ?>
                                        <option value=" <?= $row['id']; ?>"
                                            <?= $row['id'] == $sample['id_category'] || $row['id'] == '' ? 'selected' : '' ?>>
                                            <?= $row['name']; ?>
                                        </option>
                                        <?php endforeach; ?>
                                    </select>
                                    <?= form_error('categId', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="sample">Upload sample</label>
                                    <input type="file" name="image" class="form-control" id="sample">
                                    <small class="text-info">* Max size 2 Mb</small><br>
                                         <small class="text-info">* Only for jpg | jpeg | png</small>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-2">
                                <div class="form-group">
                                    <img class="img-thumbnail" src="<?= base_url('assets/images/projects/sample/thumbs/' . $sample['image']) ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="keywords">Keywords (SEO)</label>
                            <textarea class="form-control" name="keywords" id="keywords" rows="2" cols="10" ><?= $sample['keywords']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description" id="editor1" rows="10" cols="80"
                                style="visibility: hidden; display: none;"><?= $sample['description']; ?></textarea>
                            <?= form_error('description', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success btn-flat pull-right">Edit</button>
                        <a href="<?= site_url('admin/sample/'); ?>" class="btn pull-right">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>