<?php if($configs) : ?>   
    <!-- Breadcrumbs -->
    <section id="breadcrumbs">
        <div class="container">
            <div class=" row justify-content-between align-items-center">
                <div class="col-12 col-md col-lg">
                    <h2 class="text-uppercase"><?= $this->uri->segment(1); ?></h2>
                </div>
                <div class="col-12 col-md col-lg">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= site_url('/'); ?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?= $this->uri->segment(1); ?></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- archive -->
    <section id="archives" class="">
        <div class="container">
        <?php if($files) : ?>   
            <div class="row justify-content-start">
                <div class="col-12" data-aos="fade-up">
                    <!-- category files -->
                    <ul class="nav" id="pills-tab" role="tablist">
                        <?php $i=1; foreach($category as $row_category) : ?>
                            <li class="nav-item">
                                <a class="nav-link <?php if($i == 1) echo 'active'; ?>" id="pills-<?= $row_category['name']; ?>-tab" data-toggle="pill" href="#pills-<?= $row_category['name']; ?>" role="tab" aria-controls="pills-<?= $row_category['name']; ?>" aria-selected="true"><i class="fas fa-file"></i>
                                <?= $row_category['name']; ?></a>
                            </li>
                        <?php $i++; endforeach; ?>
                    </ul>
                    <!-- items gallery -->
                    <div class="tab-content p-0" id="pills-tabContent">
                        <!-- tab file -->
                        <?php $i=1; foreach($category as $row_category) : ?>
                            <div class="tab-pane fade show <?php if($i == 1) echo 'active'; ?>" id="pills-<?= $row_category['name']; ?>" role="tabpanel" aria-labelledby="pills-<?= $row_category['name']; ?>-tab">
                                <div class="row justify-content-center">
                                    <!-- file -->
                                    <?php $i=1; foreach($files as $row_file) : ?>
                                        <?php if($row_file['id_category'] === $row_category['id'] && !empty($files)): ?>
                                            <div class="col-6 col-lg-3 mb-3">
                                                <div class="card" data-aos="fade-up" data-aos-delay="300" style="background-image: url('<?= base_url('assets/images/file.jpg'); ?>');">
                                                    <div class="card-body">
                                                        <a href=" <?= site_url('archives/download/'.$row_file['file_name']); ?>" title="<?= $row_file['title']; ?>" class="btn-download"><i class="fas fa-download"></i> Download</a>
                                                    </div>
                                                    <div class="card-footer">
                                                        <h6 title="document"><?= $row_file['title']; ?></h6>
                                                        <small><?= date('d F Y', strtotime($row_file['created_at'])); ?></small>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        <?php $i++; endforeach; ?>
                    </div>
                </div>
            </div>
        <?php else : ?>
            <div class="row">
                <div class="col-12">
                    <div class="card text-center">
                        <div class="card-body">
                            <h4>File belum tersedia.</h4>
                            <p><?=$configs['siteUrl'];?></p>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        </div>
    </section>    
<?php else : ?>
    <section>
        <div class="container" style="min-height: 300px; ">
            <div class="row align-items-center">
                <div class="col-12 text-center">
                    <h2 class="mt-5">EMPTY!</h2>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>