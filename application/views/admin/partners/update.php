<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?= $title; ?></h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- flash data -->
    <?= $this->session->flashdata('message'); ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="<?= site_url('admin/partners/edit/' . $partner['id']); ?>" enctype="multipart/form-data">
                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" class="form-control" value="<?= $this->security->get_csrf_hash(); ?>" >
                    <div class="box-body">
                        <input type="hidden" name="id" value="<?= $partner['id']; ?>">
                        <div class="row">
                            <div class="col-xs-12 col-md-8">
                                <div class="form-group">
                                    <label for="partnerName">Partner Name</label>
                                    <input type="text" name="partnerName" class="form-control" id="partnerName"
                                        value="<?= $partner['partner_name']; ?>">
                                    <?= form_error('partnerName', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select name="status" id="status" class="form-control">
                                        <?php if (!$partner['status']) : ?>
                                        <option value="">Selecet Status</option>
                                        <option value="Publish">Publish</option>
                                        <option value="Pending">Pending</option>
                                        <?php else : ?>
                                        <?php if ($partner['status'] == 'Publish') : ?>
                                        <option value="Publish">Publish</option>
                                        <option value="Pending">Pending</option>
                                        <?php elseif ($partner['status'] == 'Pending') : ?>
                                        <option value="Pending">Pending</option>
                                        <option value="Publish">Publish</option>
                                        <?php endif; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label for="deputy">Deputy</label>
                                    <input type="text" name="deputy" class="form-control" id="deputy" placeholder="Deputy"
                                        value="<?= $partner['deputy_name']; ?>">
                                    <?= form_error('deputy', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" name="email" class="form-control" id="email"
                                        value="<?= $partner['email']; ?>">
                                    <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label for="phoneNumber">Phone Number</label>
                                    <input type="text" name="phoneNumber" class="form-control" id="phoneNumber"
                                        value="<?= $partner['phone_number']; ?>">
                                    <?= form_error('phoneNumber', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-3 col-md-2">
                                    <img class="img-thumbnail" src="<?= base_url('assets/images/partners/'.$partner['image']); ?>" alt="">
                                </div>
                                <div class="col-xs-9 col-md-10">
                                    <label for="image">Upload Image</label>
                                    <input type="file" name="image" class="form-control" id="image">
                                    <small class="text-info">* Max size 1 Mb</small><br>
                                         <small class="text-info">* Only for jpg | jpeg | png</small>
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success btn-flat pull-right">Edit</button>
                        <a href="<?= site_url('admin/partners/'); ?>" class="btn pull-right">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>