<?php header('Content-type: application/xml; charset="ISO-8859-1"',true);  ?>

<urlset xmlns="http://www.sitemap.org/schemas/sitemap/0.9">
    <url>
        <loc><?= base_url(); ?></loc>
        <priority>1.0</priority>
    </url>

    <url>
        <loc><?= base_url('about/'); ?></loc>
        <priority>0.5</priority>
    </url>
    <url>
        <loc><?= base_url('services/'); ?></loc>
        <priority>0.5</priority>
    </url>
    <url>
        <loc><?= base_url('careers/'); ?></loc>
        <priority>0.5</priority>
    </url>
    <url>
        <loc><?= base_url('gallery/'); ?></loc>
        <priority>0.5</priority>
    </url>
    <url>
        <loc><?= base_url('archives/'); ?></loc>
        <priority>0.5</priority>
    </url>
    <url>
        <loc><?= base_url('contact/'); ?></loc>
        <priority>0.5</priority>
    </url>

    <!-- Sitemap news -->
    <?php foreach ($links['posts'] as $row) : ?>
        <url>
            <loc><?= base_url('news/post/') . $row['slug'] ?></loc>
            <priority>0.5</priority>
            <changefreq>daily</changefreq>
            <lastmod><?= date('d F Y', strtotime($row['created_at'])); ?></lastmod>
        </url>
    <?php endforeach; ?>
    
    <!-- Sitemap category news -->
    <?php foreach ($links['category_news'] as $row) : ?>
        <url>
            <loc><?= base_url('news/category/') . $row['slug'] ?></loc>
            <priority>0.5</priority>
            <changefreq>daily</changefreq>
        </url>
    <?php endforeach; ?>

    <!-- Sitemap services -->
    <?php foreach ($links['services'] as $row) : ?>
        <url>
            <loc><?= base_url('services/detail/') . $row['slug'] ?></loc>
            <priority>0.5</priority>
            <changefreq>daily</changefreq>
            <lastmod><?= date('d F Y', strtotime($row['created_at'])); ?></lastmod>
        </url>
    <?php endforeach; ?>
    
    <!-- Sitemap projects -->
    <?php foreach ($links['projects'] as $row) : ?>
        <url>
            <loc><?= base_url('projects/detail-project/') . $row['slug'] ?></loc>
            <priority>0.5</priority>
            <changefreq>daily</changefreq>
            <lastmod><?= date('d F Y', strtotime($row['created_at'])); ?></lastmod>
        </url>
    <?php endforeach; ?>
    
    <!-- Sitemap projects -->
    <?php foreach ($links['sample'] as $row) : ?>
        <url>
            <loc><?= base_url('projects/detail-sample/') . $row['slug'] ?></loc>
            <priority>0.5</priority>
            <changefreq>daily</changefreq>
            <lastmod><?= date('d F Y', strtotime($row['created_at'])); ?></lastmod>
        </url>
    <?php endforeach; ?>
    
    <!-- Sitemap careers -->
    <?php foreach ($links['careers'] as $row) : ?>
        <url>
            <loc><?= base_url('careers/detail/') . $row['slug'] ?></loc>
            <priority>0.5</priority>
            <changefreq>daily</changefreq>
            <lastmod><?= date('d F Y', strtotime($row['created_at'])); ?></lastmod>
        </url>
    <?php endforeach; ?>

</urlset>