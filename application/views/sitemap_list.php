    <!-- Breadcrumbs -->
    <section id="breadcrumbs">
        <div class="container">
            <div class=" row justify-content-between align-items-center">
                <div class="col-12 col-md col-lg">
                    <h2 class="text-uppercase"><?= $this->uri->segment(1); ?></h2>
                </div>
                <div class="col-12 col-md col-lg">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= site_url('/'); ?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?= $this->uri->segment(1); ?></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Contact -->
    <section id="contact" class="">
        <div class="container">
            <div class="row d-flex align-content-start flex-wrap pt-4">
                <!-- navigaion -->
                <div class="col-md-4 mb-3">
                    <div class="list-group" style="border-radius:0;">
                        <a href="#" class="list-group-item list-group-item-action bg-info text-white" style="border-radius:0; font-size: 14px;">
                            <h5 style="margin-bottom: 2px; font-size: 17px;">Navigasi</h5>
                        </a>
                        <a href="<?= site_url('/'); ?>" class="list-group-item list-group-item-action" style="border-radius:0; font-size: 14px;"><i class="fas fa-link"></i> Home</a>
                        <a href="<?= site_url('about/'); ?>" class="list-group-item list-group-item-action" style="border-radius:0; font-size: 14px;"><i class="fas fa-link"></i> About</a>
                        <a href="<?= site_url('services/'); ?>" class="list-group-item list-group-item-action" style="border-radius:0; font-size: 14px;"><i class="fas fa-link"></i> Services</a>
                        <a href="<?= site_url('projects/finished-projects/'); ?>" class="list-group-item list-group-item-action" style="border-radius:0; font-size: 14px;"><i class="fas fa-link"></i> Finished Projects</a>
                        <a href="<?= site_url('projects/sample/'); ?>" class="list-group-item list-group-item-action" style="border-radius:0; font-size: 14px;"><i class="fas fa-link"></i> Sample Production</a>
                        <a href="<?= site_url('gallery/'); ?>" class="list-group-item list-group-item-action" style="border-radius:0; font-size: 14px;"><i class="fas fa-link"></i> Gallery</a>
                        <a href="<?= site_url('careers/'); ?>" class="list-group-item list-group-item-action" style="border-radius:0; font-size: 14px;"><i class="fas fa-link"></i> Careers</a>
                        <a href="<?= site_url('news/'); ?>" class="list-group-item list-group-item-action" style="border-radius:0; font-size: 14px;"><i class="fas fa-link"></i> News</a>
                        <a href="<?= site_url('archives/'); ?>" class="list-group-item list-group-item-action" style="border-radius:0; font-size: 14px;"><i class="fas fa-link"></i> Archives</a>
                        <a href="<?= site_url('contact/'); ?>" class="list-group-item list-group-item-action" style="border-radius:0; font-size: 14px;"><i class="fas fa-link"></i> Contact</a>
                        <a href="<?= site_url('sitemap/'); ?>" class="list-group-item list-group-item-action" style="border-radius:0; font-size: 14px;"><i class="fas fa-link"></i> Sitemap</a>
                    </div>
                </div>
                
                <!-- services & category & careers & sample -->
                <div class="col-md-4 mb-3">
                    <!--  -->
                    <div class="list-group mb-3" style="border-radius:0;">
                        <a href="#" class="list-group-item list-group-item-action bg-info text-white" style="border-radius:0; font-size: 14px;">
                            <h5 style="margin-bottom: 2px; font-size: 17px;">Servis Kami</h5>
                        </a>
                        <?php foreach($services as $row): ?>
                        <a href="<?= site_url('/services/detail/'.$row['slug']); ?>" class="list-group-item list-group-item-action" style="border-radius:0; font-size: 14px;"><i class="fas fa-link"></i> <?= $row['title']; ?></a>
                        <?php endforeach; ?>
                    </div>
                    <!--  -->
                    <div class="list-group mb-3" style="border-radius:0;">
                        <a href="#" class="list-group-item list-group-item-action bg-info text-white" style="border-radius:0; font-size: 14px;">
                            <h5 style="margin-bottom: 2px; font-size: 17px;">Kategori Berita</h5>
                        </a>
                        <?php foreach($category_news as $row): ?>
                        <a href="<?= site_url('/news/category/'.$row['slug']); ?>" class="list-group-item list-group-item-action" style="border-radius:0; font-size: 14px;"><i class="fas fa-link"></i> <?= $row['name']; ?></a>
                        <?php endforeach; ?>
                    </div>
                    <!--  -->
                    <div class="list-group mb-3" style="border-radius:0;">
                        <a href="#" class="list-group-item list-group-item-action bg-info text-white" style="border-radius:0; font-size: 14px;">
                            <h5 style="margin-bottom: 2px; font-size: 17px;">Karir</h5>
                        </a>
                        <?php foreach($careers as $row): ?>
                        <a href="<?= site_url('/careers/detail/'.$row['slug']); ?>" class="list-group-item list-group-item-action" style="border-radius:0; font-size: 14px;"><i class="fas fa-link"></i> <?= $row['title']; ?></a>
                        <?php endforeach; ?>
                    </div>
                    <!--  -->
                    <div class="list-group" style="border-radius:0;">
                        <a href="#" class="list-group-item list-group-item-action bg-info text-white" style="border-radius:0; font-size: 14px;">
                            <h5 style="margin-bottom: 2px; font-size: 17px;">Sample Production</h5>
                        </a>
                        <?php foreach($sample as $row): ?>
                        <a href="<?= site_url('/projects/detail-sample/'.$row['slug']); ?>" class="list-group-item list-group-item-action" style="border-radius:0; font-size: 14px;"><i class="fas fa-link"></i> <?= $row['sample_name']; ?></a>
                        <?php endforeach; ?>
                    </div>
                </div>
                
                <!-- news & projects -->
                <div class="col-md-4 mb-3">
                    <div class="list-group mb-3" style="border-radius:0;">
                        <a href="#" class="list-group-item list-group-item-action bg-info text-white" style="border-radius:0; font-size: 14px;">
                            <h5 style="margin-bottom: 2px; font-size: 17px;">News</h5>
                        </a>
                        <?php foreach($posts as $row): ?>
                        <a href="<?= site_url('/news/post/'.$row['slug']); ?>" class="list-group-item list-group-item-action" style="border-radius:0; font-size: 14px;"><i class="fas fa-link"></i> <?= $row['title']; ?></a>
                        <?php endforeach; ?>
                    </div>
                    <!-- projects -->
                    <div class="list-group mb-3" style="border-radius:0;">
                        <a href="#" class="list-group-item list-group-item-action bg-info text-white" style="border-radius:0; font-size: 14px;">
                            <h5 style="margin-bottom: 2px; font-size: 17px;">Finished Projects</h5>
                        </a>
                        <?php foreach($projects as $row): ?>
                        <a href="<?= site_url('/projects/detail-project/'.$row['slug']); ?>" class="list-group-item list-group-item-action" style="border-radius:0; font-size: 14px;"><i class="fas fa-link"></i> <?= $row['project_name']; ?></a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>