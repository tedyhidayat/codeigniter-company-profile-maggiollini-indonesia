<!-- navbar -->
<nav class="navbar navbar-expand-lg sticky-top navbar">
    <div class="container">
        <?php if($configs): ?>
            <a class="navbar-brand" href="<?= base_url(); ?>">
                <img src="<?= base_url('assets/images/configurations/'.$configs['logo']); ?>" alt="<?= $configs['siteName']; ?>" title="<?= $configs['siteName']?>" class="logo">
            </a>
        <?php else: ?>
            <a class="navbar-brand" href="#">
                <h3>THDEVELOPER</h3>
            </a>
        <?php endif; ?>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-align-justify"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <?php if($configs): ?>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item <?= ($this->uri->segment(1) == '') ? 'active' : '' ?>">
                    <a class="nav-link" href="<?= base_url('/'); ?>">HOME</a>
                </li>
                <li class="nav-item <?= ($this->uri->segment(1) == 'about') ? 'active' : '' ?>">
                    <a class="nav-link" href="<?= base_url('/about/'); ?>">ABOUT</a>
                </li>
                <li class="nav-item <?= ($this->uri->segment(1) == 'services') ? 'active' : '' ?>">
                    <a class="nav-link" href="<?= base_url('/services/'); ?>">SERVICES</a>
                </li>
                <li class="nav-item dropdown <?= ($this->uri->segment(1) == 'projects') ? 'active' : '' ?>">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        PROJECTS
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item <?= ($this->uri->segment(2) == 'finished-projects') || ($this->uri->segment(2) == 'detail-project') ? 'active' : '' ?>" href="<?= base_url('/projects/finished-projects/'); ?>">FINISHED PROJECTS</a>
                        <a class="dropdown-item <?= ($this->uri->segment(2) == 'sample') || ($this->uri->segment(2) == 'detail-sample') ? 'active' : '' ?>" href="<?= base_url('/projects/sample/'); ?>">SAMPLE PRODUCTION</a>
                    </div>
                </li>
                <li class="nav-item <?= ($this->uri->segment(1) == 'gallery') ? 'active' : '' ?>">
                    <a class="nav-link" href="<?= base_url('/gallery/'); ?>">GALLERY</a>
                </li>
                <li class="nav-item <?= ($this->uri->segment(1) == 'careers') ? 'active' : '' ?>">
                    <a class="nav-link" href="<?= base_url('/careers/'); ?>">CAREERS</a>
                </li>
                <li class="nav-item <?= ($this->uri->segment(1) == 'news') ? 'active' : '' ?>">
                    <a class="nav-link" href="<?= base_url('/news/'); ?>">NEWS</a>
                </li>
                <li class="nav-item <?= ($this->uri->segment(1) == 'archives') ? 'active' : '' ?>">
                    <a class="nav-link" href="<?= base_url('/archives/'); ?>">ARCHIVES</a>
                </li>
                <li class="nav-item <?= ($this->uri->segment(1) == 'contact') ? 'active' : '' ?>">
                    <a class="nav-link" href="<?= base_url('/contact/'); ?>">CONTACT</a>
                </li>
            </ul>
            <?php else: ?>
                <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Setting your website now!</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="<?= site_url('maggioteam'); ?>">Click here</a>
                </li>
            </ul>
            <?php endif; ?>
        </div>
    </div>
</nav>
