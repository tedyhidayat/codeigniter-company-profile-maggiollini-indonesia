<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category_projects extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		security_access();
		$this->load->model('User_model');
		$this->load->model('Category_projects_model');
	}

	// function show index category projects
    public function index() {
		// get data from models
		$data = [
			'title' 			=> 'Category projects',
			'config' 			=> $this->configs->getConfiguration(),
			'user' 				=> $this->User_model->getUserBySession(),
			'category_projects' => $this->Category_projects_model->getAllCategoryprojects(),
		];
		// view index category projects
        $this->templating->load('layouts/admin/wrapper', 'admin/category_projects/index', $data);
	}
	
	// function create category projects page
	public function create() {
		// get data from models
		$data = [
			'title' 	=> 'Add Category Projects',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
		];
		// validation rules
		$this->form_validation->set_rules('categName', 'category', 'trim|required|is_unique[tbl_category_projects.name]', [
			'is_unique' => 'Category ' . $this->input->post('categName') . ' already exist!'
		]);
		$this->form_validation->set_rules('position', 'position', 'required|numeric|is_unique[tbl_category_projects.position]', [
			'is_unique' => 'Position already exist!'
		]);
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/category_projects/create', $data);
		} else {
			// if validation success call model create from category_projects_model
			$this->Category_projects_model->create($data);
		}
	}

	// function edit category projects page
	public function edit($id) {
		// get data from models
		$data = [
			'id' 		=> $id,
			'title' 	=> 'Edit Category projects',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'category' 	=> $this->Category_projects_model->getCategoryprojectsById($id),
		];
		// validation rules
		$this->form_validation->set_rules('categName', 'category name', 'trim|required');
		$this->form_validation->set_rules('position', 'position', 'required|trim|numeric');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/category_projects/update', $data);
		} else {
			// if validation success call model create from category_projects_model
			$this->Category_projects_model->edit($data);
		}
	}

	// function delete category projects
	public function delete($id) {
		// get data from modles
		$data = ['id' => $id, 'category' => $this->Category_projects_model->getCategoryprojectsById($id)];
		// call function delete from model Category_projects_model
		$this->Category_projects_model->delete($data);
	}
	
}

/* End of file Category_projects.php */

?>