<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Config_page extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		security_access();
		$this->load->model('User_model');
		$this->load->model('Config_page_model');
	}

	// function show index Config_page page
    public function index(){
		// get data from models
		$data = [
			'title' 	  	=> 'Configuration Page',
			'config' 	  	=> $this->configs->getConfiguration(),
			'user' 			=> $this->User_model->getUserBySession(),
			'config_page'   => $this->Config_page_model->getAllConfigPage(),
		];
		// view index Config_page page
        $this->templating->load('layouts/admin/wrapper', 'admin/config_page/index', $data);
	}
	
	// function create index Config_page page
	public function create() {
		// get data from models
		$data = [
			'title' 	=> 'Add Configuration Page',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
		];
		// validation rules
		$this->form_validation->set_rules('image', 'Image', 'trim');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('page_position', 'Page Position', 'trim|required');
		$this->form_validation->set_rules('type', 'Type', 'trim|required');
		$this->form_validation->set_rules('description', 'Description', 'trim');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/config_page/create', $data);
		} else {
			// if validation success call function create from Config_page_model
			$this->Config_page_model->create($data);
		}
	}

	// function edit index Config_page page
	public function edit($id) {
		// get data from models
		$data = [
				'id' 		=> $id,
				'title' 	=> 'Edit Configuration Page',
				'config' 	=> $this->configs->getConfiguration(),
				'user' 		=> $this->User_model->getUserBySession(),
			'config_page'	=> $this->Config_page_model->getConfigPageById($id),
		];
		// validation rules
		$this->form_validation->set_rules('image', 'image', 'trim');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('description', 'Description', 'trim');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/config_page/update', $data);
		} else {
			// if validation success call function edit from  Config_page_model
			$this->Config_page_model->edit($data);
		}
	}

	// function delete index Config_page page
	public function delete($id) {
		// get data from models
		$data = ['id' => $id, 'config_page' => $this->Config_page_model->getConfigPageById($id)];
		// call function delete from Config_page_models
		$this->Config_page_model->delete($data);
	}
	
}

/* End of file Config_page.php */

?>
