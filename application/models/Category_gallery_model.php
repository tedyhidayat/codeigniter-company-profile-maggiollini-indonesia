<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_gallery_model extends CI_Model {

	// function get all data category gallery
	public function getAllCategoryGallery() {
		return $this->db->get('tbl_category_gallery')->result_array();
	}
	
	// function get data category by ID
	public function getCategoryGalleryById($id) {
		return $this->db->get_where('tbl_category_gallery', ['id' => $id])->row_array();
	}

	// function create data category gallery
	public function create($data) {	
		// get data from input form
		$data = [
			'position' 	=> $this->input->post('position', true),
			'name' 		=> $this->input->post('categName', true),
			'slug' 		=> url_title($this->input->post('categName', true), 'dash', true),
		];
		// query insert
		$this->db->insert('tbl_category_gallery', $data);
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to insert data!</div>');
			redirect('admin/category_gallery/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Success!</div>');
			redirect('admin/category_gallery/');
		}
		
	}

	// function edit data category gallery
	public function edit($data) {
		// get data from input form
		$position 	= $this->input->post('position', true);
		$name 		= $this->input->post('categName', true);
		$slug 		= url_title($this->input->post('categName', true), 'dash', true);
		// query update
		$this->db->set('name', $name);
		$this->db->set('slug', $slug);
		$this->db->set('position', $position);
		$this->db->where('id', $data['id']);
		$this->db->update('tbl_category_gallery');
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to edit data!</div>');
			redirect('admin/category_gallery/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your category has been updated!</div>');
			redirect('admin/category_gallery/');
		}
		
	}

	// function delete data category gallery
	public function delete($data) {
		// query delete
		$this->db->where('id', $data['id']);
		$this->db->delete('tbl_category_gallery');
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to deleting data!</div>');
			redirect('admin/category_gallery/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your category has been delete!</div>');
			redirect('admin/category_gallery/');
		}
		
	}

}

/* End of file Category_gallery_model.php */
/* Location: ./application/models/Category_gallery_model.php */