<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $title; ?></title>
    <!-- ico -->
    <?php if($config) : ?>
    <link rel="shortcut icon" href="<?= base_url('assets/images/configurations/'.$config['icon']) ?>" type="image/x-icon">
    <?php endif; ?>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet"
        href="<?= base_url('assets/backend/'); ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet"
        href="<?= base_url('assets/backend/'); ?>bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('assets/backend/'); ?>dist/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition login-page">
    
    <div class="row">
        <div class="col-xs-12 col-md-4 col-md-offset-4" style="margin-top: 80px;">
            <h2 class="text-center text-red" style="font-size:100px ;"><i class="fa fa-warning text-red"></i><br> 404</h2>
        </div>
        <div class="col-xs-12 col-md-4 col-md-offset-4 text-center">
            <h3>Oops! Page not found.</h3>
            <p>We could not find the page you were looking for.</p>
            <a href="<?= base_url('admin/dashboard'); ?>" class="btn btn-success" style="margin-top:10px;"><i class="fa fa-refresh"></i>&nbsp; Back to Homepage</a>
        </div>
    </div>

    <!-- jQuery 3 -->
    <script src="<?= base_url('assets/backend/'); ?>bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?= base_url('assets/backend/'); ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

</body>

</html>
