<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_projects_model extends CI_Model {

	// function get all data category projects
	public function getAllCategoryprojects() {
		return $this->db->get('tbl_category_projects')->result_array();
	}
	
	// function get one data category project by ID
	public function getCategoryprojectsById($id) {
		return $this->db->get_where('tbl_category_projects', ['id' => $id])->row_array();
	}

	// function create data category project
	public function create($data) {	
		// get data from input form
		$data = [
			'position'  => $this->input->post('position', true),
			'name' 		=> $this->input->post('categName', true),
			'slug' 		=> url_title($this->input->post('categName', true), 'dash', true),
		];
		// query insert
		$this->db->insert('tbl_category_projects', $data);
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to creating data!</div>');
			redirect('admin/category_projects/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Success!</div>');
			redirect('admin/category_projects/');
		}
		
	}

	// function edit data category projects
	public function edit($data) {
		// get data from input form
		$position 	= $this->input->post('position', true);
		$name 		= $this->input->post('categName', true);
		$slug 		= url_title($this->input->post('categName', true), 'dash', true);
		// query update
		$this->db->set('name', $name);
		$this->db->set('slug', $slug);
		$this->db->set('position', $position);
		$this->db->where('id', $data['id']);
		$this->db->update('tbl_category_projects');
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to edit data!</div>');
			redirect('admin/category_projects/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your category has been updated!</div>');
			redirect('admin/category_projects/');
		}
		
	}

	// function delete data category projects
	public function delete($data) {
		// query delete
		$this->db->where('id', $data['id']);
		$this->db->delete('tbl_category_projects');
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Category <b><u> Failed to delete'. $data['category']['name'] .'</u></b> !</div>');
			redirect('admin/category_projects/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Category <b><u>'. $data['category']['name'] .'</u></b> has been delete!</div>');
			redirect('admin/category_projects/');
		}
		
	}

}

/* End of file Category_projects_model.php */
/* Location: ./application/models/Category_projects_model.php */