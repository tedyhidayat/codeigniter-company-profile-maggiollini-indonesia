<?php if($configs) : ?>      
    <!-- Breadcrumbs -->
    <section id="breadcrumbs">
        <div class="container">
            <div class=" row justify-content-between align-items-center">
                <div class="col-12 col-md col-lg">
                    <h2 class="text-uppercase"><?= $this->uri->segment(1); ?></h2>
                </div>
                <div class="col-12 col-md col-lg">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= site_url('/'); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?= site_url('/projects/'.$this->uri->segment(2)); ?>"><?= $this->uri->segment(1); ?></a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?= $this->uri->segment(2); ?></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Recent Projects -->
    <section id="projects" class="">
        <div class="container-fluid">
        <?php if($sample) : ?>   
            <header>
            <?php foreach($config_page as $row_page) : ?>
                <?php if($row_page['page_position'] == 'Sample Production'): ?>
                <h3 class="text-capitalize"><?= $row_page['title']; ?></h3>
                <p><?= $row_page['description']; ?></p>
                <?php endif; ?>
            <?php endforeach; ?>

            </header>
            <div class="row justify-content-center">
                <!-- controller filter -->
                <div class="col-12">
                    <ul class="nav justify-content-center">
                        <li class="nav-item">
                          <a data-filter="all" class="btn nav-link active">All</a>
                        </li>
                        <?php foreach($category as $row_category) : ?>
                        <li class="nav-item">
                          <a data-filter="<?= $row_category['name']; ?>" class="btn nav-link"><?= $row_category['name']; ?></a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <!-- data filter gallery -->
                <div class="container-fluid">
                    <div class="filter-container" data-aos="fade-up">
                        <!-- item filter -->
                        <?php foreach($sample as $row_sample) : ?>
                        <div class="col-sm-6 col-md-6 col-lg-4 box-projects filtr-item" data-category="all,<?= $row_sample['name']; ?>">
                            <div class="card img-item web">
                                <div class="card-body p-0">
                                    <img src="<?= base_url('assets/images/projects/sample/thumbs/'.$row_sample['image']); ?>" class="card-img-top img-projects" alt="<?= $row_sample['sample_name']; ?>" title="<?= $row_sample['sample_name']; ?>">
                                    <div class="box-control d-flex justify-content-between">
                                        <div class="col-8">
                                            <h5><?= $row_sample['sample_name']; ?></h5>
                                            <span><?= $row_sample['name']; ?></span>
                                        </div>
                                        <div class="col-4 d-flex flex-row-reverse align-items-center">
                                            <a href="<?= site_url('/projects/detail-sample/'.$row_sample['slug']); ?>" class="btn-control-project">
                                                <i class="fas fa-info-circle alt"></i>
                                            </a>
                                            <a href="<?= base_url('assets/images/projects/sample/'.$row_sample['image']); ?>" class="btn-control-project venobox preview-link" data-gall="galleryProject" title="<?= $row_sample['sample_name']; ?>">
                                                <i class="fas fa-search-plus alt"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>

                <!-- pagination -->
                <div class="col-12 mt-3 text-center">
                    <?php if(isset($pagination)) : ?>
                        <nav aria-label="Page navigation example" class="limiting-data">
                            <?= $pagination; ?>
                        </nav>
                    <?php endif; ?>
                </div>
            </div>
        <?php else : ?>
            <div class="container">
                <div class="row">
                    <div class="col-12 mb-5">
                        <div class="card text-center" style="border: 1px solid #ddd;">
                            <div class="card-body">
                                <h4>Gambar kosong.</h4>
                                <p><?= $configs['siteUrl']; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        </div>
    </section>
<?php else : ?>
    <section>
        <div class="container" style="min-height: 300px; ">
            <div class="row align-items-center">
                <div class="col-12 text-center">
                    <h2 class="mt-5">EMPTY!</h2>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>