<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?=$title;?></h1>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-solid">
                <div class="box-body">
                    <h6 class="box-title pull-left">
                        Welcome 
                        <b>
                            <span class="text-uppercase">
                                <u><?= $this->session->userdata('full_name'); ?></u>
                            </span>
                        </b>
                    </h6>
                    <h6 class="box-title pull-right"> <span class="pull-right"><?= date('l, d F Y') ?></span></h6>
                </div>
            </div>
        </div>
    </div>
    <!-- boxes -->
    <div class="row">
        <div class="col-lg-3 col-xs-4">
            <div class="small-box bg-info">
                <div class="inner">
                    <h3><?= $count_partners; ?></h3>
                    <p>Partners</p>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <a href="<?= site_url('admin/partners/'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-4">
            <div class="small-box bg-info">
                <div class="inner">
                    <h3><?= $count_projects; ?></h3>
                    <p>Finshed Projects</p>
                </div>
                <div class="icon">
                    <i class="fa fa-check"></i>
                </div>
                <a href="<?= site_url('admin/projects/'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-4">
            <div class="small-box bg-info">
                <div class="inner">
                    <h3><?= $count_sample; ?></h3>
                    <p>Sample Products</p>
                </div>
                <div class="icon">
                    <i class="fa fa-briefcase"></i>
                </div>
                <a href="<?= site_url('admin/sample/'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-4">
            <div class="small-box bg-info">
                <div class="inner">
                    <h3><?= $count_gallery; ?></h3>
                    <p>Gallery</p>
                </div>
                <div class="icon">
                    <i class="fa fa-image"></i>
                </div>
                <a href="<?= site_url('admin/gallery/'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <!-- row message -->
    <div class="row">
        <div class="col-xs-12">
        <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-envelope"></i> Messages</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                <?= $this->session->flashdata('message'); ?>
                <div class="table-responsive">
                <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="3%">NO.</th>
                                    <th>From</th>
                                    <th>Inquiry</th>
                                    <th>Subject</th>
                                    <th width="30%">Message</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!$messages) : ?>
                                <tr>
                                    <td class="text-center" colspan="6">Empty!</td>
                                </tr>
                                <?php else : ?>
                                <?php $no = 1; foreach ($messages as $row): ?>
                                <tr>
                                    <td><?= $no++; ?>.</td>
                                    <td>
                                        <b><?= $row['full_name']; ?></b><br>
                                        <small class="text-blue" style="margin-right: 5px;">
                                            <i class="fa fa-envelope"></i>
                                            <?= $row['email']; ?>
                                        </small>
                                        <small class="text-blue">
                                            <i class="fa fa-calendar"></i>
                                            <?= date("d F Y", strtotime($row['created_at'])); ?>
                                        </small>
                                    </td>
                                    <td><?= $row['inquiry']; ?></td>
                                    <td><?= $row['subject']; ?></td>
                                    <td><?= word_limiter(strip_tags($row['message']),10); ?></td>
                                    <td>
                                        <?php include('messages/detail.php') ?>
                                        <?php include('messages/delete.php') ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
            </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <!-- ./end row message -->

    <!-- row POSTS AND CAREERS -->
    <div class="row">
        <div class="col-xs-12 col-md-5">
            <!-- Posts -->
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-user"></i> Recent Job Vacancy</h3>
                    <div class="box-tools pull-right">
                        <a href="<?= site_url('admin/careers/create/') ?>" class="btn bg-maroon btn-sm btn-flat"><i class="fa fa-plus"></i> Create Job</a>
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        <?php if ($careers) : ?>
                            <!-- looping careers -->
                            <?php $no = 1; foreach ($careers as $row): ?>
                            <li class="item">
                                <div class="product-img">
                                    <img src="<?= base_url('assets/images/careers/'.$row['image']); ?>" width="50" height="50">
                                </div>
                                <div class="product-info">
                                    <a href="#" class="product-title">
                                        <span class="text-black"><?= $row['title']; ?></span>
                                        <?php if ($row['status'] == 'Publish') : ?> 
                                            <span class="label label-success pull-right">Publish</span>
                                        <?php else : ?>
                                            <span class="label label-warning pull-right">Pending</span>
                                        <?php endif; ?>
                                        <br>
                                        <small class="text-blue">
                                            <i class="fa fa-calendar"></i>
                                            <?= date("d F Y", strtotime($row['created_at'])); ?>
                                        </small>
                                    </a>
                                    <span class="product-description">
                                        <small>
                                            <?= $row['position']; ?> - <span class="text-green">Rp. <?= number_format($row['salary'], 2, ',', '.'); ?> /M</span>
                                        </small>
                                    </span>
                                </div>
                            </li>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <li class="item text-center"><h3>Nothing data ! </h3></li>
                        <?php endif; ?>
                    </ul>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a href="<?= base_url('admin/careers'); ?>" class="uppercase">View More</a><br>
                    <span class="label bg-navy"><?= $count_careers; ?> Jobs</span>
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
        <div class="col-xs-12 col-md-7">
            <!-- Posts -->
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-file-text"></i> Recent Posts</h3>
                    <div class="box-tools pull-right">
                        <a href="<?= site_url('admin/posts/create/') ?>" class="btn bg-maroon btn-sm btn-flat"><i class="fa fa-plus"></i> Create Post</a>
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        <?php if ($allPosts) : ?>
                            <!-- looping careers -->
                            <?php $no = 1; foreach ($allPosts as $row): ?>
                            <li class="item">
                                <div class="product-img">
                                <img src="<?= base_url('assets/images/posts/'.$row['image']); ?>" width="50" height="50">
                                </div>
                                <div class="product-info">
                                    <span class="text-black"><b><?= $row['title']; ?></b></span>
                                    <?php if ($row['status'] == 'Publish') : ?> 
                                        <span class="label label-success pull-right">Publish</span>
                                    <?php else : ?>
                                        <span class="label label-warning pull-right">Pending</span>
                                    <?php endif; ?>
                                    <br>
                                    <small class="text-blue">
                                        <i class="fa fa-user"></i>
                                        <?= $row['full_name']; ?>
                                        &nbsp;&nbsp;
                                        <i class="fa fa-clock-o"></i>
                                        <?= date("d F Y", strtotime($row['created_at'])); ?>
                                        &nbsp;&nbsp;
                                    </small>
                                    <br>
                                    <small class="text-blue">
                                        <i class="fa fa-globe"></i>
                                        <a href="<?= site_url('posts/'.$row['slug']); ?>">
                                        <?= site_url('posts/'.$row['slug']); ?>
                                        </a>
                                    </small>
                                    <br>
                                    <small>
                                        <?= word_limiter(strip_tags($row['description']), 35); ?>
                                    </small>
                                </div>
                            </li>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <li class="item text-center"><h3>Nothing data ! </h3></li>
                        <?php endif; ?>
                    </ul>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a href="<?= base_url('admin/posts') ?>" class="uppercase">View More</a><br>
                    <span class="label bg-navy"><?= $count_posts; ?> Jobs</span>
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
    </div>
    <!-- /.row Posts -->
</section>
<!-- /.content -->