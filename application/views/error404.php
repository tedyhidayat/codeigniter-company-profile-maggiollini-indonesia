<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= $title; ?></title>
    <?php if($configs) : ?>
    <link rel="shortcut icon" href="<?= base_url('assets/images/configurations/'. $configs['icon']); ?>" type="image/x-icon">
    <meta name="author" content="<?= $configs['siteName']; ?>">
    <meta name="description" content="<?= $configs['metaDesc']; ?>">
    <?php endif; ?>
    <link rel="stylesheet" href="<?= base_url('assets/frontend/'); ?>libraries/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/frontend/'); ?>libraries/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/frontend/'); ?>libraries/aos/aos.css">
    <link rel="stylesheet" href="<?= base_url('assets/frontend/'); ?>styles/main.css">
    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> -->

  </head>
  <body>  
        
    <!-- 404 not found -->
    <section id="notfound">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-10 text-center"> 
                    <img src="<?= base_url('assets/images/404-notfound.jpg'); ?>" alt="" class="img-fluid"><br />
                    <a href="<?= site_url(); ?>" class="btn btn-general">Kembali ke Home</a>
                </div>
            </div>
        </div>
    </section>

    <script src="<?= base_url('assets/frontend/'); ?>libraries/jquery/jquery-3.4.1.min.js"></script>
    <script src="<?= base_url('assets/frontend/'); ?>libraries/bootstrap/js/bootstrap.js"></script>
    <script src="<?= base_url('assets/frontend/'); ?>libraries/jquery_easing/jquery.easing.min.js"></script>
    <script src="<?= base_url('assets/frontend/'); ?>libraries/fontawesome/js/all.min.js"></script>
    <script src="<?= base_url('assets/frontend/'); ?>libraries/aos/aos.js"></script>
    <script src="<?= base_url('assets/frontend/'); ?>scripts/main.js"></script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script> -->
  </body>
</html>