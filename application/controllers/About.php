<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		create_cookie();
		$this->load->model('Abouts_model');
		$this->load->model('Gallery_model');
		$this->load->model('Partners_model');
	}

	// function show login page
	public function index()	{
		// unset sessinon captcha if there
		$this->session->unset_userdata('admin_access');
		$this->session->unset_userdata('captchaImage');
		// identification
		$configurations = $this->configs->getConfiguration();
		// check config for title
		(!empty($configurations['siteName'])) ? $title1 = $this->uri->segment(1) : $title1 = "THDEV";
		(!empty($configurations['siteName'])) ? $title2 = $configurations['siteName'] : $title2 = "Web Developer";

		$data = [
			'title' 		=> $title1.' | '.$title2,
			'configs' 		=> $this->configs->getConfiguration(),
			'about' 		=> $this->Abouts_model->getallAboutActive(),
			'team' 			=> $this->Gallery_model->public_about_team(),
			'banner' 		=> $this->Gallery_model->public_about_banner(),
			'partners' 		=> $this->Partners_model->getAllPartnersActive(),
			'config_page' 	=> $this->config_page->getAllConfigPageActiveHeading(),
        ];
        
        $this->templating->load('layouts/public/wrapper', 'about', $data);
	}


}

/* End of file About.php */
/* Location: ./application/controllers/Index.php */