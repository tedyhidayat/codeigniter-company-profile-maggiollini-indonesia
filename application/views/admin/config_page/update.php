<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?= $title; ?></h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- flash data -->
    <?= $this->session->flashdata('message'); ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="<?= site_url('admin/config-page/edit/' . $config_page['id']); ?>"
                    enctype="multipart/form-data" role="form">
                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" class="form-control" value="<?= $this->security->get_csrf_hash(); ?>" >
                    <div class="box-body">
                        <input type="hidden" name="id" value="<?= $config_page['id']; ?>">
                        <div class="row">
                            <div class="col xs-12 col-md-8">
                                <div class="form-group">
                                    <label for="title">Job Title</label>
                                    <input type="text" name="title" class="form-control" id="title"
                                        value="<?= $config_page['title']; ?>">
                                    <?= form_error('title', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="col xs-12 col-md-4">
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select name="status" id="status" class="form-control">
                                        <?php if (!$config_page['status']) : ?>
                                        <option value="">Selecet Gender</option>
                                        <option value="Publish">Publish</option>
                                        <option value="Pending">Pending</option>
                                        <?php else : ?>
                                        <?php if ($config_page['status'] == 'Publish') : ?>
                                        <option value="Publish">Publish</option>
                                        <option value="Pending">Pending</option>
                                        <?php elseif ($config_page['status'] == 'Pending') : ?>
                                        <option value="Pending">Pending</option>
                                        <option value="Publish">Publish</option>
                                        <?php endif; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="page_position">Page Position</label>
                                    <input type="text" name="page_position" class="form-control" id="page_position"
                                        value="<?= $config_page['page_position']; ?>" readonly>
                                </div>
                            </div>
                            <div class="col xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="type">Type</label>
                                    <input type="text" name="type" class="form-control" id="type"
                                        value="<?= $config_page['type']; ?>" readonly>
                                </div>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label for="image">Image</label>
                            <div class="row">
                                <div class="col-xs-3 col-md-2">
                                    <img width="200" class="img-thumbnail"
                                        src="<?= base_url('assets/images/configurations/config_page/thumbs/'.$config_page['image']); ?>" alt="">
                                </div>
                                <div class="col-xs-9 col-md-10">
                                    <input type="file" name="image" class="form-control" id="image">
                                    <small class="text-info">* Max size 2 Mb</small><br>
                                         <small class="text-info">* Only for jpg | jpeg | png</small>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" id="description" name="description" rows="5" cols="80"><?= $config_page['description']; ?></textarea>
                            <?= form_error('description', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success btn-flat pull-right">Edit</button>
                        <a href="<?= site_url('admin/config-page/'); ?>" class="btn pull-right">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>