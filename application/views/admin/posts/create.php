    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= $title; ?></h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- flash data -->
        <?= $this->session->flashdata('message'); ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" action="<?= site_url('admin/posts/create/'); ?>" enctype="multipart/form-data" role="form">
                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" class="form-control" value="<?= $this->security->get_csrf_hash(); ?>" >
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12 col-md-8">
                                    <div class="form-group">
                                    <label for="title">Title</label>
                                        <input type="text" name="title" class="form-control" id="title"
                                            placeholder="Title" value="<?= set_value('title'); ?>">
                                        <?= form_error('title', '<small class="text-danger">', '</small>'); ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                    <label for="status">Status</label>
                                        <select name="status" id="status" class="form-control">
                                            <option value="Pending">Pending</option>
                                            <option value="Publish">Publish</option>
                                        </select>
                                        <?= form_error('status', '<small class="text-danger">', '</small>'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                    <label for="categId">Category</label>
                                        <select name="categId" id="categId" class="form-control">
                                            <option value="">Select Category</option>
                                            <?php foreach ($categories as $row) : ?>
                                            <option value="<?= $row['id']; ?>"><?= $row['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <?= form_error('categId', '<small class="text-danger">', '</small>'); ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                    <label for="type">Type</label>
                                        <select name="type" id="type" class="form-control">
                                            <option>Select Type</option>
                                            <option value="News">News</option>
                                            <option value="Profile">Profile</option>
                                        </select>
                                        <?= form_error('type', '<small class="text-danger">', '</small>'); ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                    <label for="image">Image</label>
                                        <input type="file" name="image" class="form-control" id="image">
                                        <small class="text-info">* Max size 2 Mb</small><br>
                                         <small class="text-info">* Only for jpg | jpeg | png</small>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="keywords">Keywords (SEO)</label>
                                <textarea class="form-control" name="keywords" id="keywords" cols="10"
                                    placeholder="Type Keywords" rows="4"><?= set_value('keywords'); ?></textarea>
                                <?= form_error('keywords', '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea id="editor1" name="description" rows="20" cols="80"
                                    style="visibility: hidden; display: none;"><?= set_value('description'); ?></textarea>
                                <?= form_error('description', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success btn-flat pull-right">Save</button>
                            <a href="<?= site_url('admin/posts/'); ?>" class="btn pull-right">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
