<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services_model extends CI_Model {

	// sitemap
	public function sitemap() {
		$this->db->select('tbl_services.slug, tbl_services.created_at, tbl_services.title');
		$this->db->from('tbl_services');
		$this->db->order_by('id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// function get all data services
	public function getAllServices() {
		$this->db->select('tbl_services.*, tbl_users.full_name');
		$this->db->from('tbl_services');
		$this->db->join('tbl_users','tbl_users.id = tbl_services.id_user', 'LEFT');
		$this->db->order_by('id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// function get all data services active
	public function getAllServicesActive() {
		$this->db->select('tbl_services.*, tbl_users.full_name');
		$this->db->from('tbl_services');
		$this->db->join('tbl_users','tbl_users.id = tbl_services.id_user', 'LEFT');
		$this->db->where('tbl_services.status','Publish');
		$this->db->order_by('tbl_services.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	// function get data services limiter
	public function public_home_services() {
		$this->db->select('tbl_services.*, tbl_users.full_name');
		$this->db->from('tbl_services');
		$this->db->join('tbl_users','tbl_users.id = tbl_services.id_user', 'LEFT');
		$this->db->where('tbl_services.status','Publish');
		$this->db->limit(3);
		$this->db->order_by('tbl_services.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// function get data services limiter footer
	public function public_services_footer() {
		$this->db->select('tbl_services.*, tbl_users.full_name');
		$this->db->from('tbl_services');
		$this->db->join('tbl_users','tbl_users.id = tbl_services.id_user', 'LEFT');
		$this->db->where('tbl_services.status','Publish');
		$this->db->order_by('tbl_services.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	// function get data services by slug
	public function getDetailService($slug = NULL) {
		$query = $this->db->get_where('tbl_services', ['slug' => $slug])->row_array();
		if(!$query) {
			redirect('errors/error404','refresh');
			die;
		} else {
			return $query;
		}
	}

	// function get data services by ID
	public function getServicesById($id) {
		return $this->db->get_where('tbl_services', ['id' => $id])->row_array();
	}

	// function create data service
	public function create($data) {
		// get uploaded file 
		$upload_image = $_FILES['image']['name'];
		// check if there uploaded file
		if ($upload_image) {
			// define config uploaded file
			$config['max_size']     	= 2048;
			$config['allowed_types'] 	= 'jpg|png';
			$config['upload_path'] 		= './assets/images/services/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-service-';
			// initializing config uploaded file
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('image')) {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . '</div>');
				redirect('admin/services');
			} else {
				// if success
				// define config for creating thumbnail
				$image = $this->upload->data('file_name');
				$config['image_library'] 	= 'gd2';
				$config['create_thumb'] 	= TRUE;
				$config['maintain_ratio'] 	= TRUE;
				$config['width']         	= 500;
				$config['height']       	= 470;
				$config['thumb_marker']    	= '';
				$config['source_image']	 	= './assets/images/services/'.$image;
				$config['new_image']	 	= './assets/images/services/thumbs/'.$image;
				// initializing config create thumbnail
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				// collecting data to insert to tables
				$data = [
					'image' 		=> $image,
					'created_at' 	=> date('Y-m-d H:i:s'),
					'id_user' 		=> $this->session->userdata('id'),
					'title' 		=> $this->input->post('title', true),
					'status' 		=> $this->input->post('status', true),
					'keywords' 		=> $this->input->post('keywords', true),
					'description' 	=> $this->input->post('description', true),
					'slug' 			=> url_title($this->input->post('title', true), 'dash', true),
				];
			}
		} else {
			// if nothing file uploaded
			// collecting data to insert to tables
			$data = [
				'image' 		=> 'default.png',
				'created_at' 	=> date('Y-m-d H:i:s'),
				'id_user' 		=> $this->session->userdata('id'),
				'title' 		=> $this->input->post('title', true),
				'status' 		=> $this->input->post('status', true),
				'keywords' 		=> $this->input->post('keywords', true),
				'description' 	=> $this->input->post('description', true),
				'slug' 			=> url_title($this->input->post('title', true), 'dash', true),
			];
		}
		// query insert
		$this->db->insert('tbl_services', $data);
		// check if there data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to insert data!</div>');
			redirect('admin/services/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Success!</div>');
			redirect('admin/services/');
		}
		
	}

	// function edit data service
	public function edit($data) {
		// get data from input form
		$id_user 		= $this->session->userdata('id');
		$title 			= $this->input->post('title', true);
		$status 		= $this->input->post('status', true);
		$keywords 		= $this->input->post('keywords', true);
		$description 	= $this->input->post('description', true);
		$slug 			= url_title($this->input->post('title', true), 'dash', true);
		$upload_image 	= $_FILES['image']['name'];
		// check if there uploaded file
		if ($upload_image) {
			// define config uploaded file
			$config['max_size']     	= 2048;
			$config['allowed_types'] 	= 'jpg|png';
			$config['upload_path'] 		= './assets/images/services/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-service-';
			$this->load->library('upload', $config);
			if ($this->upload->do_upload('image')) {
				// if success
				// unlink old image
				$old_image = $data['service']['image'];
				if ($old_image != 'default.png') {
					unlink(FCPATH . 'assets/images/services/' . $old_image);
					unlink(FCPATH . 'assets/images/services/thumbs/' . $old_image);
				}
				// define confige create thumbnail
				$new_image = $this->upload->data('file_name');
				$config['image_library'] 	= 'gd2';
				$config['create_thumb'] 	= TRUE;
				$config['maintain_ratio'] 	= TRUE;
				$config['width']         	= 500;
				$config['height']       	= 470;
				$config['thumb_marker']    	= '';
				$config['source_image']	 	= './assets/images/services/'.$new_image;
				$config['new_image']		= './assets/images/services/thumbs/'.$new_image;
				// initializing config creating thumbnail
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				// set new image to database
				$this->db->set('image', $new_image);
			} else {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . '</div>');
				redirect('admin/services/edit/' . $data['id']);
			}
		}

		// query update
		$this->db->set('title', $title);
		$this->db->set('slug', $slug);
		$this->db->set('description', $description);
		$this->db->set('status', $status);
		$this->db->set('keywords', $keywords);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $data['id']);
		$this->db->update('tbl_services');
		// check if there data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to edit data!</div>');
			redirect('admin/services/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your service has been updated!</div>');
			redirect('admin/services/');
		}
		
	}

	// function delete service
	public function delete($data) {
		// get old file
		$old_image = $data['service']['image'];
		// delete old file
		if ($old_image != 'default.png') {
			unlink(FCPATH . 'assets/images/services/' . $old_image);
			unlink(FCPATH . 'assets/images/services/thumbs/' . $old_image);
		}
		// query delete
		$this->db->where('id', $data['id']);
		$this->db->delete('tbl_services');
		// check if there data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to deleting data!</div>');
			redirect('admin/services/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your service has been delete!</div>');
			redirect('admin/services/');
		}
		
	}
}

/* End of file Services_model.php */
/* Location: ./application/models/Services_model.php */