<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sample_model extends CI_Model {

	// sitemap
	public function sitemap() {
		$this->db->select('tbl_sample.slug, tbl_sample.created_at, tbl_sample.sample_name');
		$this->db->from('tbl_sample');
		$this->db->order_by('id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// function get all data sample
	public function getAllSample() {
		/**
		 * tbl_sample join tbl_category_sample and tbl_users
		 * tbl_category_sample: get sample name, tbl_users: get full name
		 */
		$this->db->select('tbl_sample.*, tbl_users.full_name, tbl_category_sample.name');
		$this->db->from('tbl_sample');
		$this->db->join('tbl_users','tbl_users.id = tbl_sample.id_user', 'LEFT');
		$this->db->join('tbl_category_sample','tbl_category_sample.id = tbl_sample.id_category', 'LEFT');
		$this->db->order_by('tbl_sample.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// function get all data sample acite
	public function public_sample_active($limit,$start) {
		/**
		 * tbl_sample join tbl_category_sample and tbl_users
		 * tbl_category_sample: get sample name, tbl_users: get full name
		 */
		$this->db->select('tbl_sample.*, tbl_users.full_name, tbl_category_sample.name');
		$this->db->from('tbl_sample');
		$this->db->join('tbl_users','tbl_users.id = tbl_sample.id_user', 'LEFT');
		$this->db->join('tbl_category_sample','tbl_category_sample.id = tbl_sample.id_category', 'LEFT');
		$this->db->where('tbl_sample.status','Publish');
		$this->db->limit($limit, $start);
		$this->db->order_by('tbl_sample.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// function count sample active
	public function total() {
		/**
		 * tbl_sample join tbl_category_sample and tbl_users
		 * tbl_category_sample: get sample name, tbl_users: get full name
		 */
		$this->db->select('tbl_sample.*, tbl_users.full_name, tbl_category_sample.name');
		$this->db->from('tbl_sample');
		$this->db->join('tbl_users','tbl_users.id = tbl_sample.id_user', 'LEFT');
		$this->db->join('tbl_category_sample','tbl_category_sample.id = tbl_sample.id_category', 'LEFT');
		$this->db->where('tbl_sample.status','Publish');
		$this->db->order_by('tbl_sample.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	// function get sample by slug
	public function getSampleBySlug($slug = NULL) {
		if(!empty($slug)) {
			$this->db->select('tbl_sample.*, tbl_users.full_name, tbl_category_sample.name');
			$this->db->from('tbl_sample');
			$this->db->join('tbl_users','tbl_users.id = tbl_sample.id_user', 'LEFT');
			$this->db->join('tbl_category_sample','tbl_category_sample.id = tbl_sample.id_category', 'LEFT');
			$this->db->where('tbl_sample.slug',$slug);
			$query = $this->db->get();
			return $query->row_array();
		} else {
			return false;
		}
	}

	// function get sample by ID
	public function getSampleById($id) {
		return $this->db->get_where('tbl_sample', ['id' => $id])->row_array();
	}

	// function create data sample
	public function create($data) {	
		// get uploaded file from input file
		$upload_image = $_FILES['image']['name'];
		// check if there uploaded file
		if ($upload_image) {
			// if success
			// define config uploaded file
			$config['max_size']     	= 2048;
			$config['file_ext_tolower'] = TRUE;
			$config['allowed_types'] 	= 'jpg|jpeg|png';
			$config['upload_path'] 		= './assets/images/projects/sample/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-project-';
			// initializing config uploaded file
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('image')) {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . '</div>');
				redirect('admin/sample/create/');
			} else {
				// if success
				// define config for creating thumbnail
				$image = $this->upload->data('file_name');
				$config['image_library'] 	= 'gd2';
				$config['create_thumb'] 	= TRUE;
				$config['maintain_ratio'] 	= TRUE;
				$config['width']         	= 500;
				$config['height']       	= 470;
				$config['thumb_marker']    	= '';
				$config['source_image']	 	= './assets/images/projects/sample/'.$image;
				$config['new_image']	 	= './assets/images/projects/sample/thumbs/'.$image;
				// initializing config create thumbnail
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				// collecting data to insert to tbale in database
				$insertsample = [
					'image' 		=> $image,
					'created_at' 	=> date('Y-m-d H:i:s'),
					'id_user' 		=> $this->session->userdata('id'),
					'status' 		=> $this->input->post('status', true),
					'id_category' 	=> $this->input->post('categId', true),
					'keywords' 		=> $this->input->post('keywords', true),
					'sample_name' 	=> $this->input->post('sampleName', true),
					'description' 	=> $this->input->post('description', true),
					'slug' 			=> url_title($this->input->post('sampleName', true), 'dash', true),
				];
			}
		} else {
			// if nothing file uploaded
			// collecting data to insert to tbale in database
			$insertsample = [
				'created_at' 	=> date('Y-m-d H:i:s'),
				'id_user' 		=> $this->session->userdata('id'),
				'status' 		=> $this->input->post('status', true),
				'id_category' 	=> $this->input->post('categId', true),
				'keywords' 		=> $this->input->post('keywords', true),
				'sample_name' 	=> $this->input->post('sampleName', true),
				'description' 	=> $this->input->post('description', true),
				'slug' 			=> url_title($this->input->post('sampleName', true), 'dash', true),
			];
		}
		// query insert
		$this->db->insert('tbl_sample', $insertsample);
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to insert data!</div>');
			redirect('admin/sample/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Success insert data!</div>');
			redirect('admin/sample/');
		}
	}

	// function edit data sample
	public function edit($data) {
		$id_user 		= $this->session->userdata('id');
		$status 		= $this->input->post('status', true);
		$id_category 	= $this->input->post('categId', true);
		$keywords 		= $this->input->post('keywords', true);
		$sampleName 	= $this->input->post('sampleName', true);
		$description 	= $this->input->post('description', true);
		$slug 			= url_title($this->input->post('sampleName', true), 'dash', true);
		$upload_image 	= $_FILES['image']['name'];
		// check if there uploaded file
		if ($upload_image) {
			// define config uploaded file
			$config['file_ext_tolower'] = TRUE;
			$config['max_size']     	= 2048;
			$config['allowed_types'] 	= 'jpg|png';
			$config['upload_path'] 		= './assets/images/projects/sample/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-project-';
			// initializing config uploaded file
			$this->load->library('upload', $config);
			if ($this->upload->do_upload('image')) {
				// if success
				// unlink old image
				$old_image = $data['sample']['image'];
				if ($old_image != 'default.png') {
					unlink(FCPATH . 'assets/images/projects/sample/' . $old_image);
					unlink(FCPATH . 'assets/images/projects/sample/thumbs/' . $old_image);
				}
				// define confige create thumbnail
				$new_image = $this->upload->data('file_name');
				$config['image_library'] 	= 'gd2';
				$config['create_thumb'] 	= TRUE;
				$config['maintain_ratio'] 	= TRUE;
				$config['width']         	= 500;
				$config['height']       	= 470;
				$config['thumb_marker']    	= '';
				$config['source_image']	 	= './assets/images/projects/sample/'.$new_image;
				$config['new_image']		= './assets/images/projects/sample/thumbs/'.$new_image;
				// initializing config creating thumbnail
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				// set new image to database
				$this->db->set('image', $new_image);
			} else {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . '</div>');
				redirect('admin/sample/edit/' . $data['id']);
			}
		}
		// query update
		$this->db->set('slug', $slug);
		$this->db->set('status', $status);
		$this->db->set('keywords', $keywords);
		$this->db->set('sample_name', $sampleName);
		$this->db->set('id_category', $id_category);
		$this->db->set('description', $description);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $data['id']);
		$this->db->update('tbl_sample');
		// check if there affected row
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your sample failed to update!</div>');
			redirect('admin/sample/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your sample has been updated!</div>');
			redirect('admin/sample/');
		}
	}

	// function Delete Sample
	public function delete($data) {
		// check if there id sample
		if ($data['id']) {
			// if success
			// delete old file
			$old_image = $data['sample']['image'];
			if ($old_image) {
				unlink(FCPATH . 'assets/images/projects/sample/' . $old_image);
				unlink(FCPATH . 'assets/images/projects/sample/thumbs/' . $old_image);
			}
			$this->db->where('id', $data['id']);
			$this->db->delete('tbl_sample');
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Success delete '. $data['sample']['sample_name'] .'</div>');
			redirect('admin/sample/');
		} else {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to delete data!</div>');
			redirect('admin/sample/');
		}
	}


}

/* End of file Sample_model.php */
/* Location: ./application/models/Sample_model.php */