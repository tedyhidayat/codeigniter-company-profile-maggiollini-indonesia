<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		security_access();
		$this->load->model('User_model');
		$this->load->model('Categories_model');
	}

	// function show index
    public function index() {
		// get data from models
		$data = [
			'title' 		=> 'Posts Categories',
			'config' 		=> $this->configs->getConfiguration(),
			'user' 			=> $this->User_model->getUserBySession(),
			'categories' 	=> $this->Categories_model->getAllCategories(),
		];
		// view index Posts categories page
        $this->templating->load('layouts/admin/wrapper', 'admin/categories/index', $data);
	}
	
	// function create Posts category page
	public function create() {
		// get data from models
		$data = [
			'title' 	=> 'Add Posts Category',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
		];
		// validatiuon rules
		$this->form_validation->set_rules('categName', 'category', 'trim|required|is_unique[tbl_categories.name]', [
			'is_unique' => 'Categoty Name already exist!'
		]);
		$this->form_validation->set_rules('position', 'position', 'required|numeric|is_unique[tbl_categories.position]', [
			'is_unique' => 'position Name already exist!'
		]);
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/categories/create', $data);
		} else {
			// if validation success call model create from categories_Posts_model
			$this->Categories_model->create($data);
		}
	}

	// function edit Posts category page
	public function edit($id) {
		// get data from models
		$data = [
			'id' 		=> $id,
			'title' 	=> 'Edit Posts Category',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'category' 	=> $this->Categories_model->getCategoriesById($id),
		];
		// validation rules
		$this->form_validation->set_rules('categName', 'category name', 'trim|required');
		$this->form_validation->set_rules('position', 'position', 'required|trim|numeric');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/categories/update', $data);
		} else {
			// if validation success call model edit from categories_Posts_model
			$this->Categories_model->edit($data);
		}
	}

	// function delte Posts category page
	public function delete($id) {
		// get data from models
		$data = ['id' => $id, 'category' => $this->Categories_model->getCategoriesById($id)];
		// call function delete from model Categories_Posts_model
		$this->Categories_model->delete($data);
	}
	
}

/* End of file Categories.php */

?>
