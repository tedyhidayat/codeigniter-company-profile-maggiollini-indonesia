<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		security_access();
		$this->load->model('User_model');
		$this->load->model('Services_model');
	}

	// function show index service page
    public function index() {
		// get data from models
		$data = [
			'title' 	=> 'Services',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'services' 	=> $this->Services_model->getAllServices(),
		];
		// view index service page
        $this->templating->load('layouts/admin/wrapper', 'admin/services/index', $data);
	}
	
	// function create services page
	public function create() {
		// get data from models
		$data = [
			'title' 	=> 'Add Service',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
		];
		// validation rules
		$this->form_validation->set_rules('image', 'Image', 'trim');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('keywords', 'Keywords', 'required');
		$this->form_validation->set_rules('description', 'Description', 'trim');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/services/create', $data);
		} else {
			// if validation success call function create from services_model
			$this->Services_model->create($data);
		}
	}

	// function edit services page
	public function edit($id) {
		// get data from models
		$data = [
			'id' 		=> $id,
			'title' 	=> 'Edit Service',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'service' 	=> $this->Services_model->getServicesById($id),
		];
		// validation rules
		$this->form_validation->set_rules('image', 'Image', 'trim');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('keywords', 'Keywords', 'required');
		$this->form_validation->set_rules('description', 'Description', 'trim');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/services/update', $data);
		} else {
			// if validation success call function create from services_model
			$this->Services_model->edit($data);
		}
	}

	// function delete services page
	public function delete($id) {
		// get data from models
		$data = ['id' => $id,'service' => $this->Services_model->getServicesById($id)];
		// call function delete from services_models
		$this->Services_model->delete($data);
	}
}

/* End of file Services.php */

?>
