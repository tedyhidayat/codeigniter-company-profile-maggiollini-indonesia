<?php if($configs) : ?>   
    <!-- Breadcrumbs -->
    <section id="breadcrumbs">
        <div class="container">
            <div class=" row justify-content-between align-items-center">
                <div class="col-12 col-md col-lg">
                    <h2 class="text-uppercase"><?= $this->uri->segment(1); ?></h2>
                </div>
                <div class="col-12 col-md col-lg">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= site_url('/'); ?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?= $this->uri->segment(1); ?></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- services -->
    <section id="services" class="">
        <div class="container">
        <?php if($services) : ?> 
            <header>
                <?php foreach($config_page as $row_page) : ?>
                <?php if($row_page['page_position'] == 'Services'): ?>
                    <h3 class="text-capitalize"><?= $row_page['title']; ?></h3>
                    <p><?= $row_page['description']; ?></p>
                <?php endif; ?>
                <?php endforeach; ?>
            </header>
            <div class="row justify-content-center mt-5">
                <!-- item service -->
                <?php foreach($services as $row_service) : ?>
                <div class="col-sm-6 col-lg-4 mb-4">
                    <div class="card" data-aos="fade-up" style="background-image: url('<?= base_url('assets/images/services/thumbs/'.$row_service['image']); ?>');">
                        <div class="card-body">
                            <h5 class="card-title"><?= strip_tags($row_service['title']); ?></h5>
                            <p class="card-text"><?= word_limiter(strip_tags($row_service['description']), 17); ?></p>
                            <a href="<?= site_url('/services/detail/'.$row_service['slug']); ?>" class="btn btn-services text-uppercase" alt="<?= $row_service['title']; ?>" title="<?= $row_service['title']; ?>">Lihat Detail Servis</a>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        <?php else : ?>
            <div class="row">
                <div class="col-12 mb-5">
                    <div class="cards text-center" style="border: 1px solid #ddd;">
                        <div class="card-body">
                            <h4>Servis kosong.</h4>
                            <p><?= $configs['siteUrl']; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        </div>
    </section>
<?php else : ?>
    <section>
        <div class="container" style="min-height: 300px; ">
            <div class="row align-items-center">
                <div class="col-12 text-center">
                    <h2 class="mt-5">EMPTY!</h2>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>