<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		create_cookie();
		$this->load->model('Gallery_model');		
	}

	// function show login page
	public function index()	{
		// identification
		$configurations = $this->configs->getConfiguration();

		// check config for title
		(!empty($configurations['siteName'])) ? $title1 = $this->uri->segment(1) : $title1 = "THDEV";
		(!empty($configurations['siteName'])) ? $title2 = $configurations['siteName'] : $title2 = "Web Developer";

		$data = [
			'title' 		=> $title1.' | '.$title2,
			'configs' 		=> $this->configs->getConfiguration(),
			'services' 		=> $this->Services_model->getAllServicesActive(),
			'config_page' 	=> $this->config_page->getAllConfigPageActiveHeading(),
        ];
        
        $this->templating->load('layouts/public/wrapper', 'services', $data);
	}

	// function detail services
	public function detail($slug)	{
		// unset sessinon captcha if there
		$this->session->unset_userdata('admin_access');
		$this->session->unset_userdata('captchaImage');
		// identification
		$configurations = $this->configs->getConfiguration();
		$slug = $this->uri->segment(3);

		// check config for title
		(!empty($configurations['siteName'])) ? $title1 = $this->uri->segment(1) : $title1 = "THDEV";
		(!empty($configurations['siteName'])) ? $title2 = $configurations['siteName'] : $title2 = "Web Developer";

		$data = [
			'title' 		=> $title1.' | '.$title2,
			'configs' 		=> $this->configs->getConfiguration(),
			'service' 		=> $this->Services_model->getDetailService($slug),
        ];
        
        $this->templating->load('layouts/public/wrapper', 'service_detail', $data);
	}

}

/* End of file Services.php */
/* Location: ./application/controllers/Index.php */