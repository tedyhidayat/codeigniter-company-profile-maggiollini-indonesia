<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $title; ?></title>
    <!-- icon -->
    <?php if($config) : ?>
    <link rel="shortcut icon" href="<?= base_url('assets/images/configurations/'.$config['icon']) ?>" type="image/x-icon">
    <?php endif; ?>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet"
        href="<?= base_url('assets/backend/'); ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet"
        href="<?= base_url('assets/backend/'); ?>bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('assets/backend/'); ?>dist/css/AdminLTE.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="login-box-body">
            <?= $this->session->flashdata('message'); ?>

            <p class="text-center" style="font-size: 22px; margin-bottom:23px;"> Registration</p>

            <form class="text-center" action="<?= site_url('auth/registration/'); ?>" method="post">
                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" class="form-control" value="<?= $this->security->get_csrf_hash(); ?>" >
                <div class="form-group has-feedback">
                    <input type="text" name="full_name" class="form-control" placeholder="Full Name"
                        value="<?= set_value('full_name'); ?>" autocomplete="off">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    <?= form_error('full_name', '<small class="text-danger">', '</small>'); ?>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" name="email" class="form-control" placeholder="Email"
                        value="<?= set_value('email'); ?>" autocomplete="off">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" name="username" class="form-control" placeholder="Username"
                        value="<?= set_value('username'); ?>" autocomplete="off">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    <?= form_error('username', '<small class="text-danger">', '</small>'); ?>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password1" class="form-control" placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password2" class="form-control" placeholder="Confirm Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback text-center">
                    <label class="image"><?= $captcha; ?></label>
                    <a href="javascript:;" class="refresh btn float-right bg-maroon btn-sm btn-flat"><i class="fa fa-refresh"></i></a>
                </div>
                <div class="form-group">
                    <input type="text" name="capt" class="form-control text-center" placeholder="Captcha" autocomplete="off">
                    <?= form_error('capt', '<small class="text-danger">', '</small>'); ?>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                    </div>
                    <div class="col-xs-12" style="margin-top: 20px;">
                        <a href="<?= site_url('auth'); ?>">Have account ? login.</a>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
    <!-- jQuery 3 -->
    <script src="<?= base_url('assets/backend/'); ?>bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?= base_url('assets/backend/'); ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.refresh').on('click', function() {
                $.get('<?php echo site_url().'auth/recaptcha/'; ?>', function(data) {
                    $('.image').html(data);
                });
            });
        });
    </script>
</body>
</html>
