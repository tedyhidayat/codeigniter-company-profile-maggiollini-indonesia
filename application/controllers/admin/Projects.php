<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Projects extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		security_access();
		$this->load->model('User_model');
		$this->load->model('Projects_model');
		$this->load->model('Category_projects_model');
	} 

	// function show index project page
    public function index() {
		// get data from models
		$data = [
			'title' 	=> 'Finished Projects',
			'user' 		=> $this->User_model->getUserBySession(),
			'config' 	=> $this->configs->getConfiguration(),
			'projects' 	=> $this->Projects_model->getAllProjects()
		];
		// view index project page
        $this->templating->load('layouts/admin/wrapper', 'admin/projects/index', $data);
	}
	
	// function edit project page
	public function create() {
		// get data from models
		$data = [
			'title' 	=> 'Add Projects',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'categories'=> $this->Category_projects_model->getAllCategoryProjects()
		];
		// validation rules
		$this->form_validation->set_rules('image', 'image', 'trim');
		$this->form_validation->set_rules('status', 'status', 'required');
		$this->form_validation->set_rules('keywords', 'keywords', 'trim');
		$this->form_validation->set_rules('categoryId', 'category', 'required');
		$this->form_validation->set_rules('description', 'description', 'required|trim');
		$this->form_validation->set_rules('projectName', 'project Name', 'trim|required');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/projects/create', $data);
		} else {
			// if validation success call function create from projects_model
			$this->Projects_model->create($data);
		}
	}

	public function edit($id) {
		$data = [
			'id' 		=> $id,
			'title' 	=> 'Edit Projects',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'project' 	=> $this->Projects_model->getProjectsById($id),
			'categories'=> $this->Category_projects_model->getAllCategoryProjects(),
		];
		// validation rules
		$this->form_validation->set_rules('image', 'Image', 'trim');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('keywords', 'keywords', 'trim');
		$this->form_validation->set_rules('categoryId', 'category', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required|trim');
		$this->form_validation->set_rules('projectName', 'Project Name', 'trim|required');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/projects/update', $data);
		} else {
				// if validation success call function edit from projects_model
			$this->Projects_model->edit($data);
		}
	}

	// function delete data from models
	public function delete($id) {
		// get data from models
		$data = ['id' => $id,'project' => $this->Projects_model->getProjectsById($id)];
		// call functtion delete from project_model
		$this->Projects_model->delete($data);
		
	}
}

/* End of file Projects.php */

?>