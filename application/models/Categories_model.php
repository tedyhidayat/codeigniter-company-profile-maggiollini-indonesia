<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories_model extends CI_Model {

	// sitemap
	public function sitemap() {
		$this->db->select('tbl_categories.slug, tbl_categories.name');
		$this->db->from('tbl_categories');
		$this->db->order_by('id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// Function get all data Posts category 
	public function getAllCategories() {
		return $this->db->get('tbl_categories')->result_array();
	}
	
	// Function get one data Posts category by id
	public function getCategoriesById($id) {
		return $this->db->get_where('tbl_categories', ['id' => $id])->row_array();
	}

	// Function create data Posts category
	public function create($data) {	
		// get data from input form
		$data = [
			'position' 	=> $this->input->post('position', true),
			'name' 		=> $this->input->post('categName', true),
			'slug' 		=> url_title($this->input->post('categName', true), 'dash', true),
		];
		// query insert
		$this->db->insert('tbl_categories', $data);
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to create category!</div>');
			redirect('admin/categories/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Success!</div>');
			redirect('admin/categories/');
		}
		
	}

	// function edit data category Posts
	public function edit($data) {
		// get data from input form
		$position 	= $this->input->post('position', true);
		$name 		= $this->input->post('categName', true);
		$slug 		= url_title($this->input->post('categName', true), 'dash', true);
		// query update
		$this->db->set('name', $name);
		$this->db->set('slug', $slug);
		$this->db->set('position', $position);
		$this->db->where('id', $data['id']);
		$this->db->update('tbl_categories');
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to edit data!</div>');
			redirect('admin/categories/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your category has been updated!</div>');
			redirect('admin/categories/');
		}
	}

	// Function delete data Posts category
	public function delete($data) {
		// query delete
		$this->db->where('id', $data['id']);
		$this->db->delete('tbl_categories');
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to delete this data!</div>');
			redirect('admin/categories/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your category has been delete!</div>');
			redirect('admin/categories/');
		}
	}

}

/* End of file Categories_model.php */
/* Location: ./application/models/Categories_model.php */
