<header class="main-header">

    <!-- Logo -->
    <a href="<?= site_url('admin/dashboard'); ?>" class="logo bg-red">
        <?php if($config) : ?>  
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><img src="<?= base_url('assets/images/configurations/'.$config['logo']) ?>"></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><img src="<?= base_url('assets/images/configurations/'.$config['logo']) ?>"></span>
        <?php else : ?>
            <span class="logo-lg">THDEV</span>
            <span class="logo-mini">THD</span>
        <?php endif; ?>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown messages-menu">
                    <?php if($config) : ?> 
                        <a target="_blank" href="<?= site_url(); ?>" class="">
                            <i class="fa fa-home"></i> Visit Homepage
                        </a>
                    <?php else : ?>
                        <a href="<?= site_url(); ?>" target="_blank" class="">
                            <i class="fa fa-home"></i> Visit Homepage
                        </a>
                    <?php endif; ?>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle bg-red" data-toggle="modal" data-target="#modal-warning">
                        <i class="fa fa-sign-out "></i>
                        <span class="hidden-xs">Logout</span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>
