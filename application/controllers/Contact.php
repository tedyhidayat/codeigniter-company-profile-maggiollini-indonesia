<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	public function __construct() {
		parent::__construct();
		create_cookie();
		$this->load->helper('captcha');
		$this->load->helper('string');
	}
	

	// function show contact page
	public function index()	{
		// unset sessinon captcha if there
		$this->session->unset_userdata('admin_access');

		// delete captcha when refresh paeg
		if($this->session->userdata('captchaImage')) {
			$captchaImage = $this->session->userdata('captchaImage');
			unlink(FCPATH . 'assets/images/captcha/' . $captchaImage);
			$this->session->unset_userdata('captchaImage');
		}

		// identification
		$configurations = $this->configs->getConfiguration();

		// check config for title
		(!empty($configurations['siteName'])) ? $title1 = $this->uri->segment(1) : $title1 = "THDEV";
		(!empty($configurations['siteName'])) ? $title2 = $configurations['siteName'] : $title2 = "Web Developer";

		// Generate captcha
		$random_string = random_string('numeric',6);
		$vals = array(
			'word'          => $random_string,
			'img_path'      => APPPATH.'../assets/images/captcha/',
			'img_url'       => base_url('/assets/images/captcha/'),
			'img_width'     => '250',
			'img_height'    => 70,
			'expiration'    => 7200,
			'word_length'   => 8,
			'font_size'     => 17,	
			// White background and border, black text and red grid
			'colors'        => array(
					'background' => array(115, 115, 115),
					'border' => array(255, 255, 255),
					'text' => array(255, 255, 255),
					'grid' => array(166, 166, 166)
			)
		);
		$cap = create_captcha($vals);
		// set data for captcha word
		$this->session->set_userdata('captchaWord',$cap['word']);
		// set data for capcha image name
		$this->session->set_userdata('captchaImage',$cap['filename']);

		$data = [
			'captcha'	=> $cap['image'],
			'configs' 	=> $this->configs->getConfiguration(),
			'title' 		=> $title1.' | '.$title2,
        ];
        
        $this->templating->load('layouts/public/wrapper', 'contact', $data);
	}

	// refresh captcha
	public function recaptcha() {
		$captchaImage = $this->session->userdata('captchaImage');
		// unset image captcha
		unlink(FCPATH . 'assets/images/captcha/' . $captchaImage);

		// Generate captcha
		$random_string = random_string('numeric',6);
		$vals = array(
			'word'          => $random_string,
			'img_path'      => APPPATH.'../assets/images/captcha/',
			'img_url'       => base_url('/assets/images/captcha/'),
			'img_width'     => '250',
			'img_height'    => 70,
			'expiration'    => 7200,
			'word_length'   => 8,
			'font_size'     => 22,	
			// White background and border, black text and red grid
			'colors'        => array(
					'background' => array(115, 115, 115),
					'border' => array(255, 255, 255),
					'text' => array(255, 255, 255),
					'grid' => array(166, 166, 166)
			)
		);
		$cap = create_captcha($vals);
		$this->session->set_userdata('captchaWord',$cap['word']);
		// set data for capcha image name
		$this->session->set_userdata('captchaImage',$cap['filename']);
		echo $cap['image'];
	}

	// recieve message
	public function send() {
		// get data from models
		$this->load->model('Messages_model');

		// delete captcha when refresh paeg
		if($this->session->userdata('captchaImage')) {
			$captchaImage = $this->session->userdata('captchaImage');
			unlink(FCPATH . 'assets/images/captcha/' . $captchaImage);
			$this->session->unset_userdata('captchaImage');
		}
		
		// validation rules
		$this->form_validation->set_rules('capt', '<strong>Captcha</strong>', 'required|numeric|max_length[6]',
		[
			'required' => '<strong>Captcha</strong> tidak boleh kosong!',
			'numeric' => 'Yang anda masukkan bukan angka!',
		]);
		$this->form_validation->set_rules('email', 'email', 'required|trim|valid_email',
		[
			'required' => 'Email tidak boleh kosong!',
			'valid_email' => 'Email tidak valid!',
		]);
		$this->form_validation->set_rules('message', 'pesan', 'required|trim|max_length[10000]',
		[
			'required' => 'Pesan tidak boleh kosong!',
			'max_length' => 'Pesan terlalu panjang!',
		]);
		$this->form_validation->set_rules('inquiry', 'permintaan', 'required|trim',
		['required' => 'Keterangan permintaan tidak boleh kosong!']);
		$this->form_validation->set_rules('full_name', 'nama lengkap', 'required|trim',
		['required' => 'Nama idak boleh kosong!']);
		$this->form_validation->set_rules('contact_number', 'nomor telpon', 'required|trim|numeric|max_length[25]',
		[
			'required' => 'Nomor telpon tidak boleh kosong!',
			'numeric' => 'Yang anda masukkan bukan angka!',
			'max_length' => 'Tidak boleh lebih dari 25 karatker!',
		]);
		$this->form_validation->set_rules('company_name', 'nama perusahaan', 'required|trim',
		['required' => 'Nama perusahaan tidak boleh kosong!']);
		$this->form_validation->set_rules('company_address', 'alamat perusahaan', 'required|trim|max_length[3000]',
		[
			'required' => 'Alamat tidak boleh kosong!',
			'max_length' => 'Text melebihi kapasitas!',
		]);

		if ($this->form_validation->run() == FALSE) {
			$configurations = $this->configs->getConfiguration();

			// check config for title
			(!empty($configurations['siteName'])) ? $title1 = $this->uri->segment(1) : $title1 = "THDEV";
			(!empty($configurations['siteName'])) ? $title2 = $configurations['siteName'] : $title2 = "Web Developer";

			// Generate captcha
			$random_string = random_string('numeric',6);
			$vals = array(
				'word'          => $random_string,
				'img_path'      => APPPATH.'../assets/images/captcha/',
				'img_url'       => base_url('/assets/images/captcha/'),
				'img_width'     => '250',
				'img_height'    => 70,
				'expiration'    => 7200,
				'word_length'   => 8,
				'font_size'     => 17,	
				// White background and border, black text and red grid
				'colors'        => array(
						'background' => array(115, 115, 115),
						'border' => array(255, 255, 255),
						'text' => array(255, 255, 255),
						'grid' => array(166, 166, 166)
				)
			);
			$cap = create_captcha($vals);
			// set data for captcha word
			$this->session->set_userdata('captchaWord',$cap['word']);
			// set data for capcha image name
			$this->session->set_userdata('captchaImage',$cap['filename']);

			$data = [
				'captcha'	=> $cap['image'],
				'configs' 	=> $this->configs->getConfiguration(),
				'title' 		=> $title1.' | '.$title2,
			];
			// if validation errors redirect to page
			$this->templating->load('layouts/public/wrapper', 'contact', $data);
		} else {
			// success
			$captcha = $this->input->post('capt');
			if($this->session->userdata('captchaWord') == $captcha) {
				$this->Messages_model->create();
				if ($this->db->affected_rows() < 0) {
					// if failed
					$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Pesan gagal terkirim!</div>');
					redirect('contact/');
				} else {
					// if success
					$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable py-4" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="fas fa-check fa-2x"></i> <b>Pesan telah terkirim!</b> Untuk informasi selanjutnya pihak kami akan segera meresponnya.</div>');
					redirect('contact/send/');
				}
			} else {
				// delete captcha when refresh paeg
				if($this->session->userdata('captchaImage')) {
					$captchaImage = $this->session->userdata('captchaImage');
					unlink(FCPATH . 'assets/images/captcha/' . $captchaImage);
					$this->session->unset_userdata('captchaImage');
				}
				$this->session->set_flashdata('message', '<div class="alert alert-danger shadow alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Captcha tidak sesuai dengan gambar!</div>');
				redirect('contact/send/');
			}
		}
	}

	// function delete message
	public function delete($id) {
		$this->load->model('Messages_model');
		// get data from models
		$data = ['id' => $id, 'message' => $this->Messages_model->getMessageById($id)];
		// call functionn delete from
		$this->Messages_model->delete($data);

		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to delete this message!</div>');
			redirect('admin/dashboard/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Message has been delete!</div>');
			redirect('admin/dashboard/');
		}
	}
}

/* End of file Contact.php */
/* Location: ./application/controllers/Index.php */