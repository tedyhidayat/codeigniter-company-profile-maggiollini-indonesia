<button type="button" class="btn btn-warning btn-sm btn-flat" data-toggle="modal"
    data-target="#detail<?= $row['id'] ?>">
    <i class="fa fa-eye"></i>
</button>
<div class="modal fade" id="detail<?= $row['id'] ?>">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"><?= $row['partner_name']; ?></h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                    <?php if($row['image']) : ?>
                        <img class="img-responsive" src="<?= base_url('assets/images/partners/' . $row['image']); ?>">
                    <?php else : ?>
                        N/A
                    <?php endif; ?>
                    </div>
                    <div class="col-xs-12 col-md-8">
                        <h4>
                            Status :
                            <?php if($row['status'] == 'Pending') : ?>
                            <span class="btn btn-xs btn-warning btn-flat">Pending</span>
                            <?php elseif($row['status'] == 'Publish') : ?>
                            <span class="btn btn-xs btn-success btn-flat">Publish</span>
                            <?php endif; ?>
                        </h4>
                        <table class="table table-hover">
                            <tbody>
                                <tr>
                                    <th>Created by</th>
                                    <th>Created at</th>
                                    <th>Updated at</th>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-user"></i> <?= $row['full_name'] ?></td>
                                    <td>
                                        <i class="fa fa-calendar"></i> 
                                        <?= date("l, d F Y", strtotime($row['created_at'])); ?>
                                    </td>
                                    <td>
                                        <i class="fa fa-calendar"></i> 
                                        <?php if($row['updated_at']) : ?>
                                            <?= date("l, d F Y", strtotime($row['updated_at'])); ?>
                                        <?php else : ?>
                                            N/A
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p><b>Deputy :</b> <?= $row['deputy_name']; ?></p>
                        <p><b>Email :</b> <?= $row['email']; ?></p>
                        <p><b>Phone Number :</b> <?= $row['phone_number']; ?></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn pull-right btn-danger btn-sm btn-flat" data-dismiss="modal">Close</a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>



<!-- /.modal -->
