<?php if($configs) : ?>   
    <!-- Breadcrumbs -->
    <section id="breadcrumbs">
        <div class="container">
            <div class=" row justify-content-between align-items-center">
                <div class="col-12 col-md col-lg">
                    <h2 class="text-uppercase"><?= $this->uri->segment(1); ?></h2>
                </div>
                <div class="col-12 col-md col-lg">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= site_url('/'); ?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?= $this->uri->segment(1); ?></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- post -->
    <main>
        <!-- News -->
        <section id="news">
            <div class="container">
                <div class="row">
                    <!-- ===============list news======================== -->
                    <div class="col-md-8 news-main">
                    <?php if($posts) : ?>   
                        <!-- news item -->
                        <?php foreach($posts as $row_post) : ?>
                        <article class="entry-news" data-aos="fade-up">
                            <div class="card">
                                <div class="img-news">
                                    <img src="<?= base_url('assets/images/posts/'.$row_post['image']); ?>" class="img-fluid" alt="<?= $row_post['title']; ?>">
                                </div>
                                <div class="card-body entry-content">
                                  <h2 class="card-title"><?= $row_post['title']; ?></h2>
                                  <div class="meta">
                                      <span class="meta-item"><i class="fas fa-fw fa-folder"></i> <?= $row_post['name']; ?></span>
                                      <span class="meta-item"><i class="fas fa-fw fa-user"></i> <?= $row_post['full_name']; ?></span>
                                      <span class="meta-item"><i class="fas fa-fw fa-calendar"></i> <?= date('d F Y', strtotime($row_post['created_at'])); ?></span>
                                  </div>
                                  <p class="card-text news-content">
                                    <?= word_limiter(strip_tags($row_post['description']), 60); ?>
                                  </p>
                                  <a href="<?= site_url('news/post/'.$row_post['slug']); ?>" class="btn btn-general">Baca Selengkapnya</a>
                                </div>
                              </div>
                        </article>
                        <?php endforeach; ?>

                        <!-- pagination -->
                        <div class="pagination">
                            <?php if(isset($pagination)) : ?>
                                <nav aria-label="Page navigation example" class="limiting-data">
                                    <?= $pagination; ?>
                                </nav>
                            <?php endif; ?>
                        </div>
                    <?php else : ?>
                        <div class="card mb-3" data-aos="fade-up">
                            <div class="card-body text-center">
                                <h3>Belum ada berita yang tersedia.</h3>
                                <p><?= $configs['siteUrl']; ?></p>
                            </div>
                        </div>
                    <?php endif; ?>
                    </div>
                    <!-- ===============sidebar of news==================== -->
                    <aside class="col-md-4 sidebar" data-aos="fade-up">
                        <!-- search news -->
                        <div class="form-search">
                            <h3>Pencarian</h3>
                            <?= form_open('news/search/'); ?>
                                <div class="input-group mb-2">
                                    <input name="keywords" type="text" class="form-control" placeholder="Cari berita..">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <button type="submit"><i class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                  </div>
                            <?= form_close(); ?>
                        </div>
                        <!-- recent post -->
                        <div class="recent-post">
                            <h3>Posting Terbaru</h3>
                            <?php foreach($recent as $row_recent) : ?>
                            <div class="media">
                                <div class="img-media">
                                    <img src="<?= base_url('assets/images/posts/'.$row_recent['image']); ?>" class="img-fluid" alt="...">
                                </div>
                                <div class="media-body">
                                  <h5 class="mt-0"><a href="<?= site_url('news/post/'.$row_recent['slug']); ?>"><?= $row_recent['title']; ?></a></h5>
                                  <span><?= date('d F Y', strtotime($row_recent['created_at'])); ?></span>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                        <!-- tags -->
                        <div class="tags">
                            <h3>Kategori</h3>
                            <ul class="nav flex-row tags-item">
                                <?php foreach($category as $row_category) : ?>
                                <li class="nav-item">
                                  <a class="nav-link" href="<?= site_url('news/category/'.$row_category['slug']); ?>"><?= $row_category['name']; ?></a>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </aside>
                </div>
            </div>
        </section>
    </main>
<?php else : ?>
    <section>
        <div class="container" style="min-height: 300px; ">
            <div class="row align-items-center">
                <div class="col-12 text-center">
                    <h2 class="mt-5">EMPTY!</h2>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>