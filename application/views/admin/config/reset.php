
<button type="button" class="btn btn-danger btn-sm btn-flat pull-right" data-toggle="modal" data-target="#reset<?= $row['id'] ?>">
    <i class="fa fa-trash"></i> Reset Configurations
</button>
<div class="modal fade" id="reset<?= $row['id'] ?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Are you sure to reset site configuration ?</h4>
            </div>
            
            <div class="modal-footer">
                <a href="<?= base_url('admin/configurations/reset/' . $row['id'])?>"
                    class="btn btn-danger btn-flat pull-right"><i class="fa fa-trash"></i> Reset Now</a>
                <a href="#" class="btn btn-default btn-flat pull-left" data-dismiss="modal">Cancel</a>
            </div>
        </div>
        <!-- /.modal-content-->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

