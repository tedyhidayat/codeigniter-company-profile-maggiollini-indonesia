<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projects extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		create_cookie();
		$this->load->model('Projects_model');
		$this->load->model('Sample_model');
		$this->load->model('Category_projects_model');
		$this->load->model('Category_sample_model');
		
	}

	// function show projects 
	public function finished_projects()	{
		// unset sessinon captcha if there
		$this->session->unset_userdata('admin_access');
		$this->session->unset_userdata('captchaImage');
		// identification
		$configurations = $this->configs->getConfiguration();

		// pagination projects
		$this->load->library('pagination');

		$config['base_url'] = base_url('projects/finished-projects/');
		$config['total_rows'] = count($this->Projects_model->total());
		$config['per_page'] = 9;
		$config['uri_segment'] = 3;
		$config['prev_link'] = 'Prev';
		$config['next_link'] = 'Next';

		$limit = $config['per_page'];
		$start = ($this->uri->segment(3)) ? ($this->uri->segment(3)) : 0;
		
		$this->pagination->initialize($config);

		$category = $this->Category_projects_model->getAllCategoryprojects();
		$projects = $this->Projects_model->public_projects_active($limit, $start);

		// check config for title
		(!empty($configurations['siteName'])) ? $title1 = $this->uri->segment(1) : $title1 = "THDEV";
		(!empty($configurations['siteName'])) ? $title2 = $configurations['siteName'] : $title2 = "Web Developer";

		$data = [
			'limit'			=> $limit,
			'projects' 		=> $projects,
			'category'		=> $category,
			'total'			=> $config['total_rows'],
			'pagination' 	=> $this->pagination->create_links(),
			'configs' 		=> $this->configs->getConfiguration(),
			'config_page' 	=> $this->config_page->getAllConfigPageActiveHeading(),
			'title' 		=> $title1.' | '.$title2,
        ];
		
		$this->templating->load('layouts/public/wrapper', 'finished_projects', $data);
	}

	// function detail Projects
	public function detail_project($slug)	{
		// identification
		$configurations = $this->configs->getConfiguration();
		$slug = $this->uri->segment(3);

		// check config for title
		(!empty($configurations['siteName'])) ? $title1 = $this->uri->segment(1) : $title1 = "THDEV";
		(!empty($configurations['siteName'])) ? $title2 = $configurations['siteName'] : $title2 = "Web Developer";

		$data = [
			'configs' 			=> $this->configs->getConfiguration(),
			'finished_project'  => $this->Projects_model->getProjectsBySlug($slug),
			'title' 		=> $title1.' | '.$title2,
        ];
        
        $this->templating->load('layouts/public/wrapper', 'finished_projects_detail', $data);
	}

	// function show sample 
	public function sample()	{
		// identification
		$configurations = $this->configs->getConfiguration();

		// pagination projects
		$this->load->library('pagination');

		$config['base_url'] = base_url('projects/sample/');
		$config['total_rows'] = count($this->Sample_model->total());
		$config['per_page'] = 9;
		$config['uri_segment'] = 3;
		$config['prev_link'] = 'Prev';
		$config['next_link'] = 'Next';

		$limit = $config['per_page'];
		$start = ($this->uri->segment(3)) ? ($this->uri->segment(3)) : 0;
		
		$this->pagination->initialize($config);

		$category = $this->Category_sample_model->getAllCategorySample();
		$sample = $this->Sample_model->public_sample_active($limit, $start);

		// check config for title
		(!empty($configurations['siteName'])) ? $title1 = $this->uri->segment(1) : $title1 = "THDEV";
		(!empty($configurations['siteName'])) ? $title2 = $configurations['siteName'] : $title2 = "Web Developer";

		$data = [
			'limit'			=> $limit,
			'sample' 		=> $sample,
			'category'		=> $category,
			'total'			=> $config['total_rows'],
			'pagination' 	=> $this->pagination->create_links(),
			'configs' 		=> $this->configs->getConfiguration(),
			'config_page' 	=> $this->config_page->getAllConfigPageActiveHeading(),
			'title' 		=> $title1.' | '.$title2,
        ];
		
		$this->templating->load('layouts/public/wrapper', 'sample', $data);
	}

	// function detail sample
	public function detail_sample($slug)	{
		// identification
		$configurations = $this->configs->getConfiguration();
		$slug = $this->uri->segment(3);

		// check config for title
		(!empty($configurations['siteName'])) ? $title1 = $this->uri->segment(1) : $title1 = "THDEV";
		(!empty($configurations['siteName'])) ? $title2 = $configurations['siteName'] : $title2 = "Web Developer";

		$data = [
			'configs' 	=> $this->configs->getConfiguration(),
			'sample'  	=> $this->Sample_model->getSampleBySlug($slug),
			'title' 		=> $title1.' | '.$title2,
        ];
        
        $this->templating->load('layouts/public/wrapper', 'sample_detail', $data);
	}

}

/* End of file Projects.php */
/* Location: ./application/controllers/Index.php */