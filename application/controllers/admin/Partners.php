<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Partners extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		security_access();
		$this->load->model('User_model');
		$this->load->model('Partners_model');
	}

	// function show index partner page
    public function index(){
		// get data from models
		$data = [
			'title' 	=> 'Partners',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'partners' 	=> $this->Partners_model->getAllPartners(),
		];
		// view index page partner
        $this->templating->load('layouts/admin/wrapper', 'admin/partners/index', $data);
	}
	
	// function create partner page
	public function create() {
		// get data from models
		$data = [
			'title' => 'Add Partner',
			'config' => $this->configs->getConfiguration(),
			'user' => $this->User_model->getUserBySession()
		];
		// validation rules
		$this->form_validation->set_rules('image', 'Image', 'trim');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('deputy', 'deputy', 'trim');
		$this->form_validation->set_rules('phoneNumber', 'phone number', 'trim');
		$this->form_validation->set_rules('email', 'email', 'valid_email|trim');
		$this->form_validation->set_rules('partnerName', 'partner name', 'trim');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/partners/create', $data);
		} else {
			// if validation success call function create from partners_model
			$this->Partners_model->create($data);
		}
	}

	// function edit partner page
	public function edit($id) {
		// get data from models
		$data = [
			'id' 		=> $id,
			'title' 	=> 'Edit Partner',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'partner' 	=> $this->Partners_model->getPartnersById($id),
		];
		// validation rules
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('deputy', 'deputy', 'trim');
		$this->form_validation->set_rules('phoneNumber', 'phone number', 'trim');
		$this->form_validation->set_rules('email', 'email', 'valid_email|trim');
		$this->form_validation->set_rules('partnerName', 'partner name', 'trim');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/partners/update', $data);
		} else {
			// if validation success call function edit from partners_model
			$this->Partners_model->edit($data);
		}
	}

	public function delete($id) {
		// get data from models
		$data = ['id' => $id, 'partner' => $this->Partners_model->getPartnersById($id)];
		// call function delete from partners_model
		$this->Partners_model->delete($data);
	}
}

/* End of file Partners.php */

?>