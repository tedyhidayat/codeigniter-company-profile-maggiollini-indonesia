<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Messages_model extends CI_Model {

	// function get all data Messages
	public function getAllMessages() {
		return $this->db->get('tbl_message')->result_array();
	}

	// Function get data message
	public function getMessageById($id) {
		return $this->db->get_where('tbl_message', ['id' => $id])->row_array();
	}

	// Function send message 
	public function create() {	
		// get data from input form
		$data = [
			'email' 			=> htmlspecialchars($this->input->post('email', true)),
			'inquiry' 			=> htmlspecialchars($this->input->post('inquiry', true)),
			'message' 			=> htmlspecialchars($this->input->post('message', true)),
			'subject' 			=> htmlspecialchars($this->input->post('subject', true)),
			'company_name' 		=> htmlspecialchars($this->input->post('company_name', true)),
			'contact_number' 	=> htmlspecialchars($this->input->post('contact_number', true)),
			'company_address' 	=> htmlspecialchars($this->input->post('company_address', true)),
			'full_name' 		=> htmlspecialchars($this->input->post('title', true)).'. '.htmlspecialchars($this->input->post('full_name', true)),
		];
		// query insert
		$this->db->insert('tbl_message', $data);
	}

	// Function delete data Posts category
	public function delete($data) {
		// query delete
		$this->db->where('id', $data['id']);
		$this->db->delete('tbl_message');
	}

}

/* End of file Messages_model.php */
/* Location: ./application/models/Messages_model.php */