   <!-- Breadcrumbs -->
   <section id="breadcrumbs">
        <div class="container">
            <div class=" row justify-content-between align-items-center">
                <div class="col-12 col-md col-lg">
                    <h2 class="text-uppercase"><?= $this->uri->segment(1); ?></h2>
                </div>
                <div class="col-12 col-md col-lg">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= site_url('/'); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?= site_url('/projects/sample/'); ?>"><?= $this->uri->segment(1); ?></a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?= $this->uri->segment(2); ?></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
   
   <!-- Detail Projects -->
    <section id="detail-project" class="">
        <div class="container">
            <div class="row" data-aos="fade-in">
                <div class="col-12 col-md-12 col-lg-5 p-4 p-lg-0 text-center justify-content-start detail-img">
                    <img class="img-fluid xzoom" id="xzoom-default" xoriginal="<?= base_url('assets/images/projects/sample/'. $sample['image']); ?>" src="<?= base_url('assets/images/projects/sample/'. $sample['image']); ?>" alt="<?= $sample['sample_name']; ?>" title="<?= $sample['sample_name']; ?>">
                    <div class="clearfix"></div>
                    <cite><small class="text-muted"><?= $configs['siteName']; ?></small></cite>
                </div>
                <div class="col-12 col-md-12 col-lg-7 px-4 pl-md-5 intro-txt">
                    <!-- content -->
                    <article>
                        <header>
                            <h2 class="title-detail"><?= $sample['sample_name']; ?></h2>
                            <small class="text-muted"><i class="fas fa-folder-open"></i> <?= $sample['name']; ?></small>
                            &nbsp;&nbsp;
                            <small class="text-muted"><i class="fas fa-clock"></i>  <?= date('d-F-Y', strtotime($sample['created_at'])); ?></small>
                            &nbsp;&nbsp;
                            <small class="text-muted"><i class="fas fa-tags"></i> <?= $sample['keywords']; ?></small>
                        </header>
                        <p class="mt-3 text-justify">
                            <?= $sample['description']; ?>
                        </p>
                    </article>
                    <!-- button share and back -->
                    <div class="d-md-flex justify-content-between mt-4 button">
                        <div class="share">
                            <small> <b> Share to:</b></small><br>
                            
                            <a href="http://www.facebook.com/share.php?u=<?= site_url('projects/detail-sample/'.$sample['slug']); ?>" target="_blank" class="btn"><i class="fab fa-facebook"></i></a>

                            <a href="https://www.twitter.com/intent/tweet?text=<?= $sample['sample_name']; ?>&url=<?= site_url('projects/detail-sample/'.$sample['slug']); ?>&text=<?= $sample['name']; ?>;hashtags=design" target="_blank" class="btn"><i class="fab fa-twitter"></i></a>

                            <a href="whatsapp://send?text=<?= site_url('projects/detail-sample/'.$sample['slug']); ?>" class="btn"><i class="fab fa-whatsapp"></i></a>

                        </div>
                        <div class="button-back">
                            <a href="<?= site_url('/projects/sample/'); ?>" class="btn btn-general text-uppercase mt-4"><i class="fas fa-fw fa-arrow-left"></i> Kembali ke Sampel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>