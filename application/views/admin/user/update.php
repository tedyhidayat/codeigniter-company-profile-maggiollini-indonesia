<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $title; ?>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <?= $this->session->flashdata('message'); ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="col-xs-12 col-md-6 col-md-offset-3">
                        <form action="<?= site_url('admin/user/edit/'.$userId['id']); ?>" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" class="form-control" value="<?= $this->security->get_csrf_hash(); ?>" >
                            <div class="form-group">
                                <label>Status <span class="text-danger">*</span></label>
                                <select name="status" id="status" class="form-control
                                <?= $userId['status'] == 1 ? 'bg-green' : 'bg-red' ?>">
                                    <?php if ($userId['status'] == 1) : ?>
                                        <option value="1">Active</option>
                                        <option value="0">Blocked</option>
                                    <?php elseif ($userId['status'] == 0) : ?>
                                        <option value="0">Blocked</option>
                                        <option value="1">Active</option>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Full Name <span class="text-danger">*</span></label>
                                <input type="text" name="full_name" class="form-control" value="<?= $userId['full_name']; ?>">
                                <?= form_error('full_name', '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                            <label for="status">Gender</label>
                            <select name="gender" id="status" class="form-control">
                                <?php if (!$userId['gender']) : ?>
                                    <option value="">Selecet Gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                <?php else : ?>
                                    <?php if ($userId['gender'] == 'Male') : ?>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    <?php elseif ($userId['gender'] == 'Female') : ?>
                                        <option value="Female">Female</option>
                                        <option value="Male">Male</option>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                            <div class="form-group">
                                <label>Email <span class="text-danger">*</span></label>
                                <input type="text" name="email" class="form-control"
                                    value="<?= $userId['email']; ?>">
                                <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                                <label>Username <span class="text-danger">*</span></label>
                                <input type="text" name="username" class="form-control" value="<?= $userId['username']; ?>">
                                <?= form_error('username', '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-2">
                                    <img width="100" src="<?= base_url('assets/images/user/'.$userId['profile_picture']); ?>" alt="" class="img-thumbnail">
                                </div>
                                <div class="col-xs-12 col-md-10">
                                    <label>Profile Picture <span class="text-danger">*</span></label>
                                    <input type="file" name="profile_picture" class="form-control">
                                </div>
                            </div>                                
                            <div class="row" style="margin-top: 25px;">
                                <div class="col-xs-12">
                                    <button type="submit" class="btn btn-success btn-flat pull-right">Edit</button>
                                    <a class="pull-right btn" href="<?= site_url('admin/user/'); ?>">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>