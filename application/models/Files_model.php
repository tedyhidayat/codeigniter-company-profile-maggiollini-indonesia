<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Files_model extends CI_Model {

	// function all data files
	public function getAllFiles() {
		/**
		 * get data tbl_file: all
		 * tbl_files join to tbl_users and tbl_category_files
		 */
		$this->db->select('tbl_files.*, tbl_category_files.name, tbl_users.full_name' );
		$this->db->from('tbl_files');
		$this->db->join('tbl_category_files','tbl_category_files.id = tbl_files.id_category', 'LEFT');
		$this->db->join('tbl_users','tbl_users.id = tbl_files.id_user', 'LEFT');
		$this->db->order_by('tbl_files.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// function all data files
	public function getAllFileActive() {
		/**
		 * get data tbl_file: all
		 * tbl_files join to tbl_users and tbl_category_files
		 */
		$this->db->select('tbl_files.*, tbl_category_files.name, tbl_users.full_name' );
		$this->db->from('tbl_files');
		$this->db->join('tbl_category_files','tbl_category_files.id = tbl_files.id_category', 'LEFT');
		$this->db->join('tbl_users','tbl_users.id = tbl_files.id_user', 'LEFT');
		$this->db->where('tbl_files.status','Publish');
		$this->db->order_by('tbl_files.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	// function get file by ID
	public function getFilesById($id) {
		return $this->db->get_where('tbl_files', ['id' => $id])->row_array();
	}

	// function create data file
	public function create($data) {
		// get data from input file 
		$upload_image = $_FILES['file']['name'];
		if ($upload_image) {
			// define file uploaded
			$config['max_size']     	= 10240; //10mb
			$config['allowed_types'] 	= 'pdf|docx|pptx|';
			$config['upload_path'] 		= './assets/files/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-file-';
			// initializing file uploaded
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('file')) {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . '</div>');
				redirect('admin/files/create');
			} else {
				// if success
				// collacting data for insert table 
				$data = [
					'id_user' 		=> $data['user']['id'],
					'created_at'	=> date('Y-m-d H:i:s'),
					'file_name' 	=> $this->upload->data('file_name'),
					'title' 		=> $this->input->post('title', true),
					'status' 		=> $this->input->post('status', true),
					'id_category'	=> $this->input->post('categId', true),
					'keywords' 		=> $this->input->post('keywords', true),
					'description' 	=> $this->input->post('description', true),
					'slug' 			=> url_title($this->input->post('title', true), 'dash', true),
				];
			}
		} else {
			// collactoing data to insert
			$data = [
				'id_user' 		=> $data['user']['id'],
				'created_at'	=> date('Y-m-d H:i:s'),
				'title' 		=> $this->input->post('title', true),
				'status' 		=> $this->input->post('status', true),
				'id_category'	=> $this->input->post('categId', true),
				'keywords' 		=> $this->input->post('keywords', true),
				'description' 	=> $this->input->post('description', true),
				'slug' 			=> url_title($this->input->post('title', true), 'dash', true),
			];
		}
		// query insert
		$this->db->insert('tbl_files', $data);
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to insert data!</div>');
			redirect('admin/files/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Success!</div>');
			redirect('admin/files/');
		}
		
	}

	// function edit files
	public function edit($data) {
		// get data from input form
		$title 			= $this->input->post('title', true);
		$status 		= $this->input->post('status', true);
		$id_category	= $this->input->post('categId', true);
		$keywords 		= $this->input->post('keywords', true);
		$description 	= $this->input->post('description', true);
		$slug 			= url_title($this->input->post('title', true), 'dash', true);
		$upload_image 	= $_FILES['file']['name'];
		if ($upload_image) {
			// define input file uploaded
			$config['max_size']     	= 10240; //10mb
			$config['allowed_types'] 	= 'pdf|docx|pptx|';
			$config['upload_path'] 		= './assets/files/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-file-';
			// initializing configuration file
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('file')) {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . ' You only can upload '. $config['allowed_types'] . ' type.' . '</div>');
				redirect('admin/files/edit/'. $data['id']);
			} else {
				// if suceess
				// unlik old file
				$old_image = $data['file']['file_name'];
				if ($old_image != 'default.png') {unlink(FCPATH . 'assets/files/' . $old_image);}
				// set image to table
				$this->db->set('file_name', $this->upload->data('file_name'));
			}
		}
		// query update
		$this->db->set('slug', $slug);
		$this->db->set('title', $title);
		$this->db->set('status', $status);
		$this->db->set('keywords', $keywords);
		$this->db->set('id_category', $id_category);
		$this->db->set('description', $description);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $data['id']);
		$this->db->update('tbl_files');
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your file ' . $title . ' failed to edit!</div>');
			redirect('admin/files/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your file ' . $title . ' has been updated!</div>');
			redirect('admin/files/');
		}
		
	}

	// function delete data files
	public function delete($data) {
		// get old file
		$old_file= $data['file']['file_name'];
		// unlink file
		if ($old_file) {
			unlink(FCPATH . 'assets/files/' . $old_file);
		}
		// query delete
		$this->db->where('id', $data['id']);
		$this->db->delete('tbl_Files');
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to delete data!</div>');
			redirect('admin/files/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your file has been delete!</div>');
			redirect('admin/files/');
		}
		
	}
}

/* End of file Files_model.php */
/* Location: ./application/models/Files_model.php */