<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		create_cookie();
		$this->load->model('Gallery_model');
		$this->load->model('Videos_model');
		$this->load->model('Category_gallery_model');
	}

	// function show  page
	public function index()	{
		// unset sessinon captcha if there
		$this->session->unset_userdata('admin_access');
		$this->session->unset_userdata('captchaImage');
		// identification
		$configurations = $this->configs->getConfiguration();

		// check config for title
		(!empty($configurations['siteName'])) ? $title1 = $this->uri->segment(1) : $title1 = "THDEV";
		(!empty($configurations['siteName'])) ? $title2 = $configurations['siteName'] : $title2 = "Web Developer";

		$data = [
			'configs' 		=> $this->configs->getConfiguration(),
			'videos' 		=> $this->Videos_model->public_videos_active(),
			'gallery' 		=> $this->Gallery_model->getAllGalleryActive(),
			'config_page' 	=> $this->config_page->getAllConfigPageActiveHeading(),
			'title' 		=> $title1.' | '.$title2,
        ];
        
        $this->templating->load('layouts/public/wrapper', 'gallery', $data);
    }

}

/* End of file Gallery.php */
/* Location: ./application/controllers/Index.php */