!(function($) {
    "BLOCK";

    /**Prevent download image */
    $(document).ready(function() {
        $('img').bind('contextmenu',function(e){
            return false;
        });
    });
    
    /* carousel bootstrap swipe hand */
    $('.carousel').on('slide.bs.carousel', function () {
        touch: true
    });

    /* Back to top button */ 
    $(window).scroll(function() {
        if ($(this).scrollTop() > 200) {
        $('.back-to-top').fadeIn('slow');
        } else {
        $('.back-to-top').fadeOut('slow');
        }
    });

    $('.back-to-top').click(function() {
        $('html, body').animate({
        scrollTop: 0
        }, 1500, 'easeInOutExpo');
        return false;
    });

    /* xzoom detail image */ 
    $(document).ready(function() {
        $('.xzoom, .xzoom-gallery').xzoom({
            zoomWidth: 670,
            zoomHeight: 400,
            title: false,
            fadeOut: true,
            tint: '#333',
            Xoffset: 10,
        });
    });

    /*Porfolio filter with filtrizr*/ 
    $(window).on('load', function() {
        /*active button filtr on click*/ 
        $('#projects .nav .nav-item a').click(function() {
            $('.nav-item a').removeClass('active');
            $(this).addClass('active');
        });
        const options = {
           easing: 'ease-out',
           filter: 'all',
           layout: 'sameSize',
           delay: 15,
           delayMode: 'progressive',
           gutterPixels: 0,
           setupControls: true,
           animationDuration: 0.4,
           filterOutCss: {
                opacity: 0,
                transform: 'scale(0.5)'
           },
           filterIntCss: {
                opacity: 0,
                transform: 'scale(1)'
           },
           spinner: {
               enabled: true,
               fillColor: '#2184d0',
               styles: {
                   height: '75px',
                   width: '75px',
                   margin: '0 auto',
                   'z-index': 2,
               }
           }
        }
        const filterizr = new Filterizr('.filter-container', options);
        /*Initiate venobox (lightbox feature used in portofilo)*/ 
        $(document).ready(function() {
            $('.venobox').venobox({
                share: [],
                numeratio :true,
                infinigall: true,
                titlePosition: 'bottom',
                border: '5px',
                spinner: 'wave'
            });
        });
    });

    /*Portfolio details carousel*/ 
    $(".box-clients").owlCarousel({
        autoplay: true,
        dots: false,
        loop: true,
        margin: 8,
        items:1,
        nav: false,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    });

    /* aos animate */ 
    AOS.init({
        duration: 800,
        easing: "ease-in-out"
    });
})(jQuery);