<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?= $title; ?></h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- flash data -->
    <?= $this->session->flashdata('message'); ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-success">
                <div class="box-header">
                    <a href="<?= site_url('admin/config-page/create'); ?>" class="btn btn-success btn-sm btn-flat"><i
                            class="fa fa-plus"></i> Create New</a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="50">NO.</th>
                                    <th>Image</th>
                                    <th>Title Page</th>
                                    <th>Position</th>
                                    <th>Type</th>
                                    <th>Description</th>
                                    <th class="text-center">Status</th>
                                    <th width="100">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!$config_page) : ?>
                                <tr>
                                    <td class="text-center" colspan="6">Empty!</td>
                                </tr>
                                <?php else : ?>
                                <?php $no = 1; foreach ($config_page as $row): ?>
                                <tr>
                                    <td><?= $no++; ?>.</td>
                                    <td><img width="70" class="img-thumbnail"
                                            src="<?= base_url('assets/images/configurations/config_page/thumbs/' . $row['image']); ?>"
                                            alt=""></td>
                                    <td><b><?= $row['title']; ?></b></td>
                                    <td><?= $row['page_position']; ?></td>
                                    <td><?= $row['type']; ?></td>
                                    <td><?= $row['description']; ?></td>
                                    <td class="text-center">
                                        <?php if($row['status'] == 'Pending') : ?>
                                        <span class="btn btn-xs btn-warning btn-flat">Pending</span>
                                        <?php elseif($row['status'] == 'Publish') : ?>
                                        <span class="btn btn-xs btn-success btn-flat">Publish</span>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <a href="<?= site_url('admin/config-page/edit/' . $row['id']); ?>"
                                            class="btn btn-info btn-sm btn-flat"><i class="fa fa-pencil"></i></a>
                                        <?php include('delete.php') ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>
