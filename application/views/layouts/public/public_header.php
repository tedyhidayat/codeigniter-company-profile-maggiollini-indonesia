<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <title><?= $title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php if($configs): ?>
    <meta name="author" content="<?= $configs['siteName']; ?>">
    <meta name="description" content="<?= $configs['metaDesc']; ?>">
    <meta name="keywords" content="<?= $configs['keywords']; ?>">
    <link rel="shortcut icon" href="<?= base_url('assets/images/configurations/'. $configs['icon']); ?>" type="image/x-icon">
    <?php endif; ?>
    <meta http-equiv="refresh" content="3600">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="<?= base_url('assets/frontend/'); ?>libraries/bootstrap/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="<?= base_url('assets/frontend/'); ?>libraries/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/frontend/'); ?>libraries/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/frontend/'); ?>libraries/animate.css/animate.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/frontend/'); ?>libraries/venobox/venobox.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/frontend/'); ?>libraries/aos/aos.css">
    <link rel="stylesheet" href="<?= base_url('assets/frontend/'); ?>styles/main.css">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->

  </head>
  <body>