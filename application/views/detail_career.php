   <!-- Breadcrumbs -->
   <section id="breadcrumbs">
        <div class="container">
            <div class=" row justify-content-between align-items-center">
                <div class="col-12 col-md col-lg">
                    <h2 class="text-uppercase"><?= $this->uri->segment(1); ?></h2>
                </div>
                <div class="col-12 col-md col-lg">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= site_url('/'); ?>">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?= site_url($this->uri->segment(1)); ?>"><?= $this->uri->segment(1); ?></a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?= $this->uri->segment(2); ?></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
   
   <!-- Detail services -->
   <section id="detail-services" class="">
        <div class="container">
            <div class="row animated fadeIn">
                <div class="col-12 col-md-12 col-lg-4 px-4 p-lg-0 text-center justify-content-start detail-img">
                    <img class="img-responsive" src="<?= base_url('assets/images/careers/'.$career['image']); ?>" alt="<?= $career['title']; ?>">
                    <cite><small class="text-muted"><?= $configs['siteName']; ?></small></cite>
                </div>
                <div class="col-12 col-md-12 col-lg-8 px-4 pl-md-5 services-txt">
                    <article>
                        <header>
                            <h2 class="title-detail"><?= $career['title']; ?></h2>
                            <span class="text-muted"><i class="fas fa-fw fa-user"></i>Posisi:  <?= $career['position']; ?></span>
                            &nbsp;&nbsp;
                            <span class="text-muted"><i class="fas fa-fw fa-dollar-sign"></i> <mark> Rp. <?= number_format($career['salary'], 2, ',', '.'); ?> /Month</mark></span>
                        </header>
                        <hr>
                        <p class="mt-3 text-justify"><?= $career['description']; ?></p>
                    </article>
                    <!-- share -->
                    <div class="d-md-flex justify-content-between mt-4 button">
                        <div class="share">
                            <small> <b> Bagikan dengan:</b></small><br>

                            <a href="http://www.facebook.com/share.php?u=<?= site_url('careers/detail/'.$career['slug']); ?>" target="_blank" class="btn"><i class="fab fa-facebook"></i></a>

                            <a href="https://www.twitter.com/intent/tweet?text=<?= $career['title']; ?>&url=<?= site_url('careers/detail/'.$career['slug']); ?>&text=<?= $career['position']; ?>;hashtags=design" target="_blank" class="btn"><i class="fab fa-twitter"></i></a>

                            <a href="whatsapp://send?text=<?= site_url('careers/detail/'.$career['slug']); ?>" class="btn"><i class="fab fa-whatsapp"></i></a>
                        </div>
                        <div class="button-back">
                            <a href="<?= site_url('/careers/'); ?>" class="btn btn-general text-uppercase mt-4"><i class="fas fa-fw fa-arrow-left"></i> Kembali ke karir</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>