<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts_model extends CI_Model {

	// sitemap
	public function sitemap() {
		$this->db->select('tbl_posts.slug,tbl_posts.created_at,tbl_posts.title');
		$this->db->from('tbl_posts');
		$this->db->where('tbl_posts.status', 'Publish');
		$this->db->order_by('tbl_posts.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// funcrtion get all Posts 
	public function getAllPosts() {
		/**
		 * tbl_posts join tbl_users and tbl_categories Posts
		 * tbl_caegory Posts: get category name, tbl_users: get full name
		 */
		$this->db->select('tbl_posts.*, tbl_categories.name, tbl_users.full_name' );
		$this->db->from('tbl_posts');
		$this->db->join('tbl_categories','tbl_categories.id = tbl_posts.id_category', 'LEFT');
		$this->db->join('tbl_users','tbl_users.id = tbl_posts.id_user', 'LEFT');
		$this->db->order_by('tbl_posts.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// function get serval datta Posts
	public function getServalPosts() {
		/**
		 * tbl_posts join tbl_users and tbl_categories Posts
		 * tbl_caegory Posts: get category name, tbl_users: get full name
		 * only show serval data, max 5 data  where recent data
		 */
		$this->db->select('tbl_posts.*, tbl_categories.name, tbl_users.full_name' );
		$this->db->from('tbl_posts');
		$this->db->join('tbl_categories','tbl_categories.id = tbl_posts.id_category', 'LEFT');
		$this->db->join('tbl_users','tbl_users.id = tbl_posts.id_user', 'LEFT');
		$this->db->limit(5);
		$this->db->order_by('tbl_posts.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// funcrtion get all Posts Active
	public function getAllPostActive($limit, $start) {
		/**
		 * tbl_posts join tbl_users and tbl_categories Posts
		 * tbl_caegory Posts: get category name, tbl_users: get full name
		 */
		$this->db->select('tbl_posts.*, tbl_categories.name, tbl_users.full_name' );
		$this->db->from('tbl_posts');
		$this->db->join('tbl_categories','tbl_categories.id = tbl_posts.id_category', 'LEFT');
		$this->db->join('tbl_users','tbl_users.id = tbl_posts.id_user', 'LEFT');
		$this->db->where('tbl_posts.status','Publish');
		$this->db->order_by('tbl_posts.id','DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		return $query->result_array();
	}

	// funcrtion get all Posts Active
	public function getRecentPost() {
		/**
		 * tbl_posts join tbl_users and tbl_categories Posts
		 * tbl_caegory Posts: get category name, tbl_users: get full name
		 */
		$this->db->select('tbl_posts.*, tbl_categories.name, tbl_users.full_name' );
		$this->db->from('tbl_posts');
		$this->db->join('tbl_categories','tbl_categories.id = tbl_posts.id_category', 'LEFT');
		$this->db->join('tbl_users','tbl_users.id = tbl_posts.id_user', 'LEFT');
		$this->db->where('tbl_posts.status','Publish');
		$this->db->order_by('tbl_posts.id','DESC');
		$this->db->limit(4);
		$query = $this->db->get();
		return $query->result_array();
	}

	// funcrtion get all Posts by category
	public function getPostByCategory($slug) {
		/**
		 * tbl_posts join tbl_users and tbl_categories Posts
		 * tbl_caegory Posts: get category name, tbl_users: get full name
		 */
		$this->db->select('tbl_posts.*,tbl_posts.slug as slug_post, tbl_categories.*, tbl_users.full_name' );
		$this->db->from('tbl_posts');
		$this->db->join('tbl_categories','tbl_categories.id = tbl_posts.id_category', 'LEFT');
		$this->db->join('tbl_users','tbl_users.id = tbl_posts.id_user', 'LEFT');
		$this->db->where('tbl_categories.slug', $slug);
		$this->db->order_by('tbl_posts.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// function get Posts by slug
	public function getPostBySlug($slug) {
		$this->db->select('tbl_posts.*, tbl_categories.name, tbl_users.full_name' );
		$this->db->from('tbl_posts');
		$this->db->join('tbl_categories','tbl_categories.id = tbl_posts.id_category', 'LEFT');
		$this->db->join('tbl_users','tbl_users.id = tbl_posts.id_user', 'LEFT');
		$this->db->where('tbl_posts.slug',$slug);
		$query = $this->db->get();
		$res = $query->row_array();

		if(!$res) {
			redirect('errors/error404','refresh');
			die;
		} else {
			return $res;
		}
	}

	// funcrtion get all Posts Active
	public function search($keywords) {
		/**
		 * tbl_posts join tbl_users and tbl_categories Posts
		 * tbl_caegory Posts: get category name, tbl_users: get full name
		 */
		$this->db->select('tbl_posts.*, tbl_categories.name, tbl_users.full_name' );
		$this->db->from('tbl_posts');
		$this->db->join('tbl_categories','tbl_categories.id = tbl_posts.id_category', 'LEFT');
		$this->db->join('tbl_users','tbl_users.id = tbl_posts.id_user', 'LEFT');
		$this->db->like('title', $keywords);
		$this->db->or_like('tbl_categories.name', $keywords);
		$query = $this->db->get();
		return $query->result_array();
	}

	// funcrtion get all Posts Active total
	public function total() {
		/**
		 * tbl_posts join tbl_users and tbl_categories Posts
		 * tbl_caegory Posts: get category name, tbl_users: get full name
		 */
		$this->db->select('tbl_posts.*, tbl_categories.name, tbl_users.full_name' );
		$this->db->from('tbl_posts');
		$this->db->join('tbl_categories','tbl_categories.id = tbl_posts.id_category', 'LEFT');
		$this->db->join('tbl_users','tbl_users.id = tbl_posts.id_user', 'LEFT');
		$this->db->where('tbl_posts.status','Publish');
		$this->db->order_by('tbl_posts.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	// function get Posts by ID 
	public function getPostsById($id) {
		return $this->db->get_where('tbl_posts', ['id' => $id])->row_array();
	}

	// function create Posts
	public function create($data) {
		// get uploaded file 
		$upload_image = $_FILES['image']['name'];
		// check uploaded file
		if ($upload_image) {
			// define config for upload file
			$config['max_size']     	= 2048;
			$config['allowed_types'] 	= 'jpg|png';
			$config['upload_path'] 		= './assets/images/posts/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-post-';
			// initializing config uploading file
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('image')) {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . '</div>');
				redirect('admin/Posts');
			} else {
				// if success
				// deffine config for creating thumbnail
				$file = $this->upload->data('file_name');
				$config['image_library'] 	= 'gd2';
				$config['create_thumb'] 	= TRUE;
				$config['maintain_ratio'] 	= TRUE;
				$config['width']         	= 500;
				$config['height']       	= 470;
				$config['thumb_marker']    	= '';
				$config['source_image']	 	= './assets/images/posts/'.$file;
				$config['new_image']	 	= './assets/images/posts/thumbs/'.$file;
				// initializing config uploaded file
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				// collecting data for insert to table
				$data = [
					'image' 		=> $file,
					'id_user' 		=> $data['user']['id'],
					'created_at'	=> date('Y-m-d H:i:s'),
					'type' 			=> $this->input->post('type', true),
					'title' 		=> $this->input->post('title', true),
					'status' 		=> $this->input->post('status', true),
					'id_category'	=> $this->input->post('categId', true),
					'keywords' 		=> $this->input->post('keywords', true),
					'description' 	=> $this->input->post('description', true),
					'slug' 			=> url_title($this->input->post('title', true), 'dash', true),
				];
			}
		} else {
			// if nothing uploaded file
			// collecting data for insert to table
			$data = [
				'image' 		=> 'default.png',
				'id_user' 		=> $data['user']['id'],
				'created_at'	=> date('Y-m-d H:i:s'),
				'type' 			=> $this->input->post('type', true),
				'title' 		=> $this->input->post('title', true),
				'status' 		=> $this->input->post('status', true),
				'id_category'	=> $this->input->post('categId', true),
				'keywords' 		=> $this->input->post('keywords', true),
				'description' 	=> $this->input->post('description', true),
				'slug' 			=> url_title($this->input->post('title', true), 'dash', true),
			];
		}
		// query insert
		$this->db->insert('tbl_posts', $data);
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to insert data!</div>');
			redirect('admin/posts/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Success!</div>');
			redirect('admin/posts/');
		}
		
	}

	// function edit Posts
	public function edit($data) {
		// get data from input form
		$type 			= $this->input->post('type', true);
		$title 			= $this->input->post('title', true);
		$status 		= $this->input->post('status', true);
		$id_category	= $this->input->post('categId', true);
		$keywords 		= $this->input->post('keywords', true);
		$description 	= $this->input->post('description', true);
		$slug 			= url_title($this->input->post('title', true), 'dash', true);
		$upload_image 	= $_FILES['image']['name'];
		// check uploaded file
		if ($upload_image) {
			// define config upload file
			$config['max_size']     	= 2048;
			$config['allowed_types'] 	= 'jpg|png';
			$config['upload_path'] 		= './assets/images/posts/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-post-';
			// initializing data config upload file
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('image')) {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . '</div>');
				redirect('admin/Posts');
			} else {
				// if success
				// define config creating thumbnail
				$new_image = $this->upload->data('file_name');
				$config['image_library'] 	= 'gd2';
				$config['create_thumb'] 	= TRUE;
				$config['maintain_ratio'] 	= TRUE;
				$config['width']         	= 500;
				$config['height']       	= 470;
				$config['thumb_marker']    	= '';
				$config['source_image']	 	= './assets/images/posts/'.$new_image;
				$config['new_image']	 	= './assets/images/posts/thumbs/'.$new_image;
				// initializing config creating thumbnail
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				// get old file
				$old_image = $data['post']['image'];
				// unlink old file
				if ($old_image != 'default.png') {
					unlink(FCPATH . 'assets/images/posts/' . $old_image);
					unlink(FCPATH . 'assets/images/posts/thumbs/' . $old_image);
				}
				// set new file to table
				$this->db->set('image', $new_image);
			}
		}
		// query update
		$this->db->set('type', $type);
		$this->db->set('slug', $slug);
		$this->db->set('title', $title);
		$this->db->set('status', $status);
		$this->db->set('keywords', $keywords);
		$this->db->set('description', $description);
		$this->db->set('id_category', $id_category);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $data['id']);
		$this->db->update('tbl_posts');
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Fauiled to edit data!</div>');
			redirect('admin/posts/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your Posts has been updated!</div>');
			redirect('admin/posts/');
		}
	}
	
	// function delete Posts
	public function delete($data) {
		// get old image
		$old_image = $data['post']['image'];
		// unlink old file
		if ($old_image != 'default.png') {
			unlink(FCPATH . 'assets/images/posts/' . $old_image);
			unlink(FCPATH . 'assets/images/posts/thumbs/' . $old_image);
		}
		// query delete
		$this->db->where('id', $data['id']);
		$this->db->delete('tbl_posts');
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to delete data!</div>');
			redirect('admin/posts/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your post has been delete!</div>');
			redirect('admin/posts/');
		}
	}

}

/* End of file Posts_model.php */
/* Location: ./application/models/Posts_model.php */