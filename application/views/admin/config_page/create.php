<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?= $title; ?></h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- flash data -->
    <?= $this->session->flashdata('message'); ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="<?= site_url('admin/config-page/create'); ?>" enctype="multipart/form-data"
                    role="form">
                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" class="form-control" value="<?= $this->security->get_csrf_hash(); ?>" >
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12 col-md-8">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" name="title" class="form-control" id="title" placeholder="Title"
                                        value="<?= set_value('title'); ?>">
                                    <?= form_error('title', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>    
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="Pending">Pending</option>
                                        <option value="Publish">Publish</option>
                                    </select>
                                </div>
                            </div>    
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="position">Page Position</label>
                                    <select name="page_position" id="position" class="form-control">
                                        <option value="">Select Page</option>
                                        <option value="Home">Home</option>
                                        <option value="About">About</option>
                                        <option value="Services">Services</option>
                                        <option value="News">News</option>
                                        <option value="Careers">Careers</option>
                                        <option value="Team">Team</option>
                                        <option value="Client">Client</option>
                                        <option value="Gallery">Gallery</option>
                                        <option value="Archives">Archives</option>
                                        <option value="Contact">Contact</option>
                                        <option value="Finished Projects">Finished Projects</option>
                                        <option value="Sample Production">Sample Production</option>
                                    </select>
                                    <?= form_error('page_position', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="type">Type</label>
                                    <select name="type" id="type" class="form-control">
                                        <option value="">Select Type</option>
                                        <option value="Heading">Heading</option>
                                        <option value="Sub Heading">Sub Heading</option>
                                    </select>
                                    <?= form_error('type', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>    
                        </div>
                        <div class="form-group">
                            <label for="image">Image</label>
                            <input type="file" name="image" class="form-control" id="image">
                            <small class="text-info">* Max size 2 Mb</small><br>
                                         <small class="text-info">* Only for jpg | jpeg | png</small>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" id="description" name="description" rows="5" cols="80"><?= set_value('description'); ?></textarea>
                            <?= form_error('description', '<small class="text-danger">', '</small>'); ?>
                        </div>
                        
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success btn-flat pull-right">Save</button>
                        <a href="<?= site_url('admin/config-page/'); ?>" class="btn pull-right">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>