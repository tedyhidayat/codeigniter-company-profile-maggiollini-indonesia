<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?= $title; ?></h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- flash data -->
    <?= $this->session->flashdata('message'); ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="<?= site_url('admin/careers/edit/' . $career['id']); ?>"
                    enctype="multipart/form-data" role="form">
                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" class="form-control" value="<?= $this->security->get_csrf_hash(); ?>" >
                    <div class="box-body">
                        <input type="hidden" name="id" value="<?= $career['id']; ?>">
                        <div class="row">
                            <div class="col xs-12 col-md-8">
                                <div class="form-group">
                                    <label for="title">Job Title</label>
                                    <input type="text" name="title" class="form-control" id="title"
                                        value="<?= $career['title']; ?>">
                                    <?= form_error('title', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="col xs-12 col-md-4">
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select name="status" id="status" class="form-control">
                                        <?php if (!$career['status']) : ?>
                                        <option value="">Selecet Gender</option>
                                        <option value="Publish">Publish</option>
                                        <option value="Pending">Pending</option>
                                        <?php else : ?>
                                        <?php if ($career['status'] == 'Publish') : ?>
                                        <option value="Publish">Publish</option>
                                        <option value="Pending">Pending</option>
                                        <?php elseif ($career['status'] == 'Pending') : ?>
                                        <option value="Pending">Pending</option>
                                        <option value="Publish">Publish</option>
                                        <?php endif; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col xs-12 col-md-8">
                                <div class="form-group">
                                    <label for="position">Position</label>
                                    <input type="text" name="position" class="form-control" id="position"
                                        value="<?= $career['position']; ?>">
                                    <?= form_error('position', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="col xs-12 col-md-4">
                                <div class="form-group">
                                    <label for="salary">Salary</label>
                                    <input type="number" name="salary" class="form-control" id="salary"
                                        value="<?= $career['salary']; ?>">
                                    <?= form_error('salary', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label for="image">Image</label>
                            <div class="row">
                                <div class="col-xs-3 col-md-2">
                                    <img width="200" class="img-thumbnail"
                                        src="<?= base_url('assets/images/careers/'.$career['image']); ?>" alt="">
                                </div>
                                <div class="col-xs-9 col-md-10">
                                    <input type="file" name="image" class="form-control" id="image">
                                    <small class="text-info">* Max size 2 Mb</small><br>
                                         <small class="text-info">* Only for jpg | jpeg | png</small>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="keywords">Keywords (SEO)</label>
                            <textarea class="form-control" name="keywords" id="keywords" rows="2" cols="10" ><?= $career['keywords']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea id="editor1" name="description" rows="10" cols="80"
                                style="visibility: hidden; display: none;"><?= $career['description']; ?></textarea>
                            <?= form_error('description', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success btn-flat pull-right">Edit</button>
                        <a href="<?= site_url('admin/careers/'); ?>" class="btn pull-right">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>