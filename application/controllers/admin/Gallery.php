<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		security_access();
		$this->load->model('User_model');
		$this->load->model('Gallery_model');
		$this->load->model('Category_gallery_model');
	}

	// function show index gallery page
    public function index(){
		// get data from models
		$data = [
			'title' 	=> 'Gallery',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'galleries' => $this->Gallery_model->getAllGallery(),
		];
		// view index gallery page
        $this->templating->load('layouts/admin/wrapper', 'admin/gallery/index', $data);
	}
	
	// function create index gallery page
	public function create() {
		// get data from models
		$data = [
			'title' 	=> 'Add Gallery',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'categories'=> $this->Category_gallery_model->getAllCategoryGallery()
		];
		// validation rules
		$this->form_validation->set_rules('file', 'file', 'trim');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('description', 'Description', 'trim');
		$this->form_validation->set_rules('categId', 'category', 'trim|required');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/gallery/create', $data);
		} else {
			// if validation success call function create from gallery_model
			$this->Gallery_model->create($data);
		}
	}

	// function edit index gallery page
	public function edit($id) {
		// get data from models
		$data = [
			'id' 		=> $id,
			'title' 	=> 'Edit Gallery',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'gallery'	=> $this->Gallery_model->getGalleryById($id),
			'categories'=> $this->Category_gallery_model->getAllCategoryGallery(),
		];
		// validation rules
		$this->form_validation->set_rules('image', 'image', 'trim');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('description', 'Description', 'trim');
		$this->form_validation->set_rules('categId', 'category', 'trim|required');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/gallery/update', $data);
		} else {
			// if validation success call function edit from  gallery_model
			$this->Gallery_model->edit($data);
		}
	}

	// function delete index gallery page
	public function delete($id) {
		// get data from models
		$data = ['id' => $id, 'gallery' => $this->Gallery_model->getGalleryById($id)];
		// call function delete from gallery_models
		$this->Gallery_model->delete($data);
	}
	
}

/* End of file Gallery.php */

?>
