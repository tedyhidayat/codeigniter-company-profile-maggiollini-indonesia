<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery_model extends CI_Model {

	// function get all data gallery
	public function getAllGallery() {
		/**
		 * tbl_gallery join tbl_category_gallery and tbl_users
		 * tbl_category_galler: get name, tbl_users: get full name
		 */
		$this->db->select('tbl_gallery.*, tbl_category_gallery.name, tbl_users.full_name' );
		$this->db->from('tbl_gallery');
		$this->db->join('tbl_category_gallery','tbl_category_gallery.id = tbl_gallery.id_category', 'LEFT');
		$this->db->join('tbl_users','tbl_users.id = tbl_gallery.id_user', 'LEFT');
		$this->db->order_by('tbl_gallery.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// function get all data gallery active
	public function getAllGalleryActive() {
		/**
		 * tbl_gallery join tbl_category_gallery and tbl_users
		 * tbl_category_galler: get name, tbl_users: get full name
		 */
		$this->db->select('tbl_gallery.*, tbl_category_gallery.name, tbl_users.full_name' );
		$this->db->from('tbl_gallery');
		$this->db->join('tbl_category_gallery','tbl_category_gallery.id = tbl_gallery.id_category', 'LEFT');
		$this->db->join('tbl_users','tbl_users.id = tbl_gallery.id_user', 'LEFT');
		$this->db->where('tbl_gallery.status','Publish');
		$this->db->order_by('tbl_gallery.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	// function get data home slider limiter
	public function public_home_slider() {
		/**
		 * tbl_gallery join tbl_category_gallery and tbl_users
		 * tbl_category_galler: get name, tbl_users: get full name
		 */
		$this->db->select('tbl_gallery.*, tbl_category_gallery.name, tbl_category_gallery.name, tbl_users.full_name' );
		$this->db->from('tbl_gallery');
		$this->db->join('tbl_category_gallery','tbl_category_gallery.id = tbl_gallery.id_category', 'LEFT');
		$this->db->join('tbl_users','tbl_users.id = tbl_gallery.id_user', 'LEFT');
		$this->db->where(['tbl_gallery.status' => 'Publish', 'tbl_category_gallery.name' => 'home slider']);
		$this->db->limit(3);
		$this->db->order_by('id ','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// function get data gallery Team
	public function public_about_team() {
		/**
		 * tbl_gallery join tbl_category_gallery and tbl_users
		 * tbl_category_galler: get name, tbl_users: get full name
		 */
		$this->db->select('tbl_gallery.*, tbl_category_gallery.name, tbl_category_gallery.name, tbl_users.full_name' );
		$this->db->from('tbl_gallery');
		$this->db->join('tbl_category_gallery','tbl_category_gallery.id = tbl_gallery.id_category', 'LEFT');
		$this->db->join('tbl_users','tbl_users.id = tbl_gallery.id_user', 'LEFT');
		$this->db->where(['tbl_gallery.status' => 'Publish', 'tbl_category_gallery.name' => 'team']);
		$this->db->order_by('tbl_gallery.id ','ASC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// function get data gallery Team
	public function public_about_banner() {
		/**
		 * tbl_gallery join tbl_category_gallery and tbl_users
		 * tbl_category_galler: get name, tbl_users: get full name
		 */
		$this->db->select('tbl_gallery.*, tbl_category_gallery.name, tbl_category_gallery.name, tbl_users.full_name' );
		$this->db->from('tbl_gallery');
		$this->db->join('tbl_category_gallery','tbl_category_gallery.id = tbl_gallery.id_category', 'LEFT');
		$this->db->join('tbl_users','tbl_users.id = tbl_gallery.id_user', 'LEFT');
		$this->db->where(['tbl_gallery.status' => 'Publish', 'tbl_category_gallery.name' => 'banner about']);
		$this->db->order_by('tbl_gallery.id ','desc');
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	// function get gallery by ID
	public function getGalleryById($id) {
		return $this->db->get_where('tbl_gallery', ['id' => $id])->row_array();
	}

	// function create data gallery
	public function create($data) {	
		// get file from input form
		$upload_image = $_FILES['image']['name'];
		// check uploaded file
		if ($upload_image) {
			// define config upload file
			$config['max_size']     	= 2048;
			$config['allowed_types'] 	= 'jpg|jpeg|png';
			$config['upload_path'] 		= './assets/images/gallery/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-gallery-';
			// initializing config upload fileS
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('image')) {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . '</div>');
				redirect('admin/gallery/create/');
			} else {
				// if success
				// define creating thumbnail
				$config['thumb_marker']    	= '';
				$config['image_library'] 	= 'gd2';
				$config['create_thumb'] 	= TRUE;
				$config['maintain_ratio'] 	= TRUE;
				$config['width']         	= 500;
				$config['height']       	= 470;
				$config['source_image']	 	= './assets/images/gallery/'.$this->upload->data('file_name');
				$config['new_image']	 	= './assets/images/gallery/thumbs/'.$this->upload->data('file_name');
				// initializing config thumbnail
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				// collacting innsert data to table
				$data = [
					'id_user' 		=> $data['user']['id'],
					'created_at'	=> date('Y-m-d H:i:s'),
					'image' 		=> $this->upload->data('file_name'),
					'title' 		=> $this->input->post('title', true),
					'status' 		=> $this->input->post('status', true),
					'id_category'	=> $this->input->post('categId', true),
					'keywords' 		=> $this->input->post('keywords', true),
					'description' 	=>$this->input->post('description', true),
					'slug' 			=> url_title($this->input->post('title', true), 'dash', true),
				];
			}
		} else {
			// collacting innsert data to table
			$data = [
				'id_user' 		=> $data['user']['id'],
				'created_at'	=> date('Y-m-d H:i:s'),
				'title' 		=> $this->input->post('title', true),
				'status' 		=> $this->input->post('status', true),
				'id_category'	=> $this->input->post('categId', true),
				'keywords' 		=> $this->input->post('keywords', true),
				'description' 	=>$this->input->post('description', true),
				'slug' 			=> url_title($this->input->post('title', true), 'dash', true),
			];
		}
		// query insert
		$this->db->insert('tbl_gallery', $data);
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed insert data!</div>');
			redirect('admin/gallery/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Success!</div>');
			redirect('admin/gallery/');
		}
		
	}

	// function edit galery
	public function edit($data) {
		// get data from input form
		$title 			= $this->input->post('title', true);
		$status 		= $this->input->post('status', true);
		$id_category	= $this->input->post('categId', true);
		$keywords		= $this->input->post('keywords', true);
		$description 	= $this->input->post('description', true);
		$slug 			= url_title($this->input->post('title', true), 'dash', true);
		$upload_image 	= $_FILES['image']['name'];
		if ($upload_image) {
			// define uploaded file config
			$config['max_size']     	= 2048;
			$config['allowed_types'] 	= 'jpeg|jpg|png';
			$config['upload_path'] 		= './assets/images/gallery/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-gallery-';
			// initializing config file
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('image')) {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . ' You only can upload '. $config['allowed_types'] . ' type.' . '</div>');
				redirect('admin/gallery/edit/'. $data['id']);
			} else {
				// if success
				// get old file
				$old_image = $data['gallery']['image'];
				// unlink old file
				if ($old_image != 'default.png') {
					unlink(FCPATH . 'assets/images/gallery/' . $old_image);
					unlink(FCPATH . 'assets/images/gallery/thumbs/' . $old_image);
				}
				// define uploade file
				$new_file = $this->upload->data('file_name');
				$config['image_library'] 	= 'gd2';
				$config['create_thumb'] 	= TRUE;
				$config['maintain_ratio'] 	= TRUE;
				$config['width']         	= 500;
				$config['height']       	= 470;
				$config['thumb_marker']    	= '';
				$config['source_image']	 	= './assets/images/gallery/'.$new_file;
				$config['new_image']	 	= './assets/images/gallery/thumbs/'.$new_file;
				// initializing file upload
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				// set data file upload to table
				$this->db->set('image', $new_file);
			}
		}
		// query update
		$this->db->set('slug', $slug);
		$this->db->set('title', $title);
		$this->db->set('status', $status);
		$this->db->set('keywords', $keywords);
		$this->db->set('id_category', $id_category);
		$this->db->set('description', $description);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $data['id']);
		$this->db->update('tbl_gallery');
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $title . ' failed to edit!</div>');
			redirect('admin/gallery/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your file ' . $title . ' has been updated!</div>');
			redirect('admin/gallery/');
		}
		
	}

	// function delete gallery
	public function delete($data) {
		// get old file
		$old_image= $data['gallery']['image'];
		// unlink old file
		if ($old_image) {
			unlink(FCPATH . 'assets/images/gallery/' . $old_image);
			unlink(FCPATH . 'assets/images/gallery/thumbs/' . $old_image);
		}
		// query delete
		$this->db->where('id', $data['id']);
		$this->db->delete('tbl_gallery');
		// check if data affected rows'
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Image : ' . $data['gallery']['title'] .  '  failed to delete!</div>');
			redirect('admin/gallery/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Image : ' . $data['gallery']['title'] .  '  has been delete!</div>');
			redirect('admin/gallery/');
		}
		
	}
}

/* End of file Gallery_model.php */
/* Location: ./application/models/Gallery_model.php */