<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Videos extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		security_access();
		$this->load->model('User_model');
		$this->load->model('Videos_model');
		$this->load->model('Category_videos_model');
	}

	// function show index video page
    public function index() {
		// get data from models
		$data = [
			'title' => 'Videos',
			'user' 	=> $this->User_model->getUserBySession(),
			'videos' => $this->Videos_model->getAllVideos(),
			'config'=> $this->configs->getConfiguration(),
		];
		// view index video page
        $this->templating->load('layouts/admin/wrapper', 'admin/videos/index', $data);
	}
	
	// function create video page
	public function create() {
		// get data from models
		$data = [
			'title' 		=> 'Add Videos',
			'config' 		=> $this->configs->getConfiguration(),
			'user' 			=> $this->User_model->getUserBySession(),
			'categories' 	=> $this->Category_videos_model->getAllCategoryVideos(),
		];
		// validation rules
		$this->form_validation->set_rules('video', 'video', 'trim');
		$this->form_validation->set_rules('status', 'status', 'required');
		$this->form_validation->set_rules('keywords', 'keywords', 'trim');
		$this->form_validation->set_rules('title', 'title', 'trim|required');
		$this->form_validation->set_rules('categId', 'category', 'trim|required');
		$this->form_validation->set_rules('description', 'description', 'required|trim');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/videos/create', $data);
		} else {
			// if validation success call function create from Videos_model
			$this->Videos_model->create($data);
		}
	}

	// function edit video page
	public function edit($id) {
		// get data from models
		$data = [
			'title' 	=> 'Edit Videos',
			'id' 		=> $id,
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'video' 		=> $this->Videos_model->getVideosById($id),
			'categories'=> $this->Category_videos_model->getAllCategoryVideos(),
		];
		// validation rules
		$this->form_validation->set_rules('video', 'video', 'trim');
		$this->form_validation->set_rules('keywords', 'keywords', 'trim');
		$this->form_validation->set_rules('status', 'status', 'required');
		$this->form_validation->set_rules('title', 'title', 'trim|required');
		$this->form_validation->set_rules('categId', 'category', 'trim|required');
		$this->form_validation->set_rules('description', 'description', 'required|trim');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/videos/update', $data);
		} else {
			// if validation success call function edit from Videos_model
			$this->Videos_model->edit($data);
		}
	}

	// function delete video page
	public function delete($id) {
		// get data from models
		$data = ['id' => $id, 'video' => $this->Videos_model->getVideosById($id)];
		// call functionn delete from Videos_model
		$this->Videos_model->delete($data);
	}
	
}

/* End of video Videos.php */

?>