<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Videos_model extends CI_Model {

	// function all data Videos
	public function getAllVideos() {
		/**
		 * get data tbl_video: all
		 * tbl_videos join to tbl_users and tbl_category_Videos
		 */
		$this->db->select('tbl_videos.*, tbl_category_videos.name, tbl_users.full_name' );
		$this->db->from('tbl_videos');
		$this->db->join('tbl_category_videos','tbl_category_videos.id = tbl_videos.id_category', 'LEFT');
		$this->db->join('tbl_users','tbl_users.id = tbl_videos.id_user', 'LEFT');
		$this->db->order_by('tbl_videos.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// function all data Videos
	public function public_home_videos() {
		$this->db->select('tbl_videos.*');
		$this->db->from('tbl_videos');
		$this->db->where('status', 'Publish');
		$this->db->limit(1);
		$this->db->order_by('id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	// function all data Videos active latest
	public function public_videos_active() {
		$this->db->select('tbl_videos.*,tbl_category_videos.name,');
		$this->db->from('tbl_videos');
		$this->db->join('tbl_category_videos','tbl_category_videos.id = tbl_videos.id_category', 'LEFT');
		$this->db->where('status', 'Publish');
		$this->db->order_by('id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	// function get video by ID
	public function getVideosById($id) {
		return $this->db->get_where('tbl_videos', ['id' => $id])->row_array();
	}

	// function create data video
	public function create($data) {
		// get data from input video 
		$upload_video = $_FILES['video']['name'];
		if ($upload_video) {
			// define video uploaded
			$config['max_size']     	= 102400; // 100 mb in kb
			$config['allowed_types'] 	= 'mp4|webm';
			$config['upload_path'] 		= './assets/videos/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-video-';
			// initializing video uploaded
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('video')) {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . '</div>');
				redirect('admin/Videos/create');
			} else {
				// if success
				// collacting data for insert table 
				$data = [
					'id_user' 		=> $data['user']['id'],
					'created_at'	=> date('Y-m-d H:i:s'),
					'video_name' 	=> $this->upload->data('file_name'),
					'title' 		=> $this->input->post('title', true),
					'status' 		=> $this->input->post('status', true),
					'id_category'	=> $this->input->post('categId', true),
					'keywords' 		=> $this->input->post('keywords', true),
					'description' 	=> $this->input->post('description', true),
					'slug' 			=> url_title($this->input->post('title', true), 'dash', true),
				];
			}
		} else {
			// collactoing data to insert
			$data = [
				'id_user' 		=> $data['user']['id'],
				'created_at'	=> date('Y-m-d H:i:s'),
				'title' 		=> $this->input->post('title', true),
				'status' 		=> $this->input->post('status', true),
				'id_category'	=> $this->input->post('categId', true),
				'keywords' 		=> $this->input->post('keywords', true),
				'description' 	=> $this->input->post('description', true),
				'slug' 			=> url_title($this->input->post('title', true), 'dash', true),
			];
		}
		// query insert
		$this->db->insert('tbl_videos', $data);
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to insert data!</div>');
			redirect('admin/videos/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Success!</div>');
			redirect('admin/videos/');
		}
		
	}

	// function edit Videos
	public function edit($data) {
		// get data from input form
		$title 			= $this->input->post('title', true);
		$status 		= $this->input->post('status', true);
		$id_category	= $this->input->post('categId', true);
		$keywords 		= $this->input->post('keywords', true);
		$description 	= $this->input->post('description', true);
		$slug 			= url_title($this->input->post('title', true), 'dash', true);
		$upload_video 	= $_FILES['video']['name'];
		if ($upload_video) {
			// define input video uploaded
			$config['max_size']     	= 102400; // 100 mb in kb
			$config['allowed_types'] 	= 'mp4|webm';
			$config['upload_path'] 		= './assets/videos/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-video-';
			// initializing configuration video
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('video')) {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . ' You only can upload '. $config['allowed_types'] . ' type.' . '</div>');
				redirect('admin/videos/edit/'. $data['id']);
			} else {
				// if suceess
				// unlik old video
				$old_video = $data['video']['video_name'];
				if ($old_video != 'default.png') {unlink(FCPATH . 'assets/videos/' . $old_video);}
				// set video to table
				$this->db->set('video_name', $this->upload->data('file_name'));
			}
		}
		// query update
		$this->db->set('slug', $slug);
		$this->db->set('title', $title);
		$this->db->set('status', $status);
		$this->db->set('keywords', $keywords);
		$this->db->set('id_category', $id_category);
		$this->db->set('description', $description);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $data['id']);
		$this->db->update('tbl_videos');
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your video ' . $title . ' failed to edit!</div>');
			redirect('admin/videos/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your video ' . $title . ' has been updated!</div>');
			redirect('admin/videos/');
		}
		
	}

	// function delete data Videos
	public function delete($data) {
		// get old video
		$old_video= $data['video']['video_name'];
		// unlink video
		if ($old_video) {
			unlink(FCPATH . 'assets/videos/' . $old_video);
		}
		// query delete
		$this->db->where('id', $data['id']);
		$this->db->delete('tbl_videos');
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to delete data!</div>');
			redirect('admin/videos/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your video has been delete!</div>');
			redirect('admin/videos/');
		}
	}
}

/* End of video Videos_model.php */
/* Location: ./application/models/Videos_model.php */