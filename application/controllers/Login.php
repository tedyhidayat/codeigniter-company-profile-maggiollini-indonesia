<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	// function login regirect
	public function login_redirect() {
        $this->session->set_userdata('admin_access',TRUE);
        redirect('auth/');
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/Index.php */