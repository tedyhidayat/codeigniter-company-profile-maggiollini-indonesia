<?php if($configs) : ?>
<!-- mini contact -->
<section id="mini-contact" class="mini-contact d-none d-sm-block">
    <div class="container ">
        <div class="row">
            <div class="col col-sm-7 p-0 phonemail">
                <ul class="nav p-0">
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <i class="fas fa-fw fa-envelope icon"></i> <?= $configs['email']; ?>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled text-muted" aria-label="disabled" href="#">
                            |
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <i class="fas fa-fw fa-phone icon"></i> <?= $configs['telephone']; ?>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col col-sm-5 p-0">
                <ul class="nav justify-content-end">
                    <?php if($configs['twitter']): ?>
                    <li class="nav-item">
                        <a target="_blank" class="nav-link" href="<?= $configs['twitter']; ?>"><i class="fab fa-twitter icon"></i></a>
                    </li>
                    <?php endif; ?>
                    <?php if($configs['facebook']): ?>
                    <li class="nav-item">
                        <a target="_blank" class="nav-link" href="<?= $configs['facebook']; ?>"><i class="fab fa-facebook-square icon"></i></a>
                    </li>
                    <?php endif; ?>
                    <?php if($configs['instagram']): ?>
                    <li class="nav-item">
                        <a target="_blank" class="nav-link" href="<?= $configs['instagram']; ?>"><i class="fab fa-instagram icon"></i></a>
                    </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>