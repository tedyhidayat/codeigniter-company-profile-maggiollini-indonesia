<?php if($configs) : ?>   
    <!-- Breadcrumbs -->
    <section id="breadcrumbs">
        <div class="container">
            <div class=" row justify-content-between align-items-center">
                <div class="col-12 col-md col-lg">
                    <h2 class="text-uppercase"><?= $this->uri->segment(1); ?></h2>
                </div>
                <div class="col-12 col-md col-lg">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= site_url('/'); ?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?= $this->uri->segment(1); ?></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- Gsalleries -->
    <section id="galleries" class="">
        <?php if($gallery || $videos) : ?>   
        <div class="container-fluid">
            <div class="row justify-content-center">
                <header class="d-none d-sm-block">
                <?php foreach($config_page as $row_page) : ?>
                <?php if($row_page['page_position'] == 'Gallery'): ?>
                    <h3 class="text-capitalize"><?= $row_page['title']; ?></h3>
                    <p><?= $row_page['description']; ?></p>
                <?php endif; ?>
                <?php endforeach; ?>
                </header>
                <div class="col-12" data-aos="fade-up">
                    <!-- category gallery -->
                    <ul class="nav mb-3 justify-content-center" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active d-flex align-items-center flex-column bd-highlight mb-3" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">
                            <i class="fas fa-home icon-category"></i> Workshop Kami</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex align-items-center flex-column bd-highlight mb-3" id="pills-tools-tab" data-toggle="pill" href="#pills-tools" role="tab" aria-controls="pills-tools" aria-selected="false"><i class="fas fa-toolbox icon-category"></i> Peralatan Kerja</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex align-items-center flex-column bd-highlight mb-3" id="pills-team-tab" data-toggle="pill" href="#pills-team" role="tab" aria-controls="pills-team" aria-selected="false"><i class="fas fa-users icon-category"></i> Tim Kami</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex align-items-center flex-column bd-highlight mb-3" id="pills-videos-tab" data-toggle="pill" href="#pills-videos" role="tab" aria-controls="pills-videos" aria-selected="false"><i class="fas fa-film icon-category"></i> Videos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex align-items-center flex-column bd-highlight mb-3" id="pills-others-tab" data-toggle="pill" href="#pills-others" role="tab" aria-controls="pills-others" aria-selected="false"><i class="fas fa-plus icon-category"></i> Lainnya</a>
                        </li>
                    </ul>
                    <!-- items gallery -->
                    <div class="tab-content" id="pills-tabContent">
                        <!-- workshop -->
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="row justify-content-center">
                                <!-- item gallery -->
                                <?php foreach($gallery as $row_gallery) : ?>
                                <?php if($row_gallery['name'] == 'workshop') : ?>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-4 box-gallery">
                                    <div class="card img-item web">
                                        <div class="card-body p-0">
                                            <img src="<?= base_url('assets/images/gallery/'.$row_gallery['image']); ?>" class="card-img-top img-gallery" alt="<?= $row_gallery['title']; ?>" title="<?= $row_gallery['title']; ?>">
                                            <div class="box-control">
                                                <div class="box-text">
                                                    <h5><?= $row_gallery['title']; ?></h5>
                                                    <span><?= $row_gallery['name']; ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endif;?>
                                <?php endforeach;?>
                            </div>
                        </div>

                        <!-- tools -->
                        <div class="tab-pane fade show" id="pills-tools" role="tabpanel" aria-labelledby="pills-tools-tab">
                            <div class="row justify-content-center">
                                <!-- item gallery -->
                                <?php foreach($gallery as $row_gallery) : ?>
                                <?php if($row_gallery['name'] == 'tools') : ?>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-4 box-gallery">
                                    <div class="card img-item web">
                                        <div class="card-body p-0">
                                            <img src="<?= base_url('assets/images/gallery/'.$row_gallery['image']); ?>" class="card-img-top img-gallery" alt="<?= $row_gallery['title']; ?>" title="<?= $row_gallery['title']; ?>">
                                            <div class="box-control">
                                                <div class="box-text">
                                                    <h5><?= $row_gallery['title']; ?></h5>
                                                    <span><?= $row_gallery['name']; ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endif;?>
                                <?php endforeach;?>
                            </div>
                        </div>

                        <!-- team -->
                        <div class="tab-pane fade show" id="pills-team" role="tabpanel" aria-labelledby="pills-team-tab">
                            <div class="row justify-content-center">
                                <!-- item gallery -->
                                <?php foreach($gallery as $row_gallery) : ?>
                                <?php if($row_gallery['name'] == 'team') : ?>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-4 box-gallery">
                                    <div class="card img-item web">
                                        <div class="card-body p-0">
                                            <img src="<?= base_url('assets/images/gallery/'.$row_gallery['image']); ?>" class="card-img-top img-gallery" alt="<?= $row_gallery['title']; ?>" title="<?= $row_gallery['title']; ?>">
                                            <div class="box-control">
                                                <div class="box-text">
                                                    <h5><?= $row_gallery['title']; ?></h5>
                                                    <span><?= $row_gallery['name']; ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endif;?>
                                <?php endforeach;?>
                            </div>
                        </div>
                        
                        <!-- team -->
                        <div class="tab-pane fade show" id="pills-videos" role="tabpanel" aria-labelledby="pills-videos-tab">
                            <div class="row justify-content-center">
                                <!-- item gallery -->
                                <?php foreach($videos as $row_video) : ?>
                                <div class="col-12 col-sm-6 col-md-4 col-lg-4 box-gallery">
                                    <div class="card img-item web">
                                        <div class="card-body p-0">
                                            <video class="video" controls width="100%" alt="<?= $row_video['title']; ?>" title="<?= $row_video['title']; ?>">
                                                <source src="<?= base_url('assets/videos/' . $row_video['video_name']); ?>" type="video/mp4">
                                            </video>
                                        </div>
                                        <div class="card-footer p-1 bg-white text-center">
                                            <h6><?= $row_video['title']; ?></h6>
                                            <p style="font-size: 14px;">
                                                <small>
                                                    <?= $row_video['name']; ?> - <?= date("d F Y", strtotime($row_video['created_at'])); ?>
                                                </small>
                                                <br />
                                                <small alt="keywords">
                                                    Keywords: <b><?= $row_video['keywords']; ?></b> 
                                                    &nbsp;
                                                </small>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach;?>
                            </div>
                        </div>

                        <!-- other -->
                        <div class="tab-pane fade show" id="pills-others" role="tabpanel" aria-labelledby="pills-others-tab">
                            <div class="row justify-content-center">
                                <!-- item gallery -->
                                <?php foreach($gallery as $row_gallery) : ?>
                                <?php if($row_gallery['name'] == 'others') : ?>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-4 box-gallery">
                                    <div class="card img-item web">
                                        <div class="card-body p-0">
                                            <img src="<?= base_url('assets/images/gallery/'.$row_gallery['image']); ?>" class="card-img-top img-gallery" alt="<?= $row_gallery['title']; ?>" title="<?= $row_gallery['title']; ?>">
                                            <div class="box-control">
                                                <div class="box-text">
                                                    <h5><?= $row_gallery['title']; ?></h5>
                                                    <span><?= $row_gallery['name']; ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endif;?>
                                <?php endforeach;?>
                            </div>
                        </div>

                        <!--  -->
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php else : ?>
        <div class="container">
            <div class="row">
                <div class="col-12 mb-5">
                    <div class="card text-center">
                        <div class="card-body">
                            <h4>Gallery Kosong.</h4>
                            <p><?= $configs['siteUrl']; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </section>
<?php else : ?>
    <section>
        <div class="container" style="min-height: 300px; ">
            <div class="row align-items-center">
                <div class="col-12 text-center">
                    <h2 class="mt-5">EMPTY!</h2>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>