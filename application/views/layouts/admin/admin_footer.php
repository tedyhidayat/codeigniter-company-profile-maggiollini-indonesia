<!-- modal logout -->
<div class="modal fade" id="modal-warning">
	<div class="modal-dialog">
		<div class="modal-content text-center">
			<div class="modal-header">
				<h4 class="modal-title"><b> Are you sure to logout ?</b></h4>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-flat" data-dismiss="modal">Cancel</button>
				<a href="<?= site_url('auth/logout/'); ?>" class="btn btn-danger btn-flat">Logout</a>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- footer -->
<footer class="main-footer">
    <?php if($config) : ?>
        <div class="pull-right hidden-xs">
            <b><?= $config['tagline']; ?></b>
        </div>
        <strong>Copyright &copy; <?= date('Y') ?> <a target="_blank" href="<?= $config['siteUrl']; ?>"><?= $config['siteName']; ?></a>
    <?php else : ?>
        <div class="pull-right hidden-xs">
            <b>THDEV</b>
        </div>
        <strong>Copyright &copy; <?= date('Y') ?> <a target="_blank" href="#">thdev.com</a>
    <?php endif; ?>
</footer>

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?= base_url('assets/backend/'); ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url('assets/backend/'); ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DATA TABLES -->
<script src="<?= base_url('assets/backend/'); ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/backend/'); ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js">
</script>
<!-- FastClick -->
<script src="<?= base_url('assets/backend/'); ?>bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('assets/backend/'); ?>dist/js/adminlte.min.js"></script>
<!-- CK Editor -->
<script src="<?= base_url('assets/backend/'); ?>bower_components/ckeditor/ckeditor.js"></script>
<!-- Sparkline -->
<script src="<?= base_url('assets/backend/'); ?>bower_components/jquery-sparkline/dist/jquery.sparkline.min.js">
</script>
<!-- jvectormap  -->
<script src="<?= base_url('assets/backend/'); ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?= base_url('assets/backend/'); ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll -->
<script src="<?= base_url('assets/backend/'); ?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script>
$(function() {
    $('#example1').DataTable();
    $('#example2').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        'autoWidth': true,
    });
});
</script>
<script>
$(function() {
    CKEDITOR.replace('editor1');
    $('.textarea').wysihtml5();
})
</script>
</body>
</html>