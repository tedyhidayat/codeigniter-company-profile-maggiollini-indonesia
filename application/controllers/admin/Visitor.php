<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Visitor extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
        security_access();
        // $this->load->library('user_agent');
    }
    
    // function show visitor
    public function index() {
        // get data 
        // $this->load->library('user_agent');

        $data = [
            'title'             => 'Visitor',
            'browser'           => $this->agent->browser(),
            'browser_version'   => $this->agent->version(),
            'os'                => $this->agent->platform(),
            'ip'                => $this->input->ip_address(),
            'config'            => $this->configs->getConfiguration(),
			'user' 	            => $this->User_model->getUserBySession(),
        ];
        // view visitor
		$this->templating->load('layouts/admin/wrapper', 'admin/visitor', $data);
    }

	
}

/* End of file Gallery.php */

?>
