<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		create_cookie();
        $this->load->model('Posts_model');
        $this->load->model('Sample_model');
        $this->load->model('Careers_model');
        $this->load->model('Projects_model');
        $this->load->model('Services_model');
        $this->load->model('Categories_model');
        
	}

	// function show login page
	public function index()	{
		// unset sessinon captcha if there
		$this->session->unset_userdata('admin_access');
		$this->session->unset_userdata('captchaImage');

		$data['links'] = [
			'posts' 		=> $this->Posts_model->sitemap(),
			'sample' 		=> $this->Sample_model->sitemap(),
			'careers' 		=> $this->Careers_model->sitemap(),
			'projects' 		=> $this->Projects_model->sitemap(),
			'services' 		=> $this->Services_model->sitemap(),
			'category_news' => $this->Categories_model->sitemap(),
        ];
        
        $this->load->view('sitemap', $data);
    }
    
    public function list() {
        // identification
		$configurations = $this->configs->getConfiguration();

		// check config for title
		(!empty($configurations['siteName'])) ? $title1 = $this->uri->segment(1) : $title1 = "THDEV";
		(!empty($configurations['siteName'])) ? $title2 = $configurations['siteName'] : $title2 = "Web Developer";


		$data = [
			'title' 		=> $title1.' | '.$title2,
			'posts' 		=> $this->Posts_model->sitemap(),
			'sample' 		=> $this->Sample_model->sitemap(),
			'careers' 		=> $this->Careers_model->sitemap(),
			'projects' 		=> $this->Projects_model->sitemap(),
			'services' 		=> $this->Services_model->sitemap(),
			'category_news' => $this->Categories_model->sitemap(),
			'configs' 		=> $this->configs->getConfiguration(),
        ];
        
        $this->templating->load('layouts/public/wrapper', 'sitemap_list', $data);
    }
}

/* End of file Sitemap.php */
/* Location: ./application/controllers/Index.php */