    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $title; ?>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <?= $this->session->flashdata('message'); ?>

        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border disabled">
                        <h4><i class="fa fa-globe"></i> General</h4>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" action="<?= site_url('admin/configurations/create'); ?>"
                        enctype="multipart/form-data" role="form">
                        <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" class="form-control" value="<?= $this->security->get_csrf_hash(); ?>" >
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <label for="siteName">Site Name <span class="text-red">*</span></label>
                                        <input type="text" name="siteName" class="form-control" id="siteName"
                                            placeholder="Site Name" value="<?= set_value('siteName'); ?>">
                                        <?= form_error('siteName', '<small class="text-danger">', '</small>'); ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <div class="form-group">
                                        <label for="tagline">Tagline <span class="text-red">*</span></label>
                                        <input type="text" name="tagline" class="form-control" id="tagline"
                                            placeholder="Tagline" value="<?= set_value('tagline'); ?>">
                                        <?= form_error('tagline', '<small class="text-danger">', '</small>'); ?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="keywords">Keywords <span class="text-red">*</span></label>
                                <input type="text" name="keywords" class="form-control" id="keywords"
                                    placeholder="keywords" value="<?= set_value('keywords'); ?>">
                                <?= form_error('keywords', '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="siteUrl">Site URL <span class="text-red">*</span></label>
                                <input type="text" name="siteUrl" class="form-control" id="siteUrl"
                                    placeholder="https://www.example.com" value="<?= set_value('siteUrl'); ?>">
                                <?= form_error('siteUrl', '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="metaText">Meta Text <span class="text-red">*</span></label>
                                <textarea class="form-control" id="metaText" name="metaText" rows="5"
                                    cols="30"><?= set_value('metaText'); ?></textarea>
                                <?= form_error('metaText', '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="metaDesc">Meta Description <span class="text-red">*</span></label>
                                <textarea class="form-control" id="metaDesc" name="metaDesc" rows="5"
                                    cols="30"><?= set_value('metaDesc'); ?></textarea>
                                <?= form_error('metaDesc', '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="">
                                <div class="row">
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                            <label for="email">Email <span class="text-red">*</span></label>
                                            <input type="text" name="email" class="form-control" id="email"
                                                placeholder="email" value="<?= set_value('email'); ?>">
                                            <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                            <label for="telephone">Telephone Number <span class="text-red">*</span></label>
                                            <input type="text" name="telephone" class="form-control" id="telephone"
                                                placeholder="telephone" value="<?= set_value('telephone'); ?>">
                                            <?= form_error('telephone', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                            <label for="fax">Fax Number <span class="text-red">*</span></label>
                                            <input type="text" name="fax" class="form-control" id="fax"
                                                placeholder="fax" value="<?= set_value('fax'); ?>">
                                            <?= form_error('fax', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                            <label for="wa">WhatsApp <span class="text-red">*</span></label>
                                            <input type="text" name="wa" class="form-control" id="wa"
                                                placeholder="wa" value="<?= set_value('wa'); ?>">
                                            <?= form_error('wa', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <div class="row">
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                            <label for="ig">Instagram <span class="text-red">*</span></label>
                                            <input type="text" name="ig" class="form-control" id="ig" placeholder="ig"
                                                value="<?= set_value('ig'); ?>">
                                            <?= form_error('ig', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                            <label for="fb">Facebook <span class="text-red">*</span></label>
                                            <input type="text" name="fb" class="form-control" id="fb" placeholder="fb"
                                                value="<?= set_value('fb'); ?>">
                                            <?= form_error('fb', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                            <label for="tw">Twitter <span class="text-red">*</span></label>
                                            <input type="text" name="tw" class="form-control" id="tw" placeholder="tw"
                                                value="<?= set_value('tw'); ?>">
                                            <?= form_error('tw', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="address">Address <span class="text-red">*</span></label>
                                <textarea class="form-control" id="address" name="address" rows="5"
                                    cols="30"><?= set_value('address'); ?></textarea>
                                <?= form_error('address', '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="gmaps">Google Maps <span class="text-red">*</span></label>
                                <textarea class="form-control" id="gmaps" name="gmaps" rows="5"
                                    cols="30"><?= set_value('gmaps'); ?></textarea>
                                <?= form_error('gmaps', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success btn-flat pull-right">Save Configurations</button>
                            <a href="<?= site_url('admin/configurations/'); ?>" class="btn pull-right">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
