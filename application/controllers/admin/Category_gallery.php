<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category_gallery extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		security_access();
		$this->load->model('User_model');
		$this->load->model('Category_gallery_model');
	}

	// function show index category gallery
    public function index() {
		// get data from models
		$data = [
			'title' 			=> 'Category Gallery',
			'config' 			=> $this->configs->getConfiguration(),
			'user' 				=> $this->User_model->getUserBySession(),
			'category_gallery' 	=> $this->Category_gallery_model->getAllCategoryGallery(),
		];
		// view index category gallery 
        $this->templating->load('layouts/admin/wrapper', 'admin/category_gallery/index', $data);
	}
	
	// function create category galery
	public function create() {
		// get data from model
		$data = [
			'title' 	=> 'Add Category Gallery',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
		];
		// validation rules
		$this->form_validation->set_rules('categName', 'category', 'trim|required|is_unique[tbl_category_gallery.name]', [
			'is_unique' => 'Category ' . $this->input->post('categName') . ' already exist!'
		]);
		$this->form_validation->set_rules('position', 'position', 'required|numeric|is_unique[tbl_category_gallery.position]', [
			'is_unique' => 'Position already exist!'
		]);
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/category_gallery/create', $data);
		} else {
			// if validation success call model create from category_gallery_model
			$this->Category_gallery_model->create($data);
		}
	}

	// function edit category galery
	public function edit($id) {
		// get data from model
		$data = [
			'id' 		=> $id,
			'title' 	=> 'Edit Category Gallery',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'category' 	=> $this->Category_gallery_model->getCategoryGalleryById($id),
		];
		// validation rules
		$this->form_validation->set_rules('categName', 'category name', 'trim|required');
		$this->form_validation->set_rules('position', 'position', 'required|trim|numeric');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/category_gallery/update', $data);
		} else {
			// if validation success call model edit from category_gallery_model
			$this->Category_gallery_model->edit($data);
		}
	}

	// function delte category galery
	public function delete($id) {
		// get data from models
		$data = ['id' => $id, 'category' => $this->Category_gallery_model->getCategoryGalleryById($id)];
		// call function delete from model category_gallery_model
		$this->Category_gallery_model->delete($data);
	}
	
}

/* End of file Category_gallery.php */

?>