<?php if($configs) : ?>      
   <?php 
        $services_footer = $this->Services_model->public_services_footer();
    ?>
    <!-- footer -->
    <footer>
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-12 col-md-4 nav-identity">
                    <h3 class="site-name"><?= $configs['siteName']; ?></h3>
                    <ul class="list-unstyled">
                        <li class="pr-5"><?= $configs['metaDesc']; ?></li>
                    </ul>
                    <ul class="list-unstyled">
                        <li class="pr-5"></li>
                    </ul>
                    <ul class="list-unstyled">
                        <li><b class="text-light">Alamat: </b><br /> <?= $configs['address']; ?><br /><br /></li>
                        <li><b class="text-light">Nomor Telpon: </b><br /> <?= $configs['telephone']; ?><br /></li>
                        <li><b class="text-light">Email: </b><br /> <?= $configs['email']; ?></li>
                    </ul>
                </div>
                <div class="col-6 col-md-2 nav-footer mt-3 mt-md-0">
                    <h3 class="navigation">Navigasi</h3>
                    <ul class="list-unstyled">
                        <li><a href="<?= site_url('/'); ?>" class="link">Home</a></li>
                        <li><a href="<?= site_url('/about/'); ?>" class="link">About</a></li>
                        <li><a href="<?= site_url('/services/'); ?>" class="link">Services</a></li>
                        <li><a href="<?= site_url('/galleries/'); ?>" class="link">Galleries</a></li>
                        <li><a href="<?= site_url('/news/'); ?>" class="link">News</a></li>
                        <li><a href="<?= site_url('/projects/finished-projects/'); ?>" class="link">Finished Projects</a></li>
                        <li><a href="<?= site_url('/projects/sample/'); ?>" class="link">Sample</a></li>
                        <li><a href="<?= site_url('/careers/'); ?>" class="link">Careers</a></li>
                        <li><a href="<?= site_url('/contact/'); ?>" class="link">Contact</a></li>
                        <li><a href="<?= site_url('/sitemap/list/'); ?>" class="link">Sitemap</a></li>
                    </ul>
                </div>
                <div class="col-6 col-md-2 nav-footer mt-3 mt-md-0">
                    <h3 class="navigation">Servis Kami</h3>
                    <ul class="list-unstyled">
                        <?php foreach($services_footer as $row_services_footer) : ?>
                        <li><a href="<?= site_url('/services/detail/'.$row_services_footer['slug']) ?>" class="link"> <?= $row_services_footer['title']; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-12 col-md-4">
                    <div class="maps">
                        <?= $configs['gmaps']; ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- copyright -->
        <div id="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <span>&copy; Copyright <b class="text-light"><?= $configs['siteName']; ?></b> | All Rights Reserved</span><br />
                        <span><b>2020.</b> <a href="<?= site_url('/'); ?>"><?= $configs['siteUrl']; ?></a></span>
                    </div>
                    <div class="col-12 col-md-6">
                        <ul class="nav justify-content-end">
                            <?php if($configs['twitter']): ?>
                            <li class="nav-item">
                                <a target="_blank" class="nav-link" href="<?= $configs['twitter']; ?>"><i class="fab fa-twitter icon"></i></a>
                            </li>
                            <?php endif; ?>
                            <?php if($configs['facebook']): ?>
                            <li class="nav-item">
                                <a target="_blank" class="nav-link" href="<?= $configs['facebook']; ?>"><i class="fab fa-facebook-square icon"></i></a>
                            </li>
                            <?php endif; ?>
                            <?php if($configs['instagram']): ?>
                            <li class="nav-item">
                                <a target="_blank" class="nav-link" href="<?= $configs['instagram']; ?>"><i class="fab fa-instagram icon"></i></a>
                            </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- button back to top -->
    <button type="button" class="back-to-top"><i class="fas fa-arrow-up backtop-icon"></i></button>

    <?php if($configs['whatsapp']): ?>
    <!-- button chat -->
    <a href="https://wa.me/<?= $configs['whatsapp']; ?>?text=Hallo%2C%20saya%20%0ANama%20%3A%0AEmail%20%3A%0ADari%20%3A%0A%0AIngin%20bekerjasama%20dengan%20perusahaan%20anda." class="btn chat-with-us-desktop d-none d-sm-block d-md-block d-lg-block"><i class="fab fa-fw fa-whatsapp backtop-icon"></i> Hubungi Kami via WhatsApp </a>

    <!-- button chat with us fixed bottom, visible only phone size -->
    <a class="clearfix fixed-bottom btn d-block d-md-none d-lg-none d-xl-none chat-with-us" href="https://wa.me/<?= $configs['whatsapp']; ?>?text=Hallo%2C%20saya%20%0ANama%20%3A%0AEmail%20%3A%0ADari%20%3A%0A%0AIngin%20bekerjasama%20dengan%20perusahaan%20anda.">
        <div class="pl-2 float-left">Hubungi Kami via WhatsApp</div>
        <div class="pr-2 float-right ico-wa"><i class="fab fa-whatsapp icon"></i></div>
    </a>
    <?php endif; ?>
<?php endif; ?>
  
    <!-- scripts -->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.0.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <!-- <script src="<?= base_url('assets/frontend/'); ?>libraries/jquery/jquery-3.4.1.min.js"></script> -->
    <!-- <script src="<?= base_url('assets/frontend/'); ?>libraries/bootstrap/js/bootstrap.js"></script> -->
    <script src="<?= base_url('assets/frontend/'); ?>libraries/jquery_easing/jquery.easing.min.js"></script>
    <script src="<?= base_url('assets/frontend/'); ?>libraries/fontawesome/js/all.min.js"></script>
    <script src="<?= base_url('assets/frontend/'); ?>libraries/owl.carousel/owl.carousel.min.js"></script>
    <script src="<?= base_url('assets/frontend/'); ?>libraries/venobox/venobox.min.js"></script>
    <script src="<?= base_url('assets/frontend/'); ?>libraries/aos/aos.js"></script>
    <script src="<?= base_url('assets/frontend/'); ?>libraries/xzoom/xzoom.min.js"></script>
    <script src="<?= base_url('assets/frontend/'); ?>libraries/filterizr/jquery.filterizr.min.js"></script>
    <script src="<?= base_url('assets/frontend/'); ?>scripts/main.js"></script>
    <script>
        $(document).ready(function() {
            $('.refresh').on('click', function() {
                $.get('<?php echo site_url().'contact/recaptcha/'; ?>', function(data) {
                    $('.image_captcha').html(data);
                });
            });
        });
    </script>

  </body>
</html>