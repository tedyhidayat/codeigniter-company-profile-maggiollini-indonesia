<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		$this->load->model('Auth_model');
		$this->load->helper('captcha');
		$this->load->helper('string');
	}

	// function show login page
	public function index()	{
	
		// check session admin login redirect
		if($this->session->userdata('admin_access')) {
			// check user session, if session available redirect to admin page
			if ($this->session->userdata('id') && $this->session->userdata('username')) {
				redirect('admin/dashboard/','refresh');
			}

			if($this->session->userdata('captchaImage')) {
				$captchaImage = $this->session->userdata('captchaImage');
				unlink(FCPATH . 'assets/images/captcha/' . $captchaImage);
				$this->session->unset_userdata('captchaImage');
			}
			
			// validatiion rules
			$this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[25]',[
				'max_length' => 'Too much character'	
			]);
			$this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[25]',[
				'max_length' => 'Too much character'	
			]);
			$this->form_validation->set_rules('capt', 'captcha', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				// Generate captcha
				$random_string = random_string('numeric',6);
				$vals = array(
					'word'          => $random_string,
					'img_path'      => APPPATH.'../assets/images/captcha/',
					'img_url'       => base_url('/assets/images/captcha/'),
					'img_width'     => '150',
					'img_height'    => 34,
					'expiration'    => 7200,
					'word_length'   => 8,
					'font_size'     => 22,	
					// White background and border, black text and red grid
					'colors'        => array(
							'background' => array(51, 51, 51),
							'border' => array(255, 255, 255),
							'text' => array(255, 255, 255),
							'grid' => array(255, 40, 40)
					)
				);
				$cap = create_captcha($vals);
				// set data for captcha word
				$this->session->set_userdata('captchaWord',$cap['word']);
				// set data for capcha image name
				$this->session->set_userdata('captchaImage',$cap['filename']);

				// check config for title
				(!empty($configurations['siteName'])) ? $title1 = $this->uri->segment(1) : $title1 = "THDEV";
				(!empty($configurations['siteName'])) ? $title2 = $configurations['siteName'] : $title2 = "Web Developer";
				
				// get data from model
				$data = [
					'title' 		=> $title1.' | '.$title2,
					'captcha'	=> $cap['image'],
					'config' 	=> $this->configs->getConfiguration(),
				];
				$this->load->view('login', $data);
			} else {
				// if success call function login
				$captcha = $this->input->post('capt');
				if($this->session->userdata('captchaWord') == $captcha) {
					$this->Auth_model->_login();
				} else {
					// unset session captcha image name
					$this->session->unset_userdata('captchaImage');
					$this->session->set_flashdata('message', '<div class="alert alert-danger shadow alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Captcha not match!</div>');
					redirect('auth/');
				}
			}
		} else {
			redirect('errors/error404/','refresh');
		}
		
	}

	// refresh captcha
	public function recaptcha() {
		$captchaImage = $this->session->userdata('captchaImage');
		// unset image captcha
		unlink(FCPATH . 'assets/images/captcha/' . $captchaImage);

		// Generate captcha
		$random_string = random_string('numeric',6);
		$vals = array(
			'word'          => $random_string,
			'img_path'      => APPPATH.'../assets/images/captcha/',
			'img_url'       => base_url('/assets/images/captcha/'),
			'img_width'     => '150',
			'img_height'    => 34,
			'expiration'    => 7200,
			'word_length'   => 8,
			'font_size'     => 22,	
			// White background and border, black text and red grid
			'colors'        => array(
				'background' => array(51, 51, 51),
				'border' => array(255, 255, 255),
				'text' => array(255, 255, 255),
				'grid' => array(255, 40, 40)
			)
		);
		$cap = create_captcha($vals);
		$this->session->set_userdata('captchaWord',$cap['word']);
		// set data for capcha image name
		$this->session->set_userdata('captchaImage',$cap['filename']);
		echo $cap['image'];
	}
	

	// function registration
	public function registration() {
		// check session admin access
		if($this->session->userdata('admin_access')) {
			// check user session, if session available redirect to admin page
			if ($this->session->userdata('id')) {
				redirect('admin/dashboard/','refresh');
			}

			if($this->session->userdata('captchaImage')) {
				$captchaImage = $this->session->userdata('captchaImage');
				unlink(FCPATH . 'assets/images/captcha/' . $captchaImage);
				$this->session->unset_userdata('captchaImage');
			}

			// validation rules
			$this->form_validation->set_rules('full_name', 'Name', 'trim|required');
			$this->form_validation->set_rules('password2', 'Password', 'trim|required|matches[password1]');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[tbl_users.email]');
			$this->form_validation->set_rules('password1', 'Password', 'trim|required|min_length[6]|matches[password2]', [
				'matches' 	 => 'Password not match!',
				'min_length' => 'Password too short!'
				]);
			$this->form_validation->set_rules('capt', 'captcha', 'trim|required');
			$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[6]|is_unique[tbl_users.username]');
			if ($this->form_validation->run() == FALSE) {
				// Generate captcha
				$random_string = random_string('numeric',6);
				$vals = array(
					'word'          => $random_string,
					'img_path'      => APPPATH.'../assets/images/captcha/',
					'img_url'       => base_url('/assets/images/captcha/'),
					'img_width'     => '150',
					'img_height'    => 34,
					'expiration'    => 7200,
					'word_length'   => 8,
					'font_size'     => 22,	
					// White background and border, black text and red grid
					'colors'        => array(
							'background' => array(51, 51, 51),
							'border' => array(255, 255, 255),
							'text' => array(255, 255, 255),
							'grid' => array(255, 40, 40)
					)
				);
				$cap = create_captcha($vals);
				// set data for captcha word
				$this->session->set_userdata('captchaWord',$cap['word']);
				// set data for capcha image name
				$this->session->set_userdata('captchaImage',$cap['filename']);

				// check config for title
				(!empty($configurations['siteName'])) ? $title1 = $this->uri->segment(1) : $title1 = "THDEV";
				(!empty($configurations['siteName'])) ? $title2 = $configurations['siteName'] : $title2 = "Web Developer";
				
				// get data from model
				$data = [
					'title' 		=> $title1.' | '.$title2,
					'captcha'	 => $cap['image'],
					'config' 	 => $this->configs->getConfiguration(),
				];
				// if validation failed redirect to registration page
				$this->load->view('registration', $data);
			} else {
				$captcha = $this->input->post('capt');
				if($this->session->userdata('captchaWord') == $captcha) {
					$this->Auth_model->registration();
					$this->session->set_flashdata('message', '<div class="alert alert-success shadow" role="alert">Congratulation! Your account has been created.</div>');
					redirect('auth/registration/');
				} else {
					// unset session captcha image name
					$this->session->unset_userdata('captchaImage');
					$this->session->set_flashdata('message', '<div class="alert alert-danger shadow alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Captcha not match!</div>');
					redirect('auth/registration/');
				}
			}
		} else {
			redirect('errors/error404/','refresh');
		}
	}

	// function logout
	public function logout() {
		// unset user data session when click buton logout 
		$this->Auth_model->logoutAt();
		$sess_array = ['id', 'email', 'username', 'full_name', 'profile_picture', 'admin_access'];
		$this->session->unset_userdata($sess_array);
		$this->session->set_flashdata('message', '<div class="alert alert-success shadow alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> You have been logout!</div>');
		redirect(base_url('/'));
	}
}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */