<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partners_model extends CI_Model {

	// function get all data partner
	public function getAllPartners() {
		$this->db->select('tbl_partners.*, tbl_users.full_name');
		$this->db->from('tbl_partners');
		$this->db->join('tbl_users','tbl_users.id = tbl_partners.id_user', 'LEFT');
		$this->db->order_by('id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// function get data partner active
	public function getAllPartnersActive() {
		$this->db->select('tbl_partners.*, tbl_users.full_name');
		$this->db->from('tbl_partners');
		$this->db->join('tbl_users','tbl_users.id = tbl_partners.id_user', 'LEFT');
		$this->db->where('tbl_partners.status','Publish');
		$this->db->order_by('tbl_partners.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// function get data partner limiter
	public function public_home_partners() {
		$this->db->select('tbl_partners.*, tbl_users.full_name');
		$this->db->from('tbl_partners');
		$this->db->join('tbl_users','tbl_users.id = tbl_partners.id_user', 'LEFT');
		$this->db->where('tbl_partners.status','Publish');
		$this->db->order_by('tbl_partners.id','DESC');
		$this->db->limit(10);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	// get data partner by ID
	public function getPartnersById($id) {
		return $this->db->get_where('tbl_partners', ['id' => $id])->row_array();
	}

	// function create ddata partner
	public function create($data) {	
		// get data upload file from input file
		$upload_image = $_FILES['image']['name'];
		// check uploaded file
		if ($upload_image) {
			// define config upload file
			$config['max_size']     	= 1024;
			$config['width']	     	= 300;
			$config['allowed_types'] 	= 'jpg|png|jpeg';
			$config['upload_path'] 		= './assets/images/partners/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-partner-';
			// initializing config upload file
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('image')) {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . '</div>');
				redirect('admin/partners');
			} else {
				// if success collecting data to insert tables
				$data = [
					'created_at' 	=> date('Y-m-d H:i:s'),
					'id_user' 		=> $data['user']['id'],
					'image' 		=> $this->upload->data('file_name'),
					'email' 		=> $this->input->post('email', true),
					'status' 		=> $this->input->post('status', true),
					'deputy_name' 	=> $this->input->post('deputy', true),
					'partner_name'  => $this->input->post('partnerName', true),
					'phone_number'  => $this->input->post('phoneNumber', true),
				];
			}
		} else {
			// if nothing uploaded file
			// if success collecting data to insert tables
			$data = [
				'created_at'   => date('Y-m-d H:i:s'),
				'id_user' 	   => $data['user']['id'],
				'email'		   => $this->input->post('email', true),
				'deputy_name'  => $this->input->post('deputy', true),
				'status' 	   => $this->input->post('status', true),
				'partner_name' => $this->input->post('partnerName', true),
				'phone_number' => $this->input->post('phoneNumber', true),
			];
		}
		// query insert
		$this->db->insert('tbl_partners', $data);
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed insert data!</div>');
			redirect('admin/partners/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Success!</div>');
			redirect('admin/partners/');
		}
		
	}

	// function edit data partner
	public function edit($data) {
		// get data  from input form
		$updated_at 	= date('Y-m-d H:i:s');
		$email 			= $this->input->post('email', true);
		$status 		= $this->input->post('status', true);
		$deputy_name 	= $this->input->post('deputy', true);
		$partner_name 	= $this->input->post('partnerName', true);
		$phone_number 	= $this->input->post('phoneNumber', true);
		$id_user 		= $data['user']['id'];
		$upload_image = $_FILES['image']['name'];
		if ($upload_image) {
			// if success
			// define config upload file
			$config['max_size']     	= 1024;
			$config['width']        	= 300;
			$config['allowed_types'] 	= 'jpg|png|jpeg';
			$config['upload_path'] 		= './assets/images/partners/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-parnter-';
			// initializing config upload data
			$this->load->library('upload', $config);
			if ($this->upload->do_upload('image')) {
				// get old file
				$old_image = $data['partner']['image'];
				// unlink upload file
				if ($old_image != 'default.png') {
					unlink(FCPATH . 'assets/images/partners/' . $old_image);
				}
				// set file to database
				$this->db->set('image', $this->upload->data('file_name'));
			} else {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . '</div>');
				redirect('admin/partners/edit/' . $data['id']);
			}
		} 
		// query update
		$this->db->set('email', $email);
		$this->db->set('status', $status);
		$this->db->set('updated_at', $updated_at);
		$this->db->set('deputy_name', $deputy_name);
		$this->db->set('partner_name', $partner_name);
		$this->db->set('phone_number', $phone_number);
		$this->db->where('id', $data['id']);
		$this->db->update('tbl_partners');
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to edit data!</div>');
			redirect('admin/partners/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your partner has been updated!</div>');
			redirect('admin/partners/');
		}
		
	}

	// function delete data partner
	public function delete($data) {
		// get old file
		$old_image = $data['partner']['image'];
		// delete old file
		if ($old_image != 'default.png') {
			unlink(FCPATH . 'assets/images/partners/' . $old_image);
		}
		// query delete
		$this->db->where('id', $data['id']);
		$this->db->delete('tbl_partners');
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed delete data!</div>');
			redirect('admin/partners/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your partner has been delete!</div>');
			redirect('admin/partners/');
		}
		
	}
}

/* End of file Partners_model.php */
/* Location: ./application/models/Partners_model.php */