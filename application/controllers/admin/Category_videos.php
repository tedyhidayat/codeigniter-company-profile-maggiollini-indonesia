<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category_videos extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		security_access();
		$this->load->model('User_model');
		$this->load->model('Category_videos_model');
	}

	// function show index categoriey videos
    public function index() {
		// get data from models
		$data = [
			'title' 			=> 'Category Videos',
			'config' 			=> $this->configs->getConfiguration(),
			'user' 				=> $this->User_model->getUserBySession(),
			'category_videos' 	=> $this->Category_videos_model->getAllCategoryVideos(),
		];
		// view index category videos
        $this->templating->load('layouts/admin/wrapper', 'admin/category_videos/index', $data);
	}
	
	// function create category file page
	public function create() {
		// get data from models
		$data = [
			'title' 	=> 'Add Category Videos',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
		];
		// validation rules
		$this->form_validation->set_rules('categName', 'category', 'trim|required|is_unique[tbl_category_videos.name]', [
			'is_unique' => 'Categoty Name already exist!'
		]);
		$this->form_validation->set_rules('position', 'position', 'required|numeric|is_unique[tbl_category_videos.position]', [
			'is_unique' => 'Position already exist!'
		]);
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/category_videos/create', $data);
		} else {
			// if validation success call model create from category_videos_model
			$this->Category_videos_model->create($data);
		}
	}

	// function edit category file page
	public function edit($id) {
		// get data from models
		$data = [
			'id' 		=> $id,
			'title' 	=> 'Edit Category Videos',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'category' 	=> $this->Category_videos_model->getCategoryVideosById($id),
		];
		// validation rules
		$this->form_validation->set_rules('categName', 'category name', 'trim|required');
		$this->form_validation->set_rules('position', 'position', 'required|trim|numeric');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/category_videos/update', $data);
		} else {
			// if validation success call model create from category_videos_model
			$this->Category_videos_model->edit($data);
		}
	}

	// function delete category file page
	public function delete($id) {
		// get data from models
		$data = ['id' => $id, 'category' => $this->Category_videos_model->getCategoryVideosById($id)];
		// call function delete from category_videos_model
		$this->Category_videos_model->delete($data);
	}
	
}

/* End of file Category_videos.php */

?>
