    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $title; ?>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <?= $this->session->flashdata('message'); ?>

        <div class="row">
            <div class="col-xs-12 col-md-4">
                <div class="box box-primary">
                    <div class="box-header with-border disabled">
                        <h4><i class="fa fa-image"></i> Icon and Logo</h4>
                    </div>
                    <!-- /.box-header -->
                    <form method="post" action="<?= site_url('admin/configurations/editLogo/' . $configId['id']); ?>"
                        enctype="multipart/form-data" role="form">
                        <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" class="form-control" value="<?= $this->security->get_csrf_hash(); ?>" >
                        <div class="box-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-md-3">
                                        <?php if($configId['logo']) : ?>
                                        <img src="<?= base_url('assets/images/configurations/' . $configId['logo']); ?>"
                                            width="100" class="img-thumbnail">
                                        <?php else : ?>
                                        <p>N/A</p>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-xs-12 col-md-9">
                                        <label for="logo">Logo</label>
                                        <input type="file" name="image" class="form-control" id="logo"
                                            placeholder="logo" value="<?= $configId['logo']; ?>">
                                        <?= form_error('logo', '<small class="text-danger">', '</small>'); ?>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success btn-flat pull-right">Edit Logo</button>
                        </div>
                    </form>
                    <form method="post" action="<?= site_url('admin/configurations/editIcon/' . $configId['id']); ?>"
                        enctype="multipart/form-data" role="form">
                        
                        <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" class="form-control" value="<?= $this->security->get_csrf_hash(); ?>" >
                        <div class="box-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-md-3">
                                        <?php if($configId['icon']) : ?>
                                        <img src="<?= base_url('assets/images/configurations/' . $configId['icon']); ?>"
                                            width="100" class="img-thumbnail">
                                        <?php else : ?>
                                        <p>N/A</p>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-xs-12 col-md-9">
                                        <label for="icon">Icon</label>
                                        <input type="file" name="image" class="form-control" id="icon"
                                            placeholder="icon" value="<?= $configId['icon']; ?>">
                                        <?= form_error('icon', '<small class="text-danger">', '</small>'); ?>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success btn-flat pull-right">Edit Icon</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-xs-12 col-md-8">
                <div class="box box-primary">
                    <div class="box-header with-border disabled">
                        <h4><i class="fa fa-globe"></i> General</h4>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" action="<?= site_url('admin/configurations/edit/' . $configId['id']); ?>"
                        enctype="multipart/form-data" role="form">
                        
                        <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" class="form-control" value="<?= $this->security->get_csrf_hash(); ?>" >
                        <div class="box-body">

                            <div class="form-group">
                                <label for="siteName">Site Name</label>
                                <input type="text" name="siteName" class="form-control" id="siteName"
                                    placeholder="Web Name" value="<?= $configId['site_name']; ?>">
                                <?= form_error('siteName', '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="tagline">Tagline</label>
                                <input type="text" name="tagline" class="form-control" id="tagline"
                                    placeholder="Tagline" value="<?= $configId['tagline']; ?>">
                                <?= form_error('tagline', '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="keywords">Keywords</label>
                                <input type="text" name="keywords" class="form-control" id="keywords"
                                    placeholder="keywords" value="<?= $configId['keywords']; ?>">
                                <?= form_error('keywords', '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="siteUrl">Site URL</label>
                                <input type="text" name="siteUrl" class="form-control" id="siteUrl"
                                    placeholder="https://www.example.com" value="<?= $configId['site_url']; ?>">
                                <?= form_error('siteUrl', '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="metaText">Meta Text</label>
                                <textarea class="form-control" id="metaText" name="metaText" rows="5"
                                    cols="30"><?= $configId['meta_text']; ?></textarea>
                                <?= form_error('metaText', '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="metaDesc">Meta Description</label>
                                <textarea class="form-control" id="metaDesc" name="metaDesc" rows="5"
                                    cols="30"><?= $configId['meta_desc']; ?></textarea>
                                <?= form_error('metaDesc', '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="">
                                <div class="row">
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="text" name="email" class="form-control" id="email"
                                                placeholder="email" value="<?= $configId['email']; ?>">
                                            <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                            <label for="telephone">Telephone Number</label>
                                            <input type="text" name="telephone" class="form-control" id="telephone"
                                                placeholder="telephone" value="<?= $configId['telephone']; ?>">
                                            <?= form_error('telephone', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                            <label for="fax">Fax Number</label>
                                            <input type="text" name="fax" class="form-control" id="fax"
                                                placeholder="fax" value="<?= $configId['fax']; ?>">
                                            <?= form_error('fax', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                            <label for="wa">WhatsApp</label>
                                            <input type="text" name="wa" class="form-control" id="wa"
                                                placeholder="Whatsapp Number" value="<?= $configId['whatsapp']; ?>">
                                            <?= form_error('wa', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <div class="row">
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                            <label for="ig">Instagram</label>
                                            <input type="text" name="ig" class="form-control" id="ig" placeholder="ig"
                                                value="<?= $configId['instagram']; ?>">
                                            <?= form_error('ig', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                            <label for="fb">Facebook</label>
                                            <input type="text" name="fb" class="form-control" id="fb" placeholder="fb"
                                                value="<?= $configId['facebook']; ?>">
                                            <?= form_error('fb', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                            <label for="tw">Twitter</label>
                                            <input type="text" name="tw" class="form-control" id="tw" placeholder="tw"
                                                value="<?= $configId['twitter']; ?>">
                                            <?= form_error('tw', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="address">Address</label>
                                <textarea class="form-control" id="address" name="address" rows="5"
                                    cols="30"><?= $configId['address']; ?></textarea>
                                <?= form_error('address', '<small class="text-danger">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="gmaps">Google Maps</label>
                                <textarea class="form-control" id="gmaps" name="gmaps" rows="5"
                                    cols="30"><?= $configId['gmaps']; ?></textarea>
                                <?= form_error('gmaps', '<small class="text-danger">', '</small>'); ?>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success btn-flat pull-right">Edit</button>
                            <a href="<?= site_url('admin/configurations/'); ?>" class="btn pull-right">Cancel</a>
                        </div>
                    </form>
                </div>
                </ div>
            </div>


    </section>
