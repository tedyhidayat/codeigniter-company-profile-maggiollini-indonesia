<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category_abouts extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		security_access();
		$this->load->model('User_model');
		$this->load->model('Category_abouts_model');
	}

	// function show index categories about
    public function index() {
		// get data from moedels
		$data = [
			'title' 			=> 'Category Abouts',
			'config' 			=> $this->configs->getConfiguration(),
			'user' 				=> $this->User_model->getUserBySession(),
			'category_abouts' 	=> $this->Category_abouts_model->getAllCategoryAbouts(),
		];
		// view index categories about
        $this->templating->load('layouts/admin/wrapper', 'admin/category_abouts/index', $data);
	}
	
	// function create categories about page
	public function create() {
		// get data from models
		$data = [
			'title' 	=> 'Add Category abouts',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
		];
		// validation rules
		$this->form_validation->set_rules('categName', 'category', 'trim|required|is_unique[tbl_category_abouts.name]', [
			'is_unique' => 'Categoty ' . $this->input->post('categName') . ' already exist!'
		]);
		$this->form_validation->set_rules('position', 'position', 'required|numeric|is_unique[tbl_category_abouts.position]', [
			'is_unique' => 'position already exist!'
		]);
		if ($this->form_validation->run() == FALSE) {
			$this->templating->load('layouts/admin/wrapper', 'admin/category_abouts/create', $data);
		} else {
			// if validation success call model create from Categories_about_model
			$this->Category_abouts_model->create($data);
		}
	}

	// function edit categories about page
	public function edit($id) {
		// get data from models
		$data = [
			'id' 		=> $id,
			'title' 	=> 'Edit Category About',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'category' 	=> $this->Category_abouts_model->getCategoryAboutsById($id),
		];
		// validation rules
		$this->form_validation->set_rules('categName', 'category name', 'trim|required');
		$this->form_validation->set_rules('position', 'position', 'required|trim|numeric');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/category_abouts/update', $data);
		} else {
			// if validation success call model edit from categories_about_model
			$this->Category_abouts_model->edit($data);
		}
	}

	// call function delete from categories_about_model
	public function delete($id) {
		// get data from models
		$data = ['id' => $id, 'category' => $this->Category_abouts_model->getCategoryAboutsById($id)];
		// call function delete from categories_about_model
		$this->Category_abouts_model->delete($data);
	}
	
}

/* End of file Category_abouts.php */

?>