<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Errors extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		$this->load->model('Gallery_model');
		
	}

	// function show login page
	public function error404()	{
		// unset sessinon captcha if there
		$this->session->unset_userdata('admin_access');
		$this->session->unset_userdata('captchaImage');
		// identification
		$configurations = $this->configs->getConfiguration();
		// check config for title
		(!empty($configs)) ? $title1 = '404 Not found!' : $title1 = "THDEV";
		(!empty($configurations['siteName'])) ? $title2 = $configurations['siteName'] : $title2 = "Web Developer";

		$data = [
			'configs' 		=> $this->configs->getConfiguration(),
			'title' 		=> $title1.' | '.$title2,
			
        ];
        $this->load->view('error404',$data);
	}

}

/* End of file Services.php */
/* Location: ./application/controllers/Index.php */