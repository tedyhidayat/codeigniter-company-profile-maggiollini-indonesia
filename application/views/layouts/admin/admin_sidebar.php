<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
            <img src="<?= base_url('assets/images/user/'.$user['profile_picture']); ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
            <p><?= $user['full_name']; ?></p>
            <a href="#"><i class="fa fa-circle text-blue"></i> <?= $user['level']; ?></a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">

            <?php if($config) : ?>
                <li class="header">MAIN NAVIGATION</li>
                <!-- dashboard -->
                <li <?= $this->uri->segment(2) == 'dashboard' || $this->uri->segment(2) == '' ? 'class="active"' : '' ?>>
                    <a href="<?= site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <!-- posts -->
                <li class="treeview <?= $this->uri->segment(2) == 'posts' || $this->uri->segment(2) == 'categories' ? 'active' : '' ?>">
                    <a href="#">
                        <i class="fa fa-file-text-o"></i>
                        <span>Posts</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li
                            <?= $this->uri->segment(2) == 'posts' || $this->uri->segment(2) == '' ? 'class="active"' : '' ?>>
                            <a href="<?= site_url('admin/posts/'); ?>"><i class="fa fa-list"></i> Listing Posts</a></li>
                        <li
                            <?= $this->uri->segment(2) == 'categories' || $this->uri->segment(2) == '' ? 'class="active"' : '' ?>>
                            <a href="<?= site_url('admin/categories/'); ?>"><i class="fa fa-tags"></i> Category</a>
                        </li>
                    </ul>
                </li>
                <!-- services -->
                <li <?= $this->uri->segment(2) == 'services' || $this->uri->segment(2) == '' ? 'class="active"' : '' ?>>
                    <a href="<?= site_url('admin/services/'); ?>"><i class="fa fa-heart"></i>
                        <span>Services</span>
                    </a>
                </li>
                <!-- products -->
                <li class="treeview <?= $this->uri->segment(2) == 'projects' || $this->uri->segment(2) == 'category_projects' || $this->uri->segment(2) == 'sample' || $this->uri->segment(2) == 'category_sample' ? 'active' : '' ?>">
                    <a href="#">
                        <i class="fa fa-pie-chart"></i>
                        <span>Products</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <!-- projects -->
                        <li class="treeview <?= $this->uri->segment(2) == 'projects' || $this->uri->segment(2) == 'category_projects' ? 'active' : '' ?>">
                            <a href="#"><i class="fa fa-circle-o"></i> Projects
                                <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li <?= $this->uri->segment(2) == 'projects' || $this->uri->segment(2) == '' ? 'class="active"' : '' ?>><a href="<?= site_url('admin/projects/') ?>"><i class="fa fa-list"></i> Listing Projects</a></li>
                                <li <?= $this->uri->segment(2) == 'category_projects' || $this->uri->segment(2) == '' ? 'class="active"' : '' ?>><a href="<?= site_url('admin/category_projects/') ?>"><i class="fa fa-tags"></i> Categories</a></li>
                            </ul>
                        </li>
                        <!-- sample -->
                        <li class="treeview <?= $this->uri->segment(2) == 'sample' || $this->uri->segment(2) == 'category_sample' ? 'active' : '' ?>">
                            <a href="#"><i class="fa fa-circle-o"></i> Sample
                                <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li <?= $this->uri->segment(2) == 'sample' || $this->uri->segment(2) == '' ? 'class="active"' : '' ?>><a href="<?= site_url('admin/sample/') ?>"><i class="fa fa-list"></i> Listing Sample</a></li>
                                <li <?= $this->uri->segment(2) == 'category_sample' || $this->uri->segment(2) == '' ? 'class="active"' : '' ?>><a href="<?= site_url('admin/category_sample/') ?>"><i class="fa fa-tags"></i> Categories</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- partners -->
                <li <?= $this->uri->segment(2) == 'partners' || $this->uri->segment(2) == '' ? 'class="active"' : '' ?>>
                    <a href="<?= site_url('admin/partners/'); ?>"><i class="fa fa-users"></i> <span>Partners</span>
                    </a>
                </li>
                <!-- careers -->
                <li <?= $this->uri->segment(2) == 'careers' || $this->uri->segment(2) == '' ? 'class="active"' : '' ?>>
                    <a href="<?= site_url('admin/careers/'); ?>"><i class="fa fa-user"></i> <span>Careers</span>
                    </a>
                </li>
                <!-- about -->
                <li class="treeview <?= $this->uri->segment(2) == 'abouts' || $this->uri->segment(2) == 'category_abouts' ? 'active' : '' ?>">
                    <a href="#">
                        <i class="fa fa-edit"></i>
                        <span>About Us</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li
                            <?= $this->uri->segment(2) == 'abouts' || $this->uri->segment(2) == '' ? 'class="active"' : '' ?>>
                            <a href="<?= site_url('admin/abouts/'); ?>"><i class="fa fa-list"></i> Listing About</a></li>
                        <li
                            <?= $this->uri->segment(2) == 'category_abouts' || $this->uri->segment(2) == '' ? 'class="active"' : '' ?>>
                            <a href="<?= site_url('admin/category_abouts/'); ?>"><i class="fa fa-tags"></i> Category</a>
                        </li>
                    </ul>
                </li>
                <!-- galleries -->
                <li class="treeview <?= $this->uri->segment(2) == 'gallery' || $this->uri->segment(2) == 'category_gallery' ? 'active' : '' ?>">
                    <a href="#">
                        <i class="fa fa-image"></i>
                        <span>Gallery</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li
                            <?= $this->uri->segment(2) == 'gallery' || $this->uri->segment(2) == '' ? 'class="active"' : '' ?>>
                            <a href="<?= site_url('admin/gallery/'); ?>"><i class="fa fa-list"></i> Listing Gallery</a>
                        </li>
                        <li
                            <?= $this->uri->segment(2) == 'category_gallery' || $this->uri->segment(2) == '' ? 'class="active"' : '' ?>>
                            <a href="<?= site_url('admin/category_gallery/'); ?>"><i class="fa fa-tags"></i> Category</a>
                        </li>
                    </ul>
                </li>
                <!-- Multimedia -->
                <li class="treeview <?= $this->uri->segment(2) == 'files' || 
                                        $this->uri->segment(2) == 'category_files' || 
                                        $this->uri->segment(2) == 'videos' || 
                                        $this->uri->segment(2) == 'category_videos' ? 'active' : '' ?>">
                    <a href="#">
                        <i class="fa fa-film"></i>
                        <span>Multimedia</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <!-- files -->
                        <li class="treeview <?= $this->uri->segment(2) == 'files' || 
                                                $this->uri->segment(2) == 'category_files' ? 'active' : '' ?>">
                            <a href="#"><i class="fa fa-file-pdf-o"></i> File Uploads
                                <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li <?= $this->uri->segment(2) == 'files' || 
                                        $this->uri->segment(2) == '' ? 'class="active"' : '' ?>>
                                    <a href="<?= site_url('admin/files/') ?>"><i class="fa fa-list"></i> Listing Files</a>
                                </li>
                                <li <?= $this->uri->segment(2) == 'category_files' || 
                                        $this->uri->segment(2) == '' ? 'class="active"' : '' ?>>
                                    <a href="<?= site_url('admin/category_files/') ?>"><i class="fa fa-tags"></i> Categories</a>
                                </li>
                            </ul>
                        </li>
                        <!-- Videos -->
                        <li class="treeview <?= $this->uri->segment(2) == 'videos' || 
                                                $this->uri->segment(2) == 'category_videos' ? 'active' : '' ?>">
                            <a href="#"><i class="fa fa-file-video-o"></i> Upload Videos
                                <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li <?= $this->uri->segment(2) == 'videos' || 
                                        $this->uri->segment(2) == '' ? 'class="active"' : '' ?>>
                                    <a href="<?= site_url('admin/videos/') ?>"><i class="fa fa-list"></i> Listing Videos</a>
                                </li>
                                <li <?= $this->uri->segment(2) == 'category_videos' || 
                                        $this->uri->segment(2) == '' ? 'class="active"' : '' ?>>
                                    <a href="<?= site_url('admin/category_videos/') ?>"><i class="fa fa-tags"></i> Categories</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            <?php endif; ?>

            <li class="header">USER ACCOUNT</li>
            <!-- profile -->
            <li <?= ($this->uri->segment(2) == 'user' && $this->uri->segment(3) == 'profile') ? 'class="active"' : '' ?>>
                <a href="<?= site_url('admin/user/profile/'); ?>">
                    <i class="fa fa-user"></i> <span>Profile Account</span>
                </a>
            </li>

            <!-- visible only user have session superadmin -->
            <?php if($user['level'] === 'superadmin') : ?>
            <li <?= ($this->uri->segment(2) == 'user' && $this->uri->segment(3) == '') ||
                    ($this->uri->segment(2) == 'user' && $this->uri->segment(3) == 'create') ||
                    ($this->uri->segment(2) == 'user' && $this->uri->segment(3) == 'edit')
                    ? 'class="active"' : '' ?>>
                <a href="<?= site_url('admin/user/'); ?>">
                    <i class="fa fa-user"></i> <span>Users</span>
                </a>
            </li>
            <!-- configurations -->
            <?= ($user['level'] === 'superadmin') ? '<li class="header">WEB SETTINGS</li>' : '' ?>
            <li <?= $this->uri->segment(2) == 'config-page' ||
                    $this->uri->segment(2) == '' ? 'class="active"' : '' ?>>
                <a href="<?= site_url('admin/config-page/'); ?>">
                    <i class="fa fa-cog"></i> <span>Configuration Pages</span>
                </a>
            </li>
            <li <?= $this->uri->segment(2) == 'configurations' ||
                    $this->uri->segment(2) == '' ? 'class="active"' : '' ?>>
                <a href="<?= site_url('admin/configurations/'); ?>">
                    <i class="fa fa-cog"></i> <span>Configuration General</span>
                </a>
            </li>
            <?php endif; ?>
            <!-- logout -->
            <li>
                <a href="#" data-toggle="modal" data-target="#modal-warning"><i class="fa fa-sign-out text-red"></i>
                    <span>Logout</span>
                </a>
            </li>
        </ul>
    </section>
    <!--/.sidebar -->
</aside>
