<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?= $title; ?></h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- flash data -->
    <?= $this->session->flashdata('message'); ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-success">
                <div class="box-header">
                    <a href="<?= site_url('admin/sample/create'); ?>" class="btn btn-success btn-sm btn-flat"><i class="fa fa-plus"></i> Create New</a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="50">NO.</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Category</th>
                                    <th class="text-center">Status</th>
                                    <th width="100">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!$allSample) : ?>
                                <tr>
                                    <td class="text-center" colspan="8">Empty!</td>
                                </tr>
                                <?php else : ?>
                                <?php $no = 1; foreach ($allSample as $row): ?>
                                <tr>
                                    <td><?= $no++; ?>.</td>
                                    <td>
                                        <?php if ($row['image']) :?>
                                        <img width="50"
                                            src="<?= base_url('assets/images/projects/sample/thumbs/'.$row['image']); ?>"
                                            class="img-thumbnail"></td>
                                    <?php else : ?>
                                    N/A
                                    <?php endif; ?>
                                    <td>
                                        <b><?= $row['sample_name']; ?></b><br>
                                        <small class="text-info"><b>Uploaded at : </b><?= date("d F Y", strtotime($row['created_at'])); ?></small><br>
                                        <small class="text-info"><b>By : </b><?= $row['full_name']; ?></small><br>
                                    </td>
                                    <td><?= $row['name']; ?></td>
                                    <td class="text-center">
                                        <?php if($row['status'] == 'Pending') : ?>
                                        <span class="btn btn-xs btn-warning btn-flat">Pending</span>
                                        <?php elseif($row['status'] == 'Publish') : ?>
                                        <span class="btn btn-xs btn-success btn-flat">Publish</span>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <a href="<?= site_url('admin/sample/edit/' . $row['id']); ?>"
                                            class="btn btn-info btn-sm btn-flat"><i class="fa fa-pencil"></i></a>
                                        <?php include('detail.php') ?>
                                        <?php include('delete.php') ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>