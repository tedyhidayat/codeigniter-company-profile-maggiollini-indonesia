<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?= $title; ?></h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- flash data -->
    <?= $this->session->flashdata('message'); ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="<?= site_url('admin/videos/edit/' . $video['id']); ?>"
                    enctype="multipart/form-data" role="form">
                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" class="form-control" value="<?= $this->security->get_csrf_hash(); ?>" >
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <?php if($video['video_name']) : ?>
                                    <div class="media">
                                        <video width="320" controls>
                                            <source src="<?= base_url('assets/videos/' . $video['video_name']); ?>" type="video/mp4">
                                        </video>
                                    </div>
                                <?php else : ?>
                                    N/A
                                <?php endif; ?>
                            </div>
                            <div class="col-xs-12 col-md-8">
                                <div class="row">
                                    <div class="col-xs-12 col-md-8">
                                        <div class="form-group">
                                        <label for="title">Title</label>
                                            <input type="text" name="title" class="form-control" id="title"
                                                value="<?= $video['title']; ?>">
                                            <?= form_error('title', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                        <label for="status">Status</label>
                                            <select name="status" id="status" class="form-control">
                                                <?php if (!$video['status']) : ?>
                                                <option value="">Selecet Status</option>
                                                <option value="Publish">Publish</option>
                                                <option value="Pending">Pending</option>
                                                <?php else : ?>
                                                <?php if ($video['status'] == 'Publish') : ?>
                                                <option value="Publish">Publish</option>
                                                <option value="Pending">Pending</option>
                                                <?php elseif ($video['status'] == 'Pending') : ?>
                                                <option value="Pending">Pending</option>
                                                <option value="Publish">Publish</option>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-8">
                                        <div class="form-group">
                                        <label for="categId">Category</label>
                                            <select name="categId" id="categId" class="form-control">
                                                <?php foreach ($categories as $row) : ?>
                                                <option value=" <?= $row['id']; ?>"
                                                    <?= $row['id'] == $video['id_category'] || $row['id'] == '' ? 'selected' : '' ?>>
                                                    <?= $row['name']; ?>
                                                </option>
                                                <?php endforeach; ?>
                                            </select>
                                            <?= form_error('categId', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                        <label for="video">Upload Video</label>
                                            <input type="file" name="video" class="form-control" id="video">
                                            <small class="text-info">* Max size 100 Mb</small><br>
                                            <small class="text-info">* Only for mp4 | webm</small><br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-12" style="padding-top: 20px;">
                                   <b><label>Video Directory</label> :</b>
                                   <?php if($video['video_name']) : ?>
                                        <a href="<?= base_url('assets/videos/'.$video['video_name']); ?>" target="_blank" class="text-green"><?= base_url('assets/videos/'.$video['video_name']); ?></a>
                                   <?php else : ?>
                                        N/A
                                   <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="keywords">Keywords (SEO)</label>
                            <textarea class="form-control" name="keywords" id="keywords" rows="2" cols="10" ><?= $video['keywords']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description" id="editor1" cols="10"
                                placeholder="Type description" rows="5"><?= $video['description']; ?></textarea>
                            <?= form_error('description', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success btn-flat pull-right">Edit</button>
                        <a href="<?= site_url('admin/videos/'); ?>" class="btn pull-right">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>