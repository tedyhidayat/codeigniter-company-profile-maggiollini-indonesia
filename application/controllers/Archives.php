<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Archives extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		create_cookie();
		$this->load->model('Files_model');
		$this->load->model('Category_files_model');
	}

	// function show  page
	public function index()	{
		// unset sessinon captcha if there
		$this->session->unset_userdata('admin_access');
		$this->session->unset_userdata('captchaImage');
		// identification
		$configurations = $this->configs->getConfiguration();

		// check config for title
		(!empty($configurations['siteName'])) ? $title1 = $this->uri->segment(1) : $title1 = "THDEV";
		(!empty($configurations['siteName'])) ? $title2 = $configurations['siteName'] : $title2 = "Web Developer";
		
		$data = [
			'configs' 		=> $this->configs->getConfiguration(),
			'files' 		=> $this->Files_model->getAllFileActive(),
			'category' 		=> $this->Category_files_model->getAllCategoryFiles(),
			'config_page' 	=> $this->config_page->getAllConfigPageActiveHeading(),
			'title' 		=> $title1.' | '.$title2,
        ];
        
        $this->templating->load('layouts/public/wrapper', 'archives', $data);
    }
    
    public function download() {
		$this->load->helper('download');
		$data = $this->uri->segment(3);
		$file = 'assets/files/'.$data;
		force_download($file, NULL);
	}


}

/* End of file Archives.php */
/* Location: ./application/controllers/Index.php */