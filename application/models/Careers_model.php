<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Careers_model extends CI_Model {

	// sitemap
	public function sitemap() {
		$this->db->select('tbl_careers.slug, tbl_careers.created_at, tbl_careers.title');
		$this->db->from('tbl_careers');
		$this->db->order_by('id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// get all data careers
	public function getAllCareers() {
		$this->db->select('tbl_careers.*, tbl_users.full_name' );
		$this->db->from('tbl_careers');
		$this->db->join('tbl_users','tbl_users.id = tbl_careers.id_user', 'LEFT');
		$this->db->order_by('tbl_careers.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// get serval data careers
	public function getServalCareers() {
		$this->db->select('tbl_careers.*, tbl_users.full_name' );
		$this->db->from('tbl_careers');
		$this->db->join('tbl_users','tbl_users.id = tbl_careers.id_user', 'LEFT');
		$this->db->limit(5);
		$this->db->order_by('tbl_careers.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// get all data careers active footer public
	public function getAllCareersFooter() {
		$this->db->select('tbl_careers.*, tbl_users.full_name' );
		$this->db->from('tbl_careers');
		$this->db->join('tbl_users','tbl_users.id = tbl_careers.id_user', 'LEFT');
		$this->db->where('tbl_careers.status','Publish');
		$this->db->limit(5);
		$this->db->order_by('tbl_careers.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// get all data careers active
	public function getAllCareersActive() {
		$this->db->select('tbl_careers.*, tbl_users.full_name' );
		$this->db->from('tbl_careers');
		$this->db->join('tbl_users','tbl_users.id = tbl_careers.id_user', 'LEFT');
		$this->db->where('tbl_careers.status','Publish');
		$this->db->order_by('tbl_careers.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	// function get career by id
	public function getCareersBySlug($slug) {
		$query = $this->db->get_where('tbl_careers', ['slug' => $slug])->row_array();
		if(!$query) {
			redirect('errors/error404','refresh');
			die;
		} else {
			return $query;
		}
	}

	// function get career by id 
	public function getCareersById($id) {
		return $this->db->get_where('tbl_careers', ['id' => $id])->row_array();
	}

	// function create careers
	public function create($data) {	
		// get file from input form
		$upload_image = $_FILES['image']['name'];
		// check if there is data uploaded
		if ($upload_image) {
			// define configuration upload file
			$config['file_ext_tolower'] = TRUE;
			$config['max_size']     	= 2048;
			$config['allowed_types'] 	= 'jpg|png|jpeg';
			$config['upload_path'] 		= './assets/images/careers/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-career-';
			// initialize config data file upload
			$this->load->library('upload', $config);
			// check data uploaded
			if ( ! $this->upload->do_upload('image')) {
				// if nothing data uploaded
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . '</div>');
				redirect('admin/careers');
			} else {
				// if available data uploaded
				// set data for insert data to table
				$data = [
					'created_at' 	=> date('Y-m-d H:i:s'),
					'id_user' 		=> $this->session->userdata('id'),
					'image' 		=> $this->upload->data('file_name'),
					'title' 		=> $this->input->post('title', true),
					'salary' 		=> $this->input->post('salary', true),
					'status' 		=> $this->input->post('status', true),
					'keywords' 		=> $this->input->post('keywords', true),
					'position' 		=> $this->input->post('position', true),
					'description' 	=> $this->input->post('description', true),
					'slug' 			=> url_title($this->input->post('title', true), 'dash', true),
				];
			}
		} else {
			// set data for insert data to table
			$data = [
				'image' 		=> 'default.png',
				'created_at' 	=> date('Y-m-d H:i:s'),
				'id_user' 		=> $this->session->userdata('id'),
				'title' 		=> $this->input->post('title', true),
				'salary' 		=> $this->input->post('salary', true),
				'status' 		=> $this->input->post('status', true),
				'keywords' 		=> $this->input->post('keywords', true),
				'position' 		=> $this->input->post('position', true),
				'description' 	=> $this->input->post('description', true),
				'slug' 			=> url_title($this->input->post('title', true), 'dash', true),
			];
		}
		// query insert data
		$this->db->insert('tbl_careers', $data);
		// check if row affected in the tbl career
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed insert data!</div>');
			redirect('admin/careers/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Success insert data!</div>');
			redirect('admin/careers/');
		}
	}

	// function edit career
	public function edit($data) {
		// get data from input from
		$id_user 		= $this->session->userdata('id');
		$title 			= $this->input->post('title', true);
		$status 		= $this->input->post('status', true);
		$salary 		= $this->input->post('salary', true);
		$keywords 		= $this->input->post('keywords', true);
		$position 		= $this->input->post('position', true);
		$description	= $this->input->post('description', true);
		$slug 			= url_title($this->input->post('title', true), 'dash', true);
		// get data uploaded file
		$upload_image 	= $_FILES['image']['name'];
		// check uploaded file
		if ($upload_image) {
			// define configuration upload file
			$config['max_size']     	= 2048;
			$config['file_ext_tolower'] = TRUE;
			$config['allowed_types'] 	= 'jpg|png|jpeg';
			$config['upload_path'] 		= './assets/images/careers/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-career-';
			// initializing configuration
			$this->load->library('upload', $config);
			// check data if there is data uploaded
			if ($this->upload->do_upload('image')) {
				// define old image
				$old_image = $data['career']['image'];
				// if ilod image not same with default.png, delete old image 
				if ($old_image != 'default.png') {
					unlink(FCPATH . 'assets/images/careers/' . $old_image);
				}
				// get new file name
				$new_image = $this->upload->data('file_name');
				// set data new image to table
				$this->db->set('image', $new_image);
			} else {
				// if uploaded file error
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . '</div>');
				redirect('admin/careers/edit/' . $data['id']);
			}
		} 
		// query update
		$this->db->set('slug', $slug);
		$this->db->set('title', $title);
		$this->db->set('status', $status);
		$this->db->set('salary', $salary);
		$this->db->set('keywords', $keywords);
		$this->db->set('position', $position);
		$this->db->set('description', $description);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $data['id']);
		$this->db->update('tbl_careers');

		// check if affected rows in table career
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed update data!</div>');
			redirect('admin/careers/');
		} else {
			# if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your career has been updated!</div>');
			redirect('admin/careers/');
		}
	}

	// function delete career
	public function delete($data) {
		// get old image
		$old_image = $data['career']['image'];
		// check if old image not same default.png, delete old image
		if ($old_image != 'default.png') {
			unlink(FCPATH . 'assets/images/careers/' . $old_image);
		}
		// query delete
		$this->db->where('id', $data['id']);
		$this->db->delete('tbl_careers');
		// check data affected rows in the tbl career
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed delete!</div>');
			redirect('admin/careers/');
		} else {
			// if sucess
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your career has been delete!</div>');
			redirect('admin/careers/');
		}
		
	}

}

/* End of file Careers_model.php */
/* Location: ./application/models/Careers_model.php */