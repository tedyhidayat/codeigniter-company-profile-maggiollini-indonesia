<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Files extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		security_access();
		$this->load->model('User_model');
		$this->load->model('Files_model');
		$this->load->model('Category_files_model');
	}

	// function show index file page
    public function index() {
		// get data from models
		$data = [
			'title' => 'Files',
			'user' 	=> $this->User_model->getUserBySession(),
			'files' => $this->Files_model->getAllFiles(),
			'config'=> $this->configs->getConfiguration(),
		];
		// view index file page
        $this->templating->load('layouts/admin/wrapper', 'admin/files/index', $data);
	}
	
	// function create file page
	public function create() {
		// get data from models
		$data = [
			'title' 		=> 'Add Files',
			'config' 		=> $this->configs->getConfiguration(),
			'user' 			=> $this->User_model->getUserBySession(),
			'categories' 	=> $this->Category_files_model->getAllCategoryFiles(),
		];
		// validation rules
		$this->form_validation->set_rules('file', 'file', 'trim');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('keywords', 'keywords', 'trim');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('categId', 'category', 'trim|required');
		$this->form_validation->set_rules('description', 'Description', 'required|trim');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/files/create', $data);
		} else {
			// if validation success call function create from files_model
			$this->Files_model->create($data);
		}
	}

	// function edit file page
	public function edit($id) {
		// get data from models
		$data = [
			'title' 	=> 'Edit Files',
			'id' 		=> $id,
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'file' 		=> $this->Files_model->getFilesById($id),
			'categories'=> $this->Category_files_model->getAllCategoryFiles(),
		];
		// validation rules
		$this->form_validation->set_rules('file', 'file', 'trim');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('keywords', 'keywords', 'trim');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('categId', 'category', 'trim|required');
		$this->form_validation->set_rules('description', 'Description', 'required|trim');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/files/update', $data);
		} else {
			// if validation success call function edit from files_model
			$this->Files_model->edit($data);
		}
	}

	// function delete file page
	public function delete($id) {
		// get data from models
		$data = ['id' => $id, 'file' => $this->Files_model->getFilesById($id)];
		// call functionn delete from files_model
		$this->Files_model->delete($data);
	}
	
}

/* End of file Files.php */

?>