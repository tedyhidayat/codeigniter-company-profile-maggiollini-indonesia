<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		$this->load->model('Posts_model');
		$this->load->model('Abouts_model');
		$this->load->model('Videos_model');
		$this->load->model('Gallery_model');
		$this->load->model('Projects_model');
		$this->load->model('Partners_model');
		$this->load->model('Category_projects_model');
	}

	// function show login page
	public function index()	{
		// unset sessinon captcha if there
		$this->session->unset_userdata('admin_access');
		$this->session->unset_userdata('captchaImage');
		// identification
		$configurations = $this->configs->getConfiguration();
		$recent_post 	= $this->Posts_model->getRecentPost();
		
		// check config for title
		(!empty($configurations['siteName'])) ? $title1 = $configurations['siteName'] : $title1 = "THDEV";
		(!empty($configurations['tagline'])) ? $title2 = $configurations['tagline'] : $title2 = "Web Developer";
		

		$data = [
			'configs' 	  => $this->configs->getConfiguration(),
			'news' 	  	  => $this->Posts_model->getRecentPost(),
			'about' 	  => $this->Abouts_model->getAllAboutActive(),
			'videos' 	  => $this->Videos_model->public_home_videos(),
			'slider' 	  => $this->Gallery_model->public_home_slider(),
			'partners' 	  => $this->Partners_model->public_home_partners(),
			'services' 	  => $this->Services_model->public_home_services(),
			'projects' 	  => $this->Projects_model->public_home_projects(),
			'config_page' => $this->config_page->getAllConfigPageActiveHeading(),
			'category'	  => $this->Category_projects_model->getAllCategoryprojects(),
			'title' 	  => $title1 .' | '. $title2,

		];
		
        
        $this->templating->load('layouts/public/wrapper', 'home', $data);
	}
}

/* End of file Home.php */
/* Location: ./application/controllers/Index.php */