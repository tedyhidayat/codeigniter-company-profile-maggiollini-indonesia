<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category_sample extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		security_access();
		$this->load->model('User_model');
		$this->load->model('Category_sample_model');
	}

	// function show index category sample
    public function index() {
		// get data from models
		$data = [
			'title' 			=> 'Category Sample',
			'config' 			=> $this->configs->getConfiguration(),
			'user' 				=> $this->User_model->getUserBySession(),
			'category_sample' 	=> $this->Category_sample_model->getAllCategorySample(),
		];
		// view index category sample
        $this->templating->load('layouts/admin/wrapper', 'admin/category_sample/index', $data);
	}
	
	// function create category sample
	public function create() {
		// get data from models
		$data = [
			'title' 	=> 'Add Category Sample',
			'config'	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
		];
		// validation rules
		$this->form_validation->set_rules('categName', 'category', 'trim|required|is_unique[tbl_category_sample.name]', [
			'is_unique' => 'Category ' . $this->input->post('categName') . ' already exist!'
		]);
		$this->form_validation->set_rules('position', 'position', 'required|numeric|is_unique[tbl_category_sample.position]', [
			'is_unique' => 'Position already exist!'
		]);
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/category_sample/create', $data);
		} else {
			// if validation success call model create from category_sample_model
			$this->Category_sample_model->create($data);
		}
	}

	// function edit category sample
	public function edit($id) {
		// get data from models
		$data = [
			'id' 		=> $id,
			'title' 	=> 'Edit Category Sample',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'category' 	=> $this->Category_sample_model->getCategorySampleById($id),
		];
		// validation rules
		$this->form_validation->set_rules('categName', 'category name', 'trim|required');
		$this->form_validation->set_rules('position', 'position', 'required|trim|numeric');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/category_sample/update', $data);
		} else {
			// if validation success call edit create from category_sample_mode
			$this->Category_sample_model->edit($data);
		}
	}

	// function delte category sample
	public function delete($id) {
		// get data from models
		$data = ['id' => $id, 'category' => $this->Category_sample_model->getCategorySampleById($id)];
		// call function delete from Category_sample_model
		$this->Category_sample_model->delete($data);
	}
}

/* End of file Category_sample.php */

?>