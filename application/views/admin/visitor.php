<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?= $title; ?></h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ip address</th>
                            <th>os</th>
                            <th>browser</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?= $ip; ?></td>
                            <td><?= $os; ?></td>
                            <td><?= $browser; ?> - <?= $browser_version; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->