<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?= $title; ?></h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- flash data -->
    <?= $this->session->flashdata('message'); ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="<?= site_url('admin/files/edit/' . $file['id']); ?>"
                    enctype="multipart/form-data" role="form">
                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" class="form-control" value="<?= $this->security->get_csrf_hash(); ?>" >
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <?php if($file['file_name']) : ?>
                                    <embed src="<?= base_url('assets/files/' . $file['file_name']); ?>#page=1" type="application/pdf"> 
                                <?php else : ?>
                                    N/A
                                <?php endif; ?>
                            </div>
                            <div class="col-xs-12 col-md-8">
                                <div class="row">
                                    <div class="col-xs-12 col-md-8">
                                        <div class="form-group">
                                        <label for="title">File Name</label>
                                            <input type="text" name="title" class="form-control" id="title"
                                                value="<?= $file['title']; ?>">
                                            <?= form_error('title', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                        <label for="status">Status</label>
                                            <select name="status" id="status" class="form-control">
                                                <?php if (!$file['status']) : ?>
                                                <option value="">Selecet Status</option>
                                                <option value="Publish">Publish</option>
                                                <option value="Pending">Pending</option>
                                                <?php else : ?>
                                                <?php if ($file['status'] == 'Publish') : ?>
                                                <option value="Publish">Publish</option>
                                                <option value="Pending">Pending</option>
                                                <?php elseif ($file['status'] == 'Pending') : ?>
                                                <option value="Pending">Pending</option>
                                                <option value="Publish">Publish</option>
                                                <?php endif; ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-8">
                                        <div class="form-group">
                                        <label for="categId">Category</label>
                                            <select name="categId" id="categId" class="form-control">
                                                <?php foreach ($categories as $row) : ?>
                                                <option value=" <?= $row['id']; ?>"
                                                    <?= $row['id'] == $file['id_category'] || $row['id'] == '' ? 'selected' : '' ?>>
                                                    <?= $row['name']; ?>
                                                </option>
                                                <?php endforeach; ?>
                                            </select>
                                            <?= form_error('categId', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                        <label for="file">Upload File</label>
                                            <input type="file" name="file" class="form-control" id="file">
                                            <small class="text-info">* Max size 10 Mb</small><br>
                                            <small class="text-info">* Only for pdf | docx | pptx</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-12" style="padding-top: 20px">
                                   <b><label>File Directory</label> :</b>
                                   <?php if($file['file_name']) : ?>
                                        <a href="<?= base_url('assets/files/'.$file['file_name']); ?>" class="text-green" target="_blank"><?= base_url('assets/files/'.$file['file_name']); ?></a>
                                   <?php else : ?>
                                        N/A
                                   <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="keywords">Keywords (SEO)</label>
                            <textarea class="form-control" name="keywords" id="keywords" rows="2" cols="10" ><?= $file['keywords']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description" id="editor1" cols="10"
                                placeholder="Type description" rows="5"><?= $file['description']; ?></textarea>
                            <?= form_error('description', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success btn-flat pull-right">Edit</button>
                        <a href="<?= site_url('admin/files/'); ?>" class="btn pull-right">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>