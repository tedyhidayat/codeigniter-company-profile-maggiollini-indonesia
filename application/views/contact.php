<?php if($configs) : ?>   
    <!-- Breadcrumbs -->
    <section id="breadcrumbs">
        <div class="container">
            <div class=" row justify-content-between align-items-center">
                <div class="col-12 col-md col-lg">
                    <h2 class="text-uppercase"><?= $this->uri->segment(1); ?></h2>
                </div>
                <div class="col-12 col-md col-lg">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= site_url('/'); ?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?= $this->uri->segment(1); ?></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact -->
    <section id="contact" class="">
        <?php if($configs) : ?> 
        <!-- gmaps -->
        <div><?= $configs['gmaps']; ?></div>
        <div class="container mt-5 mb-5">
            <!-- location email and phonenumber -->
            <div class="row">
                <div class="col-sm-6 col-lg-4 mb-3">
                    <div class="card mb-3 ">
                        <div class="card-body d-flex align-items-start">
                            <div class="icon-contact">
                                <span>
                                    <i class="fas fa-map-marker-alt"></i>
                                </span>
                            </div>
                            <div class="text-contact">
                                <h5 class="card-title text-uppercase">LOKASI</h5>
                                <p class="card-text">
                                    <span><?= $configs['address']; ?></span><br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4 mb-3">
                    <div class="card mb-3 ">
                        <div class="card-body d-flex align-items-start">
                            <div class="icon-contact">
                                <span>
                                    <i class="fas fa-envelope"></i>
                                </span>
                            </div>
                            <div class="text-contact">
                                <h5 class="card-title text-uppercase">EMail</h5>
                                <p class="card-text">
                                    <span><?= $configs['email']; ?></span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4 mb-3">
                    <div class="card mb-3 ">
                        <div class="card-body d-flex align-items-start">
                            <div class="icon-contact">
                                <span>
                                    <i class="fas fa-phone"></i>
                                </span>
                            </div>
                            <div class="text-contact">
                                <h5 class="card-title text-uppercase">KONTAK</h5>
                                <p class="card-text">
                                    <span>Telp:&nbsp;<?= $configs['telephone']; ?></span><br>
                                    <span>Fax: &nbsp;<?= $configs['fax']; ?></span><span>
                                    <!-- <span>Wa: &nbsp;<?= $configs['']; ?></span><br> -->
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- form contact -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="text-center text-uppercase mb-4">Kirim pesan pada kami</h5>

                            <?= $this->session->flashdata('message'); ?>

                            <?= form_open('contact/send/'); ?>
                                <div class="form-row mb-1">
                                    <div class="form-group col-3 col-md-1">
                                        <select name="title" class="form-control">
                                            <option value="TN">TN</option>
                                            <option value="NY">NY</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-9 col-md-11">
                                        <input type="text" name="full_name" class="form-control" placeholder="Nama Lengkap (wajib diisi)*" value="<?= set_value('full_name'); ?>" autocomplete="off">
                                        <?= form_error('full_name', '<small class="text-danger">', '</small>'); ?>
                                    </div>
                                </div>
                                <div class="form-row mb-1">
                                    <div class="form-group col-12 col-md-4">
                                        <input type="text" name="company_name" class="form-control" placeholder="Nama Perusahaan (wajib diisi)*" value="<?= set_value('company_name'); ?>" autocomplete="off">
                                        <?= form_error('company_name', '<small class="text-danger">', '</small>'); ?>
                                    </div>
                                    <div class="form-group col-12 col-md-4">
                                        <input type="text" name="email" class="form-control" placeholder="Email (wajib diisi)*" value="<?= set_value('email'); ?>" autocomplete="off">
                                        <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
                                    </div>
                                    <div class="form-group col-12 col-md-4">
                                        <input type="text" name="contact_number" class="form-control" placeholder="Nomor Telpon (wajib diisi)*" value="<?= set_value('contact_number'); ?>" autocomplete="off">
                                        <?= form_error('contact_number', '<small class="text-danger">', '</small>'); ?>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <textarea class="form-control" name="company_address" rows="3" cols="10" placeholder="Alamat perusahaan (wajib diisi)*"><?= set_value('company_address'); ?></textarea>
                                    <?= form_error('company_address', '<small class="text-danger">', '</small>'); ?>
                                </div>
                                <div class="form-group mb-3">
                                    <select name="inquiry" class="form-control">
                                        <option value="">-- Pilih keterangan permintaan --</option>
                                        <option value="Customer Care">Customer Care</option>
                                        <option value="Customer Relations">Customer Relations</option>
                                        <option value="Sales Enquiry">Sales Enquiry</option>
                                        <option value="Others">Others</option>
                                    </select>
                                    <?= form_error('inquiry', '<small class="text-danger">', '</small>'); ?>
                                </div>
                                <div class="form-group mb-3">
                                    <input type="text" name="subject" class="form-control" placeholder="Subjek" value="<?= set_value('subject'); ?>">
                                </div>
                                <div class="form-group mb-2">
                                    <textarea class="form-control" name="message" rows="8" cols="10" placeholder="Ketik pesan di sini..."><?= set_value('message'); ?></textarea>
                                    <?= form_error('message', '<small class="text-danger">', '</small>'); ?>
                                </div>
                                <div class="form-group mb-2">
                                    <label class="image_captcha"><?= $captcha; ?></label>
                                    <a href="javascript:;" class="refresh btn btn-sm" style="background-color:#008899; font-size:12px; color:white; border-radius:0;"><i class="fas fa-sync-alt"></i> Reload Captcha</a>
                                </div>
                                <div class="row">
                                    <div class="col-7 col-lg-3">
                                        <div class="form-group mb-2">
                                            <input type="text" name="capt" autocomplete="off" class="form-control" placeholder="Ketikkan angka di gambar" value="<?= set_value('subject'); ?>">
                                            <?= form_error('capt', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn float-left btn-general mt-3"><i class="fas fa-fw fa-paper-plane"></i> Kirim Pesan</button>
                            <?= form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </section>
<?php else : ?>
    <section>
        <div class="container" style="min-height: 300px; ">
            <div class="row align-items-center">
                <div class="col-12 text-center">
                    <h2 class="mt-5">EMPTY!</h2>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
