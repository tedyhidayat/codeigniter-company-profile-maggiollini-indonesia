<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_abouts_model extends CI_Model {

	// Function get all data category about
	public function getAllCategoryAbouts() {
		return $this->db->get('tbl_category_abouts')->result_array();
	}
	
	// Function get data category about by ID
	public function getCategoryAboutsById($id) {
		return $this->db->get_where('tbl_category_abouts', ['id' => $id])->row_array();
	}

	// function create data category about
	public function create($data) {	
		// get data from input form 
		$data = [
			'position'  => $this->input->post('position', true),
			'name' 		=> $this->input->post('categName', true),
			'slug' 		=> url_title($this->input->post('categName', true), 'dash', true),
		];
		// query insert
		$this->db->insert('tbl_category_abouts', $data);
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to create data!</div>');
			redirect('admin/category_abouts/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Success!</div>');
			redirect('admin/category_abouts/');
		}
		
	}

	// Function edit data category about
	public function edit($data) {
		// get data from input form
		$position 	= $this->input->post('position', true);
		$name 		= $this->input->post('categName', true);
		$slug 		= url_title($this->input->post('categName', true), 'dash', true);
		// query update
		$this->db->set('name', $name);
		$this->db->set('slug', $slug);
		$this->db->set('position', $position);
		$this->db->where('id', $data['id']);
		$this->db->update('tbl_category_abouts');
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to edit data!</div>');
			redirect('admin/category_abouts/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your category has been updated!</div>');
			redirect('admin/category_abouts/');
		}
		
	}

	// query delete data category about
	public function delete($data) {
		// query delete
		$this->db->where('id', $data['id']);
		$this->db->delete('tbl_category_abouts');
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to delete data!</div>');
			redirect('admin/category_abouts/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your category has been delete!</div>');
			redirect('admin/category_abouts/');
		}
		
	}

}

/* End of file Category_abouts_model.php */
/* Location: ./application/models/Category_abouts_model.php */