<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
// default redirect page

global $SConfig;

$route['default_controller']                = 'home';
$route['admin/dashboard']                   = "admin/dashboard";
$route['404_override']                      = 'errors/error404';
$route['translate_uri_dashes']              = FALSE;
$route['home']                              = 'home';
$route['about']                             = 'about';
$route['contact']                           = 'contact';
$route['archives']                          = 'archives';
$route['services']                          = 'services/index';
$route['services/detail/(:any)']            = 'services/detail/';
$route['careers']                           = 'careers/index';
$route['careers/detail/']                   = 'careers/';
$route['news']                              = 'news/index';
$route['news/post/(:any)']                  = 'news/post/';
$route['news/category/(:any)']              = 'news/category/';
$route['sitemap']                           = 'sitemap/list';
$route['projects']                          = 'projects/finished-projects';
$route['projects/finished-projects']        = 'projects/finished-projects';
$route['projects/detail-project/(:any)']    = 'projects/detail-project/';
$route['projects/sample']                   = 'projects/sample';
$route['projects/detail-sample/(:any)']     = 'projects/detail-sample/';
$route['sitemap/list/(:any)']               = 'errors/error404';
$route[$SConfig->_url_unlock]               = 'login/login_redirect';

