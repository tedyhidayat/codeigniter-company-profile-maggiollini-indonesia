<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	// function get all data users
	public function getAllUser() {
		return $this->db->get('tbl_users')->result_array();
	}

	// function get user by ID
	public function getUserById($id) {
		return $this->db->get_where('tbl_users', ['id' => $id])->row_array();
	}

	// function get user by SESSION when login
	public function getUserBySession() {
		return $this->db->get_where('tbl_users', ['id' => $this->session->userdata('id')])->row_array();
	}

	/**
	 * ===================================================================================================
	 * users control model for superadmin ================================================================
	 * ===================================================================================================
	 */

	//  function creating new user admin
	public function create() {
		// get data from input form
		$data = [
			'login' 			=> 0,
			'status' 			=> 0,
			'level' 			=> 'admin',
			'profile_picture' 	=> 'default.png',
			'email' 			=> htmlspecialchars($this->input->post('email', true)),
			'username' 			=> htmlspecialchars($this->input->post('username', true)),
			'full_name' 		=> htmlspecialchars($this->input->post('full_name', true)),
			'password' 			=> password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
		];
		// query insert data
		$this->db->insert('tbl_users', $data);
		// check if there affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-success shadow" role="alert> Failed to creating new user!</div>');
			redirect('admin/user/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success shadow" role="alert">'. $data['full_name'] . ' has been added!</div>');
			redirect('admin/user/');
		}
		
	}

	// function edit user
	public function edit($data) {
		// get data from input form
		$email		= $this->input->post('email', true);
		$status 	= $this->input->post('status', true);
		$gender 	= $this->input->post('gender', true);
		$status 	= $this->input->post('status', true);
		$username	= $this->input->post('username', true);
		$fullName	= $this->input->post('full_name', true);
		$upload_image 	= $_FILES['profile_picture']['name'];
		// check if there uploaded file
		if ($upload_image) {
			// define config uploaded fiole
			$config['max_size']     	= 700;
			$config['file_ext_tolower'] = TRUE;
			$config['allowed_types'] 	= 'jpg|png';
			$config['upload_path'] 		= './assets/images/user/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-profile-'.$fullName;
			// initializing config uploaded file
			$this->load->library('upload', $config);
			if ($this->upload->do_upload('profile_picture')) {
				// if success
				// get old file
				$old_image = $data['userId']['profile_picture'];
				// delte old data
				if ($old_image != 'default.png') {
					unlink(FCPATH . 'assets/images/user/' . $old_image);
				}
				// set new file to database
				$this->db->set('profile_picture', $this->upload->data('file_name'));
			} else {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . '</div>');
				redirect('admin/user/edit/' . $data['id']);
			}
		}
		// query update
		$this->db->set('email', $email);
		$this->db->set('status', $status);
		$this->db->set('gender', $gender);
		$this->db->set('username', $username);
		$this->db->set('full_name', $fullName);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $data['id']);
		$this->db->update('tbl_users');
		// check if there data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> User failed to update!</div>');
			redirect('admin/user/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> User has been updated!</div>');
			redirect('admin/user/');
		}
	}
	
	public function delete($data) {
		if ($data['id']) {
			$old_image = $data['user']['profile_picture'];
			if ($old_image) {
				unlink(FCPATH . 'assets/images/user/' . $old_image);
			}
			$this->db->where('id', $data['id']);
			$this->db->delete('tbl_users');
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Success delete '. $data['user']['full_name'] .'</div>');
			redirect('admin/user/');
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to delete data!</div>');
			redirect('admin/user/');
		}
		
	}

	/**
	 * ===================================================================================================
	 * users control model public ========================================================================
	 * ===================================================================================================
	 */

	//  function edit profile user
	public function editProfile($data) {
		// get data from input form
		$id 			= $this->input->post('id');
		$email 			= $this->input->post('email');
		$gender 		= $this->input->post('gender');
		$fullName 		= $this->input->post('full_name');
		$upload_image 	= $_FILES['image']['name'];
		// check if there uploaded file
		if ($upload_image) {
			// define config uploaded file
			$config['max_size']     	= 700;
			$config['allowed_types'] 	= 'jpg|png';
			$config['upload_path'] 		= './assets/images/user/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-profile-'.$fullName;
			// initializing config uploaded file
			$this->load->library('upload', $config);
			if ($this->upload->do_upload('image')) {
				// get old file and delete if file not same default.png
				$old_image = $data['user']['profile_picture'];
				if ($old_image != 'default.png') {
					unlink(FCPATH . 'assets/images/user/' . $old_image);
				}
				// set new file to databse
				$this->db->set('profile_picture', $this->upload->data('file_name'));
			} else {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . '</div>');
				redirect('user/editProfile');
			}
		}
		// query update
		$this->db->set('email', $email);
		$this->db->set('gender', $gender);
		$this->db->set('full_name', $fullName);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $id);
		$this->db->update('tbl_users');
		// check if there data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your profile failed to update!</div>');
			redirect('admin/user/profile/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your profile has been updated!</div>');
			redirect('admin/user/profile/');
		}
		
	}

	// function change password
	public function changePassword($data) {
		// get data from input form
		$id 				= $this->input->post('id');
		$current_password 	= $this->input->post('oldPassword');
		$new_password 		= $this->input->post('newPassword');
		// check if old password same with input current password 
		if (!password_verify($current_password, $data['user']['password'])) {
			// if not same redirect and return display error
			$this->session->set_flashdata('message', '<div class="alert alert-danger">Wrong current password !</div>');
			redirect('admin/user/profile/');
		} else {
			// if same, next proccess
			// check if new password same with current password
			if ($current_password == $new_password) {
				// if same, return display error password cannot be the same with current password
				$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>New password cannot be the same as current password !</div>');
				redirect('admin/user/profile/');
			} else {
				// if not same, next proccess
				// checking password success
				// generate password hash
				$password_hash = password_hash($new_password, PASSWORD_DEFAULT);
				// query update
				$this->db->set('password', $password_hash);
				$this->db->set('updated_at', date('Y-m-d H:i:s'));
				$this->db->where('id', $id);
				$this->db->update('tbl_users');
				// check if there data affected rows
				if ($this->db->affected_rows() < 0) {
					// if failed
					$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed to change password!</div>');
					redirect('admin/user/profile/');
				} else {
					// if success
					$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your Password has been changed!</div>');
					redirect('admin/user/profile/');
				}
			}
		}
	}

}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */