<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Careers extends CI_Controller {
	
	// function construct to define models
	public function __construct() {
		parent::__construct();
		security_access();
		$this->load->model('User_model');
		$this->load->model('Careers_model');
	}

	// function show index
    public function index() {
		// get data from models
		$data = [
			'title' 	=> 'Careers',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'careers' 	=> $this->Careers_model->getAllCareers(),
		];
		// view index careers page
        $this->templating->load('layouts/admin/wrapper', 'admin/careers/index', $data);
	}
	
	// function Create about page
	public function create() {
		// get data from models
		$data = [
			'title' 	=> 'Add Career',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
		];
		// validatiuon rules
		$this->form_validation->set_rules('image', 'image', 'trim');
		$this->form_validation->set_rules('status', 'status', 'required');
		$this->form_validation->set_rules('keywords', 'keywords', 'trim');
		$this->form_validation->set_rules('title', 'title', 'trim|required');
		$this->form_validation->set_rules('position', 'position', 'required');
		$this->form_validation->set_rules('salary', 'salary', 'required|trim');
		$this->form_validation->set_rules('description', 'description', 'required|trim');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/careers/create', $data);
		} else {
			// if validation success call model create from Careers_model
			$this->Careers_model->create($data);
		}
	}

	// function Edit about page
	public function edit($id) {
		// get data from models
		$data = [
			'title' => 'Edit Career',
			'user' => $this->User_model->getUserBySession(),
			'career' => $this->Careers_model->getCareersById($id),
			'config' => $this->configs->getConfiguration(),
			'id' => $id
		];
		// validatiuon rules
		$this->form_validation->set_rules('image', 'Image', 'trim');
		$this->form_validation->set_rules('keywords', 'keywords', 'trim');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('position', 'position', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required|trim');
		$this->form_validation->set_rules('salary', 'salary', 'required|trim');
		$this->form_validation->set_rules('description', 'Description', 'trim');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/careers/update', $data);
		} else {
			// if validation success call model edit from Careers_model
			$this->Careers_model->edit($data);
		}
	}

	// function delete about page
	public function delete($id) {
		// get data from models
		$data = ['id' => $id, 'career' => $this->Careers_model->getCareersById($id)];
		// call functio delete from model Careers_model
		$this->Careers_model->delete($data);
	}
	
}

/* End of file Careers.php */

?>
