<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Abouts extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		security_access();
		$this->load->model('User_model');
		$this->load->model('Abouts_model');
		$this->load->model('Category_abouts_model');
	}

	// function show index
    public function index() {
		// get data from models
		$data = [
			'title' 	=> 'Abouts',
			'config' 	=> $this->configs->getConfiguration(),
			'abouts' 	=> $this->Abouts_model->getAllAbouts(),
			'user' 		=> $this->User_model->getUserBySession(),
		];
		// view index about page
        $this->templating->load('layouts/admin/wrapper', 'admin/abouts/index', $data);
	}
	
	// function create about page
	public function create() {
		// get data from models
		$data = [
			'title' 		=> 'Add Abouts',
			'config' 		=> $this->configs->getConfiguration(),
			'user' 			=> $this->User_model->getUserBySession(),
			'categories' 	=> $this->Category_abouts_model->getAllCategoryAbouts(),
		];
		// validatiuon rules
		$this->form_validation->set_rules('file', 'file', 'trim');
		$this->form_validation->set_rules('status', 'status', 'required');
		$this->form_validation->set_rules('keywords', 'keywords', 'trim');
		$this->form_validation->set_rules('title', 'title', 'trim|required');
		$this->form_validation->set_rules('categId', 'category', 'trim|required');
		$this->form_validation->set_rules('description', 'description', 'required|trim');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/abouts/create', $data);
		} else {
			// if validation success call model create from About_model
			$this->Abouts_model->create($data);
		}
	}

	// function Edit about page
	public function edit($id) {
		// get data from models
		$data = [
			'id' 			=> $id,
			'title' 		=> 'Edit Abouts',
			'config' 		=> $this->configs->getConfiguration(),
			'user'			=> $this->User_model->getUserBySession(),
			'about' 		=> $this->Abouts_model->getAboutsById($id),
			'categories' 	=> $this->Category_abouts_model->getAllCategoryAbouts(),
		];
		// validatiuon rules
		$this->form_validation->set_rules('image', 'image', 'trim');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('keywords', 'keywords', 'trim');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('categId', 'category', 'trim|required');
		$this->form_validation->set_rules('description', 'Description', 'required|trim');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/abouts/update', $data);
		} else {
			// if validation success call model edit from About_model
			$this->Abouts_model->edit($data);
		}
	}

	// function Delete about page
	public function delete($id) {
		// get data from models
		$data = ['id' => $id, 'about' => $this->Abouts_model->getAboutsById($id)];
		// call function delete from model
		$this->Abouts_model->delete($data);
	}

}

/* End of file Abouts.php */

?>