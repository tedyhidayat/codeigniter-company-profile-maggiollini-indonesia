    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $title; ?>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <?= $this->session->flashdata('message'); ?>

        <div class="row">
            <div class="col-xs-12">
                <?php if (!$allConfig) : ?>
                <div class="box box-danger with-border">
                    <?php elseif($allConfig) : ?>
                    <div class="box box-success">
                        <?php endif; ?>
                        <div class="box-header with-border">
                            <?php if (!$allConfig) : ?>
                                <a href="<?= site_url('admin/configurations/create/'); ?>"
                                class="btn btn-success btn-sm btn-flat"><i class="fa fa-plus"></i> Create Config</a>
                            <?php elseif($allConfig) : ?>
                                <?php foreach($allConfig as $row) : ?>
                                    <a href="<?= site_url('admin/configurations/edit/' . $row['id']); ?>" class="btn btn-info btn-sm btn-flat"><i class="fa fa-pencil"></i> Edit Site Configuration</a>
                                   <?php include('reset.php'); ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                        <!-- /.box-header -->
                        <?php foreach($allConfig as $row) : ?>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12 col-md-2">
                                    <h4>Logo :</h4>
                                    <?php if($row['logo']) : ?>
                                    <img src="<?= base_url('assets/images/configurations/' . $row['logo']); ?>"
                                        width="100" class="img-thumbnail">
                                    <?php else : ?>
                                    <p>N/A</p>
                                    <?php endif; ?>

                                    <h4>Icon :</h4>
                                    <?php if($row['icon']) : ?>
                                    <img src="<?= base_url('assets/images/configurations/' . $row['icon']); ?>"
                                        width="100" class="img-thumbnail">
                                    <?php else : ?>
                                    <p>N/A</p>
                                    <?php endif; ?>
                                </div>
                                <div class="col-xs-12 col-md-10">
                                    <!-- Widget: user widget style 1 -->
                                    <div class="table-responsive">
                                        <table class="table table-condensed">
                                            <tbody>
                                                <tr>
                                                    <th>Last Update</th>
                                                    <td><?= date('l, d/m/Y | H:i', strtotime($row['updated_at'])); ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Site Name</th>
                                                    <td><?= $row['site_name']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Site URL</th>
                                                    <td><?= $row['site_url']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Keywords</th>
                                                    <td><?= $row['keywords']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Tagline</th>
                                                    <td><?= $row['tagline']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Telephone</th>
                                                    <td><?= $row['telephone']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>WhatsApp</th>
                                                    <td><?= $row['whatsapp']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>FAX</th>
                                                    <td><?= $row['fax']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Email</th>
                                                    <td><?= $row['email']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Instagram</th>
                                                    <td><?= $row['instagram']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Facebook</th>
                                                    <td><?= $row['facebook']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Twitter</th>
                                                    <td><?= $row['twitter']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Google Maps</th>
                                                    <td><?= $row['gmaps']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Address</th>
                                                    <td><?= $row['address']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Meta Text</th>
                                                    <td><?= $row['meta_text']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Meta Description</th>
                                                    <td><?= $row['meta_desc']; ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.widget-user -->
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>

                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
    </section>
