<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?= $title; ?></h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- flash data -->
    <?= $this->session->flashdata('message'); ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="<?= site_url('admin/partners/create'); ?>" enctype="multipart/form-data"
                    role="form">
                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" class="form-control" value="<?= $this->security->get_csrf_hash(); ?>" >
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12 col-md-8">
                                <div class="form-group">
                                    <label for="partnerName">Partner Name</label>
                                    <input type="text" name="partnerName" class="form-control" id="partnerName"
                                        placeholder="Partner Name" value="<?= set_value('partnerName'); ?>">
                                    <?= form_error('partnerName', '<small class="text-danger">', '</small>'); ?>
                                </div>    
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="Pending">Pending</option>
                                        <option value="Publish">Publish</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label for="deputy">Deputy</label>
                                    <input type="text" name="deputy" class="form-control" id="deputy" placeholder="Deputy"
                                        value="<?= set_value('deputy'); ?>">
                                    <?= form_error('deputy', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" name="email" class="form-control" id="email"
                                        placeholder="example@example.com" value="<?= set_value('email'); ?>">
                                    <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="form-group">
                                    <label for="phoneNumber">Phone Number</label>
                                    <input type="text" name="phoneNumber" class="form-control" id="phoneNumber"
                                        placeholder="Phone Number" value="<?= set_value('phoneNumber'); ?>">
                                    <?= form_error('phoneNumber', '<small class="text-danger">', '</small>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="image">Image</label>
                            <input type="file" name="image" class="form-control" id="image">
                            <small class="text-info">* Max size 1 Mb</small><br>
                            <small class="text-info">* Only for jpg | jpeg | png</small>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success btn-flat pull-right">Save</button>
                        <a href="<?= site_url('admin/partners/'); ?>" class="btn pull-right">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>