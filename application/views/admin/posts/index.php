    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= $title; ?></h1>
    </section> 
    <!-- Main content -->
    <section class="content">
        <!-- flash data -->
        <?= $this->session->flashdata('message'); ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success">
                    <div class="box-header">
                        <a href="<?= site_url('admin/posts/create'); ?>" class="btn btn-success btn-sm btn-flat"><i class="fa fa-plus"></i> Create New</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example2" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="12">NO.</th>
                                        <th>Images</th>
                                        <th>Title</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                        <th width="100">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!$allPosts) : ?>
                                    <tr>
                                        <td class="text-center" colspan="8">Empty!</td>
                                    </tr>
                                    <?php else : ?>
                                    <?php $no = 1; foreach ($allPosts as $row): ?>
                                    <tr>
                                        <td><?= $no++; ?>.</td>
                                        <td>
                                        <?php if($row['image']) : ?>
                                            <img width="70" class="img-thumbnail" src='<?= base_url('assets/images/Posts/thumbs/' . $row['image']); ?>'>
                                        <?php else : ?>
                                            N/A
                                        <?php endif; ?>
                                        </td>
                                        <td>
                                            <b><?= $row['title']; ?></b><br>
                                            <small class="text-blue">
                                                <i class="fa fa-user"></i>
                                                <?= $row['full_name']; ?>
                                                &nbsp;&nbsp;
                                                <i class="fa fa-clock-o"></i>
                                                <?= date("d F Y", strtotime($row['created_at'])); ?>
                                                &nbsp;&nbsp;
                                            </small>
                                            <br>
                                            <small class="text-blue">
                                                <i class="fa fa-globe"></i>
                                                <a href="<?= site_url('Posts/'.$row['slug']); ?>">
                                                <?= site_url('Posts/'.$row['slug']); ?>
                                                </a>
                                            </small>
                                        </td>
                                        <td><?= $row['type']; ?></td>
                                        <td>
                                            <?php if($row['status'] == 'Pending') : ?>
                                            <span class="btn btn-xs btn-warning btn-flat">Pending</span>
                                            <?php elseif($row['status'] == 'Publish') : ?>
                                            <span class="btn btn-xs btn-success btn-flat">Publish</span>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <a href="<?= site_url('admin/posts/edit/' . $row['id']); ?>"
                                                class="btn btn-info btn-sm btn-flat"><i class="fa fa-pencil"></i></a>
                                            <?php include('detail.php') ?>
                                            <?php include('delete.php') ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>



    </section>