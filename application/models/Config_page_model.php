<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config_page_model extends CI_Model {

	// function get all data config page
	public function getAllConfigPage() {
		/**
		 * tbl_gallery join tbl_category_gallery and tbl_users
		 * tbl_users: get full name
		 */
		$this->db->select('tbl_config_page.*, tbl_users.full_name' );
		$this->db->from('tbl_config_page');
		$this->db->join('tbl_users','tbl_users.id = tbl_config_page.id_user', 'LEFT');
		$this->db->order_by('tbl_config_page.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	// function get all data config page
	public function getAllConfigPageActiveHeading() {
		/**
		 * tbl_gallery join tbl_category_gallery and tbl_users
		 * tbl_users: get full name
		 */
		$this->db->select('tbl_config_page.*, tbl_users.full_name' );
		$this->db->from('tbl_config_page');
		$this->db->join('tbl_users','tbl_users.id = tbl_config_page.id_user', 'LEFT');
		$this->db->where(['tbl_config_page.status' => 'Publish', 'tbl_config_page.type' => 'Heading']);
		$this->db->order_by('tbl_config_page.id','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	// function get gallery by ID
	public function getConfigPageById($id) {
		return $this->db->get_where('tbl_config_page', ['id' => $id])->row_array();
	}

	// function create data gallery
	public function create($data) {	
		// get file from input form
		$upload_image = $_FILES['image']['name'];
		// check uploaded file
		if ($upload_image) {
			// define config upload file
			$config['max_size']     	= 2048;
			$config['allowed_types'] 	= 'jpg|jpeg|png';
			$config['upload_path'] 		= './assets/images/configurations/config_page/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-page-';
			// initializing config upload fileS
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('image')) {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . '</div>');
				redirect('admin/config-page/create/');
			} else {
				// if success
				// define creating thumbnail
				$config['thumb_marker']    	= '';
				$config['image_library'] 	= 'gd2';
				$config['create_thumb'] 	= TRUE;
				$config['maintain_ratio'] 	= TRUE;
				$config['width']         	= 500;
				$config['height']       	= 470;
				$config['source_image']	 	= './assets/images/configurations/config_page/'.$this->upload->data('file_name');
				$config['new_image']	 	= './assets/images/configurations/config_page/thumbs/'.$this->upload->data('file_name');
				// initializing config thumbnail
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				// collacting innsert data to table
				$data = [
					'id_user' 		=> $data['user']['id'],
					'type' 			=> $this->input->post('type', true),
					'image' 		=> $this->upload->data('file_name'),
					'title' 		=> $this->input->post('title', true),
					'status' 		=> $this->input->post('status', true),
					'description'	=> $this->input->post('description', true),
					'page_position'	=> $this->input->post('page_position', true),
				];
			}
		} else {
			// collacting innsert data to table
			$data = [
				'id_user' 		=> $data['user']['id'],
				'type' 			=> $this->input->post('type', true),
				'title' 		=> $this->input->post('title', true),
				'status' 		=> $this->input->post('status', true),
				'description'	=> $this->input->post('description', true),
				'page_position'	=> $this->input->post('page_position', true),
			];
		}
		// query insert
		$this->db->insert('tbl_config_page', $data);
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Failed insert data!</div>');
			redirect('admin/config-page/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Success!</div>');
			redirect('admin/config-page/');
		}
		
	}

	// function edit galery
	public function edit($data) {
		// get data from input form
		$title 			= $this->input->post('title', true);
		$status 		= $this->input->post('status', true);
		$description 	= $this->input->post('description', true);
		$upload_image 	= $_FILES['image']['name'];
		if ($upload_image) {
			// define uploaded file config
			$config['max_size']     	= 2048;
			$config['allowed_types'] 	= 'jpeg|jpg|png';
			$config['upload_path'] 		= './assets/images/configurations/config_page/';
			// format name (company name, date, page)
			$config['file_name'] 		= url_title($data['config']['siteName'], 'dash', true).'-'.date('d-m-Y').'-page-';
			// initializing config file
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('image')) {
				// if failed
				$this->session->set_flashdata('message', '<div class="alert alert-danger">' . $this->upload->display_errors() . ' You only can upload '. $config['allowed_types'] . ' type.' . '</div>');
				redirect('admin/config-page/edit/'. $data['id']);
			} else {
				// if success
				// get old file
				$old_image = $data['config_page']['image'];
				// unlink old file
				if ($old_image != 'default.png') {
					unlink(FCPATH . 'assets/images/configurations/config_page/' . $old_image);
					unlink(FCPATH . 'assets/images/configurations/config_page/thumbs/' . $old_image);
				}
				// define uploade file
				$new_file = $this->upload->data('file_name');
				$config['image_library'] 	= 'gd2';
				$config['create_thumb'] 	= TRUE;
				$config['maintain_ratio'] 	= TRUE;
				$config['width']         	= 500;
				$config['height']       	= 470;
				$config['thumb_marker']    	= '';
				$config['source_image']	 	= './assets/images/configurations/config_page/'.$new_file;
				$config['new_image']	 	= './assets/images/configurations/config_page/thumbs/'.$new_file;
				// initializing file upload
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				// set data file upload to table
				$this->db->set('image', $new_file);
			}
		}
		// query update
		$this->db->set('title', $title);
		$this->db->set('status', $status);
		$this->db->set('description', $description);
		$this->db->where('id', $data['id']);
		$this->db->update('tbl_config_page');
		// check if data affected rows
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $title . ' failed to edit!</div>');
			redirect('admin/config-page/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Your file ' . $title . ' has been updated!</div>');
			redirect('admin/config-page/');
		}
		
	}

	// function delete gallery
	public function delete($data) {
		// get old file
		$old_image= $data['config_page']['image'];
		// unlink old file
		if ($old_image) {
			unlink(FCPATH . 'assets/images/configurations/config_page/' . $old_image);
			unlink(FCPATH . 'assets/images/configurations/config_page/thumbs/' . $old_image);
		}
		// query delete
		$this->db->where('id', $data['id']);
		$this->db->delete('tbl_config_page');
		// check if data affected rows'
		if ($this->db->affected_rows() < 0) {
			// if failed
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $data['config_page']['title'] .  '  failed to delete!</div>');
			redirect('admin/config-page/');
		} else {
			// if success
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' . $data['config_page']['title'] .  '  has been delete!</div>');
			redirect('admin/config-page/');
		}
		
	}
}

/* End of file Config_page.php */
/* Location: ./application/models/Config_page.php */