 <!-- Content Header (Page header) -->
 <section class="content-header">
     <h1><?= $title; ?></h1>
 </section>
 <!-- Main content -->
 <section class="content">
     <!-- flash data -->
     <?= $this->session->flashdata('message'); ?>
     <div class="row">
         <div class="col-xs-12 col-md-5">
             <div class="box box-primary">
                 <!-- /.box-header -->
                 <!-- form start -->
                 <form method="post" action="<?= site_url('admin/category_sample/edit/' . $category['id']); ?>"
                     enctype="multipart/form-data" role="form">
                     <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" class="form-control" value="<?= $this->security->get_csrf_hash(); ?>" >
                     <div class="box-body">
                         <input type="hidden" name="id" value="<?= $category['id']; ?>">
                         <div class="form-group">
                             <label for="categName">Category Name</label>
                             <input type="text" name="categName" class="form-control" id="categName"
                                 placeholder="Category Name" value="<?= $category['name']; ?>">
                             <?= form_error('categName', '<small class="text-danger">', '</small>'); ?>
                         </div>
                         <div class="form-group">
                             <label for="position">Position</label>
                             <input type="number" name="position" min="1" class="form-control" id="position"
                                 placeholder="Insert position" value="<?= $category['position']; ?>">
                             <?= form_error('position', '<small class="text-danger">', '</small>'); ?>
                         </div>
                     </div>
                     <!-- /.box-body -->
                     <div class="box-footer">
                         <button type="submit" class="btn btn-success btn-flat pull-right">Edit</button>
                         <a href="<?= site_url('admin/category_sample/'); ?>" class="btn pull-right">Cancel</a>
                     </div>
                 </form>
             </div>
         </div>
     </div>
 </section>
