  <!-- Content Header (Page header) -->
  <section class="content-header">
      <h1><?= $title; ?></h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- flash data -->
    <?= $this->session->flashdata('message'); ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-success">
                <!-- box-header -->
                <div class="box-header">
                    <a href="<?= site_url('admin/services/create'); ?>" class="btn btn-success btn-sm btn-flat"><i class="fa fa-plus"></i> Create New</a>
                </div>
                <!-- /.box-header -->
                <!-- box-body -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="50">NO.</th>
                                    <th>Images</th>
                                    <th>Services Name</th>
                                    <th class="text-center">Status</th>
                                    <th width="100">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!$services) : ?>
                                <tr>
                                    <td class="text-center" colspan="6">Empty!</td>
                                </tr>
                                <?php else : ?>
                                <?php $no = 1; foreach ($services as $row): ?>
                                <tr>
                                    <td><?= $no++; ?>.</td>
                                    <td><img width="70" class="img-thumbnail"
                                            src="<?= base_url('assets/images/services/' . $row['image']); ?>"
                                            alt="">
                                    </td>
                                    <td>
                                        <b><?= $row['title']; ?></b><br>
                                        <small class="text-blue">
                                            Created at :
                                            <?= date("d F Y", strtotime($row['created_at'])); ?>
                                        </small>
                                        <br>
                                        <small class="text-blue">
                                            Created by :
                                            <?= $row['full_name']; ?>
                                        </small>
                                    </td>
                                    <td class="text-center">
                                        <?php if($row['status'] == 'Pending') : ?>
                                        <span class="btn btn-xs btn-warning btn-flat">Pending</span>
                                        <?php elseif($row['status'] == 'Publish') : ?>
                                        <span class="btn btn-xs btn-success btn-flat">Publish</span>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <a href="<?= site_url('admin/services/edit/' . $row['id']); ?>"
                                            class="btn btn-info btn-sm btn-flat"><i class="fa fa-pencil"></i></a>
                                        <?php include('detail.php') ?>
                                        <?php include('delete.php') ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>