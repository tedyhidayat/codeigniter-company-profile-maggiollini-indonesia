<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		create_cookie();
		$this->load->model('Posts_model');
		$this->load->model('Categories_model');
	}

	// function show News 
	public function index()	{
		// unset sessinon captcha if there
		$this->session->unset_userdata('admin_access');
		$this->session->unset_userdata('captchaImage');
		// identification
		$configurations = $this->configs->getConfiguration();

		// pagination News
		$this->load->library('pagination');

		$config['base_url'] = base_url('news/index/');
		$config['total_rows'] = count($this->Posts_model->total());
		$config['per_page'] = 3;
		$config['uri_segment'] = 3;
		$config['prev_link'] = 'Prev';
		$config['next_link'] = 'Next';

		$limit = $config['per_page'];
		$start = ($this->uri->segment(3)) ? ($this->uri->segment(3)) : 0;
		
		$this->pagination->initialize($config);

		$category 		= $this->Categories_model->getAllCategories();
		$posts 			= $this->Posts_model->getAllPostActive($limit, $start);
		$recent_post 	= $this->Posts_model->getRecentPost();

		// check config for title
		(!empty($configurations['siteName'])) ? $title1 = $this->uri->segment(1) : $title1 = "THDEV";
		(!empty($configurations['siteName'])) ? $title2 = $configurations['siteName'] : $title2 = "Web Developer";


		$data = [
			'limit'			=> $limit,
			'posts' 		=> $posts,
			'category'		=> $category,
			'recent'		=> $recent_post,
			'total'			=> $config['total_rows'],
			'pagination' 	=> $this->pagination->create_links(),
			'configs' 		=> $this->configs->getConfiguration(),
			'config_page' 	=> $this->config_page->getAllConfigPageActiveHeading(),
			'title' 		=> $title1.' | '.$title2,
        ];
		
		$this->templating->load('layouts/public/wrapper', 'news', $data);
	}

	// function detail News
	public function post()	{
		// identification
		$configurations = $this->configs->getConfiguration();
		$slug 			= $this->uri->segment(3);
		$post 			= $this->Posts_model->getPostBySlug($slug);
		$recent_post 	= $this->Posts_model->getRecentPost();
		$category 		= $this->Categories_model->getAllCategories();

		// check config for title
		(!empty($configurations['siteName'])) ? $title1 = $this->uri->segment(1) : $title1 = "THDEV";
		(!empty($post['title'])) ? $title2 = $post['title'] : $title2 = "Web Developer";

		$data = [
			'detail'  => $post,
			'recent'  => $recent_post,
			'category'=> $category,
			'configs' => $this->configs->getConfiguration(),
			'title'   => $this->uri->segment(1).' | '. $post['title'],
        ];
        
        $this->templating->load('layouts/public/wrapper', 'news_detail', $data);
	}

	public function category() {
		// identification
		$configurations = $this->configs->getConfiguration();
		$slug = $this->uri->segment(3);
		$post = $this->Posts_model->getPostByCategory($slug);
		$recent_post = $this->Posts_model->getRecentPost();
		$category 		= $this->Categories_model->getAllCategories();

		// check config for title
		(!empty($configurations['siteName'])) ? $title1 = $this->uri->segment(1) : $title1 = "THDEV";
		(!empty($configurations['siteName'])) ? $title2 = $configurations['siteName'] : $title2 = "Web Developer";

		$data = [
			'post_category'  => $post,
			'recent'  		 => $recent_post,
			'category'		 => $category,
			'configs' 		 => $this->configs->getConfiguration(),
			'title' 		=> $title1.' | '.$title2,
        ];
        
        $this->templating->load('layouts/public/wrapper', 'news_category', $data);
	}

	// function search News
	public function search(){
		// identification
		$configurations = $this->configs->getConfiguration();
		$recent_post = $this->Posts_model->getRecentPost();
		$keywords = htmlspecialchars($this->input->post('keywords', TRUE));
		$category 		= $this->Categories_model->getAllCategories();

		// check config for title
		(!empty($configurations['siteName'])) ? $title1 = $this->uri->segment(1) : $title1 = "THDEV";
		(!empty($configurations['siteName'])) ? $title2 = $configurations['siteName'] : $title2 = "Web Developer";

		$data = [
			'news'    => $this->Posts_model->search($keywords),
			'configs' => $this->configs->getConfiguration(),
			'keyword' => $keywords,
			'recent'  => $recent_post,
			'category'		 => $category,
			'title' 		=> $title1.' | '.$title2,
        ];
		
		if($keywords) {
			$this->templating->load('layouts/public/wrapper', 'news_search', $data);
		} else {
			redirect('news','refresh');
		}
	}
}

/* End of file News.php */
/* Location: ./application/controllers/Index.php */