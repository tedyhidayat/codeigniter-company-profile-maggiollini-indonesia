<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		security_access();
		$this->load->model('User_model');
		$this->load->model('Posts_model');
		$this->load->model('Categories_model');
	}

	// function show index Posts page
    public function index() {
		// get data from models
		$data = [
			'title' 	=> 'Posts',
			'allPosts' 	=> $this->Posts_model->getAllPosts(),
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
		];
		// view index Posts page
        $this->templating->load('layouts/admin/wrapper', 'admin/posts/index', $data);
	}
	
	// function create index Posts page
	public function create() {
		// get data from models
		$data = [
			'title' 	=> 'Add Post',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
			'categories'=> $this->Categories_model->getAllCategories(),
		];
		// validation rules
		$this->form_validation->set_rules('image', 'Image', 'trim');
		$this->form_validation->set_rules('type', 'type', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('keywords', 'Keywords', 'required');
		$this->form_validation->set_rules('categId', 'category', 'trim|required');
		$this->form_validation->set_rules('description', 'Description', 'required|trim');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/posts/create', $data);
		} else {
			// if validation success call function create from Posts_model
			$this->Posts_model->create($data);
		}
	}

	// function show index Posts page
	public function edit($id) {
		// get data from models
		$data = [
			'id' 		=> $id,
			'title' 	=> 'Edit Post',
			'config' 	=> $this->configs->getConfiguration(),
			'post'		=> $this->Posts_model->getPostsById($id),
			'user' 		=> $this->User_model->getUserBySession(),
			'categories'=> $this->Categories_model->getAllCategories(),
		];
		// validation rules
		$this->form_validation->set_rules('image', 'Image', 'trim');
		$this->form_validation->set_rules('type', 'type', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('keywords', 'Keywords', 'required');
		$this->form_validation->set_rules('categId', 'category', 'trim|required');
		$this->form_validation->set_rules('description', 'Description', 'required|trim');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/posts/update', $data);
		} else {
			// if validation success call function edit from Posts_model
			$this->Posts_model->edit($data);
		}
	}

	// function delete Posts page
	public function delete($id) {
		// get data from models
		$data = ['id' => $id,'post' => $this->Posts_model->getPostsById($id)];
		// call function delte from Posts_model
		$this->Posts_model->delete($data);
	}
}

/* End of file Posts.php */

?>
