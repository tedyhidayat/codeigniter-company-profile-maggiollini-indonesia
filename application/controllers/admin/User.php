<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	// function construct to define models
	public function __construct() {
		parent::__construct();
		security_access();
		$this->load->model('User_model');
	}

	// function show index user page
	public function index() {
		// get data from models
		$data = [
			'title' => 'Users',
			'users' => $this->User_model->getAllUser(),
			'config'=> $this->configs->getConfiguration(),
			'user' 	=> $this->User_model->getUserBySession()
		];
		// view index page user
		$this->templating->load('layouts/admin/wrapper', 'admin/user/index', $data);
	}

	/*
		========================================================================================================
		Controller for superadmin ============================================================================== 
		========================================================================================================
	*/
	
	// function create user page
	public function create() {
		// get data from models
		$data = [
			'title' =>  'Creat Users',
			'users' =>  $this->User_model->getAllUser(),
			'config'=> $this->configs->getConfiguration(),
			'user' 	=>  $this->User_model->getUserBySession(),
		];
		// validation rules
		$this->form_validation->set_rules('full_name', 'Name', 'trim|required'); 
		$this->form_validation->set_rules('password2', 'confirm password', 'trim|required|matches[password1]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[tbl_users.email]');
		$this->form_validation->set_rules('password1', 'Password', 'trim|required|min_length[6]|matches[password2]', [
			'matches' => 'Password not match!',
			'min_length' => 'Password too short!'
			]);
		$this->form_validation->set_rules('username', 'username', 'trim|required|min_length[6]|is_unique[tbl_users.username]');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/user/create', $data);
		} else {
			// if validation success call function create from user_model
			$this->User_model->create();
		}
	}

	// function create user page
	public function edit($id) {
		// get data from models
		$data = [
			'id' 	=> $id,
			'title' => 'Edit User',
			'config'=> $this->configs->getConfiguration(),
			'userId'=> $this->User_model->getUserById($id),
			'user' 	=> $this->User_model->getUserBySession(),
		];
		// validation rules
		$this->form_validation->set_rules('full_name', 'Name', 'trim|required'); 
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('username', 'username', 'trim|required|min_length[6]');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/user/update', $data);
		} else {
			// if validation success call function create from About_model
			$this->User_model->edit($data);
		}
	}

	// function delete user page
	public function delete($id) {
		// get data from model
		$data = ['id' => $id,'user' => $this->User_model->getUserById($id)];
		// call function delete from user_model
		$this->User_model->delete($data);
		
	}
	
	/*
		========================================================================================================
		Controller for superadmin ============================================================================== 
		========================================================================================================
	*/

	// function show index profile page user
	public function profile() {
		// get data from models
		$data = [
			'title' => 'Profile',
			'config'=> $this->configs->getConfiguration(),
			'user' 	=> $this->User_model->getUserBySession(),
		];
		// view index profile page
		$this->templating->load('layouts/admin/wrapper', 'admin/user/profile', $data);
	}

	// function edit profile user page 
	public function editProfile() {
		// get data from models
		$data = [
			'title' => 'Profile',
			'config'=> $this->configs->getConfiguration(),
			'user' 	=> $this->User_model->getUserBySession(),
		];
		// validation rules
		$this->form_validation->set_rules('gender', 'gender', 'trim');
		$this->form_validation->set_rules('full_name', 'Name', 'trim|required'); 
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/user/profile', $data);
		} else {	
			// if validation success call function edit profile from user_model
			$this->User_model->editProfile($data);
		}
	}

	// function change password user
	public function changePassword() {
		$data = [
			'title' 	=> 'Profile',
			'config' 	=> $this->configs->getConfiguration(),
			'user' 		=> $this->User_model->getUserBySession(),
		];
		// validation rules
		$this->form_validation->set_rules('oldPassword', 'Current Password', 'required|trim');
		$this->form_validation->set_rules('newPassword', 'New Password', 'trim|required|min_length[6]|matches[confPassword]');
		$this->form_validation->set_rules('confPassword', 'Confirm new password', 'trim|required|matches[newPassword]');
		if ($this->form_validation->run() == FALSE) {
			// if validation errors redirect to page
			$this->templating->load('layouts/admin/wrapper', 'admin/user/profile', $data);
		} else {
			// if validation success call function change password from user_model
			$this->User_model->changePassword($data);
		}
	}

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */