<?php if($configs) : ?>

    <!-- carousel header slide -->
    <section id="hero">
        <!-- carousel -->
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <!-- item carousel -->
                <?php if($slider) : ?>
                    <?php $i=1; foreach($slider as $row_slider): ?>
                    <div class="carousel-item <?php if($i==1) echo "active"; ?>" style="background-image: url('<?= base_url('assets/images/gallery/'.$row_slider['image']); ?>');">
                        <div class="animated fadeInUp carousel-caption">
                            <!-- caption -->
                            <div class="carousel-content">
                                <h1 class="animated fadeInDown"><?= $row_slider['title']; ?></h1>
                                <p class="animated fadeIn"><?= word_limiter(strip_tags($row_slider['description']), 50); ?></p>
                                <a href="<?= site_url('/about/'); ?>" class="btn btn-started">Tentang Kami</a>
                            </div>
                        </div>
                    </div>
                    <?php $i++; endforeach; ?>
                <?php else: ?>
                    <div class="carousel-item active" style="background-image: url('<?= base_url('assets/images/gallery/default.png'); ?>');">
                        <div class="animated fadeInUp carousel-caption">
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="btn-carousel-control" aria-hidden="true"><i class="fas fa-fw fa-long-arrow-alt-left"></i></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="btn-carousel-control" aria-hidden="true"><i class="fas fa-fw fa-long-arrow-alt-right"></i></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>

    <!-- introduction -->
    <?php if($about) : ?>
    <section id="introduction" class="">
        <div class="container">
            <div class="row">
                <?php foreach($about as $row_about) : ?>
                <?php if($row_about['name'] == 'sejarah') : ?>

                <div class="col-12 col-md-6 col-lg-6 p-2 intro-img">

                    <?php if($videos) : ?>
                        <?php foreach($videos as $row_video) : ?>
                            <div class="media">
                                <video class="video" controls>
                                    <source src="<?= base_url('assets/videos/' . $row_video['video_name']); ?>" type="video/mp4">
                                </video>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <img class="img-fluid" src="<?= base_url('assets/images/abouts/'.$row_about['image']); ?>" alt="<?= $row_about['image']; ?>" title="<?= $row_about['title']; ?>">
                <?php endif; ?>


                </div>

                <div class="col-12 col-md-6 col-lg-6 p-4 p-md-5 intro-txt">
                    <header>
                        <h2><?= $row_about['title']; ?></h2>
                        <P class="text-justify"><?= word_limiter(strip_tags($row_about['description']),45); ?></P>
                    </header>
                    <a href="<?= site_url('/about/'); ?>" title="Sejarah <?= $configs['siteName']; ?>" class="btn btn-general text-uppercase" title="">Baca Selengkapnya</a>
                </div>
                <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <?php endif; ?>

    <!-- clients -->
    <?php if($partners) : ?>
    <section id="clients">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-12 col-md-3">
                    <header>
                        <?php foreach($config_page as $row_page) : ?>
                            <?php if($row_page['page_position'] == 'Client') : ?>
                                <!-- title client -->
                                <h2 class="mt-3 text-capitalize"><?= $row_page['title']; ?></h2>
                                <p><a href="<?= base_url('/about/'); ?>#clients" class="text-white"><i class="fas fa-arrow-right"></i> Tamplikan lebih banyak</a></p>
                            <?php endif; ?>
                        <?php endforeach; ?>                        
                    </header>
                </div>
                <div class="col-12 col-md-8">
                    <!-- slider owl carousel -->
                    <div class="owl-carousel owl-theme box-clients">
                        <?php $i=1; foreach($partners as $row_partner) : ?>
                        <div class="item img-item text-center">
                            <img class="client" src="<?= base_url('assets/images/partners/'.$row_partner['image']); ?>" alt="<?= $row_partner['image']; ?>" title="<?= $row_partner['partner_name'] ?>">
                            <p class="mt-1 mb-0 text-dark"><?= $row_partner['partner_name'] ?></p>
                        </div>
                        <?php $i++; endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>

    <!-- services -->
    <?php if($services) : ?>
    <section id="services" class="">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <header class="text-center">
                        <?php foreach($config_page as $row_page) : ?>
                            <?php if($row_page['page_position'] == 'Services') : ?>
                                <!-- title service -->
                                <h2 class="mt-3 text-capitalize"><?= $row_page['title']; ?></h2>
                                <p><?= $row_page['description']; ?></p>
                            <?php endif; ?>
                        <?php endforeach; ?> 
                    </header>
                </div>
            </div>
            <div class="row justify-content-center">
                <!-- item service -->
                <?php foreach ($services as $row_service): ?>
                <div class="col-sm-6 col-md-6 col-lg-3 mb-4">
                    <div class="card" data-aos="fade-up" style="background-image: url('<?= base_url('assets/images/services/thumbs/'.$row_service['image']); ?>');">
                        <div class="card-body">
                            <h5 class="card-title"><?= $row_service['title']; ?></h5>
                            <p class="card-text"><?= word_limiter(strip_tags($row_service['description']),10); ?></p>
                            <a href="<?= site_url('/services/detail/'.$row_service['slug']); ?>" class="btn btn-services text-uppercase">Lihat Detail</a>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
                <!-- MORE SERVICES REDIRECT TO SERVICES PAGE -->
                <div class="col-sm-6 col-md-6 col-lg-3 mb-4">
                    <div class="card" data-aos="fade-up" data-aos-delay="300" style="padding-top: 10px;">
                        <!-- <div class="overlay"></div> -->
                        <div class="card-body">
                            <h5 class="card-title mt-5">Lebih Banyak</h1>
                            <h6 class="text-center mt-2 mb-0"><i class="fas fa-box-open"></i></h6>
                            <a href="<?= base_url('/services/'); ?>" class="btn btn-services text-uppercase">Lihat Semua +</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>
    
    <!-- Recent Projects -->
    <?php if($projects) : ?>
    <section id="projects" class="">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <header>
                        <header>
                        <?php foreach($config_page as $row_page) : ?>
                            <?php if($row_page['page_position'] == 'Finished Projects') : ?>
                                <!-- title client -->
                                <h2 class="mt-3 text-capitalize"><?= $row_page['title']; ?></h2>
                                <p><?= $row_page['description']; ?></p>
                            <?php endif; ?>
                        <?php endforeach; ?> 
                        </header>
                    </header>
                </div>
            </div>
            <div class="row justify-content-center">
                <!-- controller filter -->
                <div class="col-12">
                    <ul class="nav justify-content-center">
                        <li class="nav-item">
                          <a data-filter="all" class="btn nav-link active">All</a>
                        </li>
                        <?php $i=1; foreach($category as $row_project) : ?>
                        <li class="nav-item">
                          <a data-filter="<?= $row_project['name']; ?>" class="btn nav-link"><?= $row_project['name']; ?></a>
                        </li>
                        <?php $i++; endforeach; ?>
                    </ul>
                </div>
                <!-- data filter gallery -->
                <div class="container">
                    <div class="filter-container" data-aos="fade-up">
                        <!-- item filter -->
                        <?php $i=1; foreach($projects as $row_project) : ?>
                        <div class="col-sm-6 col-md-6 col-lg-4 box-projects filtr-item" data-category="all,<?= $row_project['name']; ?>">
                            <div class="card img-item web">
                                <div class="card-body p-0">
                                    <img src="<?= base_url('assets/images/projects/finished_projects/thumbs/'.$row_project['image']); ?>" class="card-img-top img-projects" alt="<?= $row_project['image']; ?>" title="<?= $row_project['project_name']; ?>">
                                    <div class="box-control d-flex justify-content-between">
                                        <div class="col-8">
                                            <h5><?= $row_project['project_name']; ?></h5>
                                            <span><?= $row_project['name']; ?></span>
                                        </div>
                                        <div class="col-4 d-flex flex-row-reverse align-items-center">
                                            <a href="<?= site_url('/projects/detail-project/'.$row_project['slug']); ?>" class="btn-control-project">
                                                <i class="fas fa-info-circle alt"></i>
                                            </a>
                                            <a href="<?= base_url('assets/images/projects/finished_projects/'.$row_project['image']); ?>" class="btn-control-project venobox preview-link" data-gall="galleryProject" title="image 1">
                                                <i class="fas fa-search-plus alt"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $i++; endforeach; ?>
                    </div>
                </div>
                <!-- btn view more project -->
                <div class="col-12 mt-4 text-center">
                    <a href="<?= site_url('/projects/finished-projects/') ?>" class="btn btn-general text-uppercase">Tamplikan Lebih Banyak</a>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>

    <!-- check if there breaking news -->
    <?php if($news) : ?>
    <!-- top news -->
    <section id="breakingnews" class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col text-center">
                    <header class="text-center">
                        <?php foreach($config_page as $row_page) : ?>
                            <?php if($row_page['page_position'] == 'News') : ?>
                                <!-- title service -->
                                <h2 class="mt-3 text-capitalize"><?= $row_page['title']; ?></h2>
                                <p><?= $row_page['description']; ?></p>
                            <?php endif; ?>
                        <?php endforeach; ?> 
                    </header>
                </div>
            </div>
            <div class="row justify-content-center">
                <!-- item service -->
                <?php foreach ($news as $row_news): ?>
                <div class="col-sm-6 col-md-6 col-lg-3 mb-4">
                    <div class="card" style="background-image: url('<?= base_url('assets/images/posts/thumbs/'.$row_news['image']); ?>');">
                        <div class="card-body">
                            <h5 class="card-title"><?= $row_news['title']; ?></h5>
                            <p class="card-text"><small><?= date('l, d F Y', strtotime($row_news['created_at'])); ?></small></p>
                            <a href="<?= site_url('/news/post/'.$row_news['slug']); ?>" class="btn btn-news text-uppercase">Baca Lebih Lengkap</a>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <?php endif; ?>
<?php else : ?>
    <section>
        <div class="container" style="min-height: 300px; ">
            <div class="row align-items-center">
                <div class="col-12 text-center">
                    <h2 class="mt-5">EMPTY!</h2>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>